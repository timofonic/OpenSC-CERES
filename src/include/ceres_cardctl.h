/*
 * ceres_cardctl.h: cardctl definitions for custom driver
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la moneda
 */

#ifndef _SC_CERES_CARDCTL_H
#define _SC_CERES_CARDCTL_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <opensc/cardctl.h>

/* key usage for card keys */
#define SC_CARD_KEY_USAGE_SIG 0x80
#define SC_CARD_KEY_USAGE_CIF 0x40

/* ERRORS */
#define SC_ERROR_INVALID_FILE -3001
#define SC_ERROR_NOT_ENOUGH_MEMORY -3002
#define SC_ERROR_OBJECT_ALREADY_EXISTS -3003


enum {
  SC_CERESCTL_BASE = _CTL_PREFIX('C', 'E', 'R'),
  SC_CERESCTL_GENERATE_KEY,
  SC_CERESCTL_STORE_KEY_COMPONENT,
  SC_CERESCTL_GET_NEW_KEY_REFERENCE,
  SC_CERESCTL_CREATE_FILE,
  SC_CERESCTL_DELETE_FILE,
  SC_CERESCTL_SELECT_CRYPTO_FILE
};
  
struct sc_ceresctl_card_genkey_info {
  unsigned char   *pubkey;
  unsigned int    pubkey_len;
  unsigned char   *exponent;
  unsigned int    exponent_len;
  unsigned int    key_usage;
  unsigned char   key_reference;

};

struct tlv {
  u8 tag;
  u8 *length;
  size_t nlen;
  u8 *value;
};
typedef struct tlv tlv_t;

struct sc_ceresctl_card_store_key_component_info {
  int private_component;
  u8 key_usage;
  u8 key_id; 
  tlv_t component;
};

struct sc_ceresctl_card_delete_file_info {
  int key_reference;
  sc_path_t path;
  unsigned int type;
};

#ifdef __cplusplus
}
#endif

#endif

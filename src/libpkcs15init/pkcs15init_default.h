/*
 * pkcs15init_default.h: PKCS#15 init Ceres default header file
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */


#ifndef _PKCS15_INIT_DEFAULT_H
#define _PKCS15_INIT_DEFAULT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <opensc/opensc.h>
#include <opensc/pkcs15.h>
#include <opensc/pkcs15-init.h>
#include "../libcard/base_card.h"
#include "../libcard/pkcs15_default.h"
#include "../include/profile.h"

  int sc_pkcs15init_card_update_any_df(sc_pkcs15_card_t *p15card,
				       sc_profile_t *profile,
				       sc_pkcs15_df_t *df,
				       int is_new);
  int sc_pkcs15init_card_update_file(struct sc_profile *profile, sc_card_t *card,
				     sc_file_t *file, void *data, unsigned int datalen);
  int sc_pkcs15init_card_add_object(struct sc_pkcs15_card *p15card,
				    struct sc_profile *profile,
				    unsigned int df_type,
				    struct sc_pkcs15_object *object);
  int sc_pkcs15init_card_store_public_key(struct sc_pkcs15_card *p15card,
					  struct sc_profile *profile,
					  struct sc_pkcs15init_pubkeyargs *keyargs,
					  struct sc_pkcs15_object **res_obj);

  int sc_pkcs15init_card_store_data(struct sc_pkcs15_card *p15card,
				    struct sc_profile *profile,
				    sc_pkcs15_object_t *object, sc_pkcs15_id_t *id,
				    sc_pkcs15_der_t *data,
				    sc_path_t *path);

  int sc_pkcs15init_card_add_object(struct sc_pkcs15_card *p15card,
				    struct sc_profile *profile,
				    unsigned int df_type,
				    struct sc_pkcs15_object *object);
  
  sc_pkcs15_object_t *sc_pkcs15init_card_new_object(int type, const char *label,
						    sc_pkcs15_id_t *auth_id, void *data);

#ifdef __cplusplus
}
#endif

#endif /* _PKCS15_INIT_DEFAULT_H */

/*
 * pkcs15init_ceres.c: Ceres card support for pkcs15init library.
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include <stdlib.h>
#include <string.h>
#include <opensc/opensc.h>
#include <opensc/log.h>
#include <opensc/pkcs15-init.h>
#include "../include/ceres_cardctl.h"
#include "pkcs15init_default.h"
#include <openssl/sha.h>
#include "../common/util.h"

#define P15_MODULE_NAME "ceres"
#define P15_MODULE_VERSION "0.1.0" 


int pkcs15init_card_key_reference(sc_profile_t *profile, sc_card_t *card,
				  sc_pkcs15_prkey_info_t *prkey_info)
{

  int r = SC_SUCCESS;
  int key_reference = -1;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering pkcs15init_card_key_reference\n");
  r = sc_card_ctl( card, SC_CERESCTL_GET_NEW_KEY_REFERENCE, &key_reference );
  if(r==SC_SUCCESS) {
    /* update info from private key */
    prkey_info->key_reference = key_reference;
   }
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function pkcs15init_card_key_reference new_key_reference=0x%X r=%d\n", key_reference, r);
  return r;

}



static int pkcs15init_card_create_key(sc_profile_t *profile, sc_card_t *card,
        sc_pkcs15_object_t *obj)
{

  int r = SC_SUCCESS;
  sc_pkcs15_prkey_info_t *key_info = (sc_pkcs15_prkey_info_t *) obj->data;
  sc_file_t *tmp_file=NULL;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering pkcs15init_card_create_key\n");

  if (obj->type != SC_PKCS15_TYPE_PRKEY_RSA) {
    sc_error(card->ctx, "FNMT CERES supports only RSA keys.");
    return SC_ERROR_NOT_SUPPORTED;
  }
  
  /* The caller is supposed to have chosen a key file path for us */
  if (key_info->path.len == 0 || key_info->modulus_length == 0)
    return SC_ERROR_INVALID_ARGUMENTS;

  /* preparing data to create virtual files */
  tmp_file = sc_file_new();
  tmp_file->size = 0x4000;

  /* creating private key file path */
  sc_format_path("3F00501520", &tmp_file->path);
  tmp_file->path.value[5]=key_info->id.value[0];
  tmp_file->path.len=0x06;
 
  /* create private key file in vfs */
  r = sc_card_ctl( card, SC_CERESCTL_CREATE_FILE, tmp_file);
  if (r!=SC_SUCCESS)
    goto end;
 
  /* creating another private key file path */
  sc_format_path("3F0020", &tmp_file->path);
  tmp_file->path.value[3]=key_info->id.value[0];
  tmp_file->path.len=0x04;
 
  /* create private key file in vfs */
  r = sc_card_ctl( card, SC_CERESCTL_CREATE_FILE, tmp_file);
  if (r!=SC_SUCCESS)
    goto end;

  /* creating public key file path */
  sc_format_path("3F00501521", &tmp_file->path);
  tmp_file->path.value[5]=key_info->id.value[0];
  tmp_file->path.len=0x06;
 
  /* create public key file in vfs */
  r = sc_card_ctl( card, SC_CERESCTL_CREATE_FILE, tmp_file);
  if (r!=SC_SUCCESS)
    goto end;
 
  /* creating another public key file path */
  sc_format_path("3F0021", &tmp_file->path);
  tmp_file->path.value[3]=key_info->id.value[0];
  tmp_file->path.len=0x04;
 
  /* create private key file in vfs */
  r = sc_card_ctl( card, SC_CERESCTL_CREATE_FILE, tmp_file);
  if (r!=SC_SUCCESS)
    goto end;

 end:
  if (tmp_file) {
    sc_file_free(tmp_file);
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving pkcs15init_card_create_key\n");
  return SC_SUCCESS;

}



static int pkcs15init_card_generate_key(sc_profile_t *profile, sc_card_t *card,
                sc_pkcs15_object_t *obj, sc_pkcs15_pubkey_t *pubkey)
{

  sc_pkcs15_prkey_info_t *key_info = (sc_pkcs15_prkey_info_t *) obj->data;
  int r = SC_SUCCESS;
  struct sc_ceresctl_card_genkey_info genkey_info;
  int opensc_key_reference = 0;
  int prkey_key_usage = 0;
  int prkey_access_flags = 0;
  int pukey_key_usage = 0;
  int pukey_access_flags = 0;
  sc_pkcs15_id_t id;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering pkcs15init_card_generate_key\n");

  if(key_info->id.len != 1)
    return SC_ERROR_INVALID_ARGUMENTS; /* we expect id to be opensc_key_reference */

  opensc_key_reference = key_info->id.value[0];

  /* store path lists */
  r = map_path_to_path_set_all_keys_paths(DRVDATA(card)->virtual_fs_to_card_path_map, opensc_key_reference, key_info->key_reference, (card->type == SC_CARD_TYPE_CERES_ST) ? 1 : 0);
  if(r != SC_SUCCESS) {
    goto out;
  }

  /* init genkey_info structure to 0 */
  memset(&genkey_info, 0, sizeof(genkey_info));

  if (obj->type != SC_PKCS15_TYPE_PRKEY_RSA) {
    sc_error(card->ctx, "FNMT CERES supports only RSA keys.");
    r = SC_ERROR_NOT_SUPPORTED;
    goto out;
  }

  /* we want to use card without virtual fs */

  /* Selecting ICC Crypto file */
  r = sc_card_ctl( card, SC_CERESCTL_SELECT_CRYPTO_FILE, NULL);
  if(r!=SC_SUCCESS)
   goto out;

  /* Updates genkey_info struct */
  genkey_info.pubkey_len = key_info->modulus_length ; 
  genkey_info.pubkey = (key_info->modulus_length < 512) ? malloc(genkey_info.pubkey_len) : malloc(genkey_info.pubkey_len/8) ;
  if(!genkey_info.pubkey) {
    if (card->ctx->debug) sc_debug(card->ctx, "Error while allocating buffer\n");
    r=SC_ERROR_OUT_OF_MEMORY;
    goto out;
  }
  genkey_info.exponent_len = 3;
  genkey_info.exponent = malloc(genkey_info.exponent_len);
  if(!genkey_info.exponent) {
    if (card->ctx->debug) sc_debug(card->ctx, "Error while allocating buffer\n");
    r=SC_ERROR_OUT_OF_MEMORY;
    goto out;
  }
  /* Stores key info to card struct needed to generate keys */
  genkey_info.key_reference = key_info->key_reference;
  /* Always creating keys with the same usage */
  genkey_info.key_usage = SC_CARD_KEY_USAGE_SIG | SC_CARD_KEY_USAGE_CIF;
  
  r = sc_card_ctl( card, SC_CERESCTL_GENERATE_KEY, &genkey_info );
  if (r < 0) {
    if (card->ctx->debug) sc_debug(card->ctx, "Error 0x%X while generating key\n", r);    
    goto out;
  }
  
  /* extract public key */
  pubkey->algorithm = SC_ALGORITHM_RSA;
  pubkey->u.rsa.exponent.len  = genkey_info.exponent_len;
  pubkey->u.rsa.exponent.data = genkey_info.exponent;
  genkey_info.exponent = NULL;
  pubkey->u.rsa.modulus.len   = (key_info->modulus_length < 512 ) ? key_info->modulus_length : (key_info->modulus_length/8 ) ; 
  pubkey->u.rsa.modulus.data  = genkey_info.pubkey;
  genkey_info.pubkey = NULL;

  /* Compute CKAID */
  memset(&id, 0, sizeof(id));
  SHA1(pubkey->u.rsa.modulus.data, pubkey->u.rsa.modulus.len, id.value);  
  id.len = 20;
  r = map_id_to_id_set_item(DRVDATA(card)->virtual_fs_to_card_ckaid_map, &key_info->id, &id);
  if(r != SC_SUCCESS) {
    goto out;
  }
 
  /* store keyinfo */
  prkey_key_usage  = SC_PKCS15_PRKEY_USAGE_DECRYPT;
  prkey_key_usage |= SC_PKCS15_PRKEY_USAGE_SIGN;
  prkey_key_usage |= SC_PKCS15_PRKEY_USAGE_WRAP;

  prkey_access_flags  = SC_PKCS15_PRKEY_ACCESS_SENSITIVE;
  prkey_access_flags |= SC_PKCS15_PRKEY_ACCESS_LOCAL;

  pukey_key_usage  = SC_PKCS15_PRKEY_USAGE_DECRYPT;
  pukey_key_usage |= SC_PKCS15_PRKEY_USAGE_UNWRAP;
  pukey_key_usage |= SC_PKCS15_PRKEY_USAGE_VERIFY;

  pukey_access_flags  = SC_PKCS15_PRKEY_ACCESS_EXTRACTABLE;
  pukey_access_flags |= SC_PKCS15_PRKEY_ACCESS_LOCAL;

  r = map_ckaid_to_keyinfo_set_item (DRVDATA(card)->card_ckaid_to_card_keyinfo_map, &id, prkey_key_usage, prkey_access_flags, pukey_key_usage, pukey_access_flags); 
 
 out:
 if(genkey_info.pubkey) {
    free(genkey_info.pubkey);
    genkey_info.pubkey = NULL;
  }
  if(genkey_info.exponent) {
    free(genkey_info.exponent);
    genkey_info.exponent = NULL;
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving pkcs15init_card_generate_key\n");
  return r;

}


static int pkcs15init_store_key(sc_profile_t *profile, sc_card_t *card,
                sc_pkcs15_object_t *obj, sc_pkcs15_prkey_t *prkey)
{
  sc_pkcs15_prkey_info_t *key_info = (sc_pkcs15_prkey_info_t *) obj->data;
  int r = SC_SUCCESS;
  struct    sc_ceresctl_card_store_key_component_info  component_info;
  unsigned int prkey_key_usage = 0;
  unsigned int prkey_access_flags = 0;
  unsigned int pukey_key_usage = 0;
  unsigned int pukey_access_flags = 0;
  int opensc_key_reference = 0;
  sc_pkcs15_id_t id;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering pkcs15init_card_store_key\n");
  
  if (obj->type != SC_PKCS15_TYPE_PRKEY_RSA) {
    sc_error(card->ctx, "CERES supports only RSA keys.");
    return SC_ERROR_NOT_SUPPORTED;
  }
  

    opensc_key_reference = key_info->path.value[key_info->path.len - 1];

  /* store path lists */
  r = map_path_to_path_set_all_keys_paths(DRVDATA(card)->virtual_fs_to_card_path_map, opensc_key_reference, key_info->key_reference, (card->type == SC_CARD_TYPE_CERES_ST) ? 1 : 0);
  if(r != SC_SUCCESS) {
    goto pcsk_out;
  }
 
  /* store keyinfo */
  prkey_key_usage  = SC_PKCS15_PRKEY_USAGE_DECRYPT;
  prkey_key_usage |= SC_PKCS15_PRKEY_USAGE_SIGN;
  prkey_key_usage |= SC_PKCS15_PRKEY_USAGE_UNWRAP;

  prkey_access_flags  = SC_PKCS15_PRKEY_ACCESS_SENSITIVE;

  pukey_key_usage  = SC_PKCS15_PRKEY_USAGE_ENCRYPT;
  pukey_key_usage |= SC_PKCS15_PRKEY_USAGE_WRAP;
  pukey_key_usage |= SC_PKCS15_PRKEY_USAGE_VERIFY;

  pukey_access_flags  = SC_PKCS15_PRKEY_ACCESS_EXTRACTABLE;

  r = map_ckaid_to_keyinfo_set_item (DRVDATA(card)->card_ckaid_to_card_keyinfo_map, &key_info->id, prkey_key_usage, prkey_access_flags, pukey_key_usage, pukey_access_flags);

 
  /* we want to use card without virtual fs */
  /* Selecting ICC Crypto file */
  r = sc_card_ctl( card, SC_CERESCTL_SELECT_CRYPTO_FILE, NULL);
  if(r!=SC_SUCCESS)
   goto pcsk_out;

  memset(&component_info, 0, sizeof(struct sc_ceresctl_card_store_key_component_info)); 

  component_info.private_component=1;
  component_info.key_id=(u8) key_info->key_reference; 

  /* RSA private key, CRT format, p component */
  buf2tlv(0x02, prkey->u.rsa.p.data, prkey->u.rsa.p.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  
  /* need to free memory */
  free_tlv( &component_info.component );

  /* RSA private key, CRT format, q component */
  buf2tlv(0x04, prkey->u.rsa.q.data, prkey->u.rsa.q.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  
  /* need to free memory */
  free_tlv( &component_info.component );

  /* RSA private key, CRT format, (q^-1)mod p component */
  buf2tlv(0x06, prkey->u.rsa.iqmp.data, prkey->u.rsa.iqmp.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  
  /* need to free memory */
  free_tlv( &component_info.component );

  /* RSA private key, CRT format, (d^-1)mod p component */
  buf2tlv(0x08, prkey->u.rsa.dmp1.data, prkey->u.rsa.dmp1.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  /* need to free memory */
  free_tlv( &component_info.component );

  /* RSA private key, CRT format, (d^-1)mod q component */
  buf2tlv(0x0A, prkey->u.rsa.dmq1.data, prkey->u.rsa.dmq1.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  /* need to free memory */
  free_tlv( &component_info.component );

  if (card->ctx->debug) sc_debug(card->ctx, "pkcs15init_card_store_key sending public components\n");
 
  /* public components */
     
  component_info.private_component=0;
 
  /* RSA public key, exponent e component */
  buf2tlv(0x12, prkey->u.rsa.exponent.data, prkey->u.rsa.exponent.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  /* need to free memory */
  free_tlv( &component_info.component );
 
 
  /* RSA public key, exponent modulus component */
  buf2tlv(0x14, prkey->u.rsa.modulus.data, prkey->u.rsa.modulus.len, &component_info.component);     
  r = sc_card_ctl( card, SC_CERESCTL_STORE_KEY_COMPONENT, &component_info );
  if (r !=SC_SUCCESS)
    goto pcsk_out;
  
  /* need to free memory */
  free_tlv( &component_info.component );

  /* Compute CKAID */
  memset(&id, 0, sizeof(id));
  SHA1(prkey->u.rsa.modulus.data , prkey->u.rsa.modulus.len , id.value);  
  id.len = 20;
  r = map_id_to_id_set_item(DRVDATA(card)->virtual_fs_to_card_ckaid_map, &key_info->id, &id);
  if(r != SC_SUCCESS) {
    goto pcsk_out;
  }

 pcsk_out:

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving pkcs15init_card_store_key\n");
  return r;

}

int pkcs15init_card_delete_object (struct sc_profile *pro, struct sc_card *card,
				   unsigned int type, const void *data, const sc_path_t *path)
{
  int r = SC_SUCCESS;
  sc_pkcs15_cert_info_t *cert_info=NULL;
  sc_pkcs15_pubkey_info_t *pubkey_info=NULL;
  sc_pkcs15_prkey_info_t *privkey_info=NULL;
  sc_path_t tmp_path, key_path;
  sc_file_t *key_file=NULL;
  struct sc_ceresctl_card_delete_file_info info_file;

  int old_use_virtual_fs; /*!< backup of use_virtual_fs */
    

  memcpy(&tmp_path, path, sizeof(struct sc_path));

  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

  switch(type) {
  case SC_PKCS15_TYPE_PRKEY_RSA:
    sc_format_path("3F003F11", &key_path);
    r = ceres_select_file(card, &key_path, &key_file);
    SC_TEST_RET(card->ctx, r, "Key Path selection failed");
    if (r<0)
      goto end;
    memcpy(key_file->path.value, key_path.value, 0x04);
    key_file->path.len = 0x04;
    key_file->path.type = SC_PATH_TYPE_PATH;
    r = sc_pkcs15init_authenticate(pro, card, key_file, SC_AC_OP_DELETE);
    if (r!=SC_SUCCESS)
      goto end;
    privkey_info = (sc_pkcs15_prkey_info_t *) data;
    info_file.key_reference = privkey_info->key_reference;
    info_file.path = privkey_info->path;
    info_file.type = type;
 
    r = sc_card_ctl(card, SC_CERESCTL_DELETE_FILE, &info_file);
    break;
  case SC_PKCS15_TYPE_PUBKEY_RSA:
    sc_format_path("3F003F11", &key_path);
    r = ceres_select_file(card, &key_path, &key_file);
    SC_TEST_RET(card->ctx, r, "Key Path selection failed");
    if (r<0)
      goto end;
    memcpy(key_file->path.value, key_path.value, 0x04);
    key_file->path.len = 0x04;
    key_file->path.type = SC_PATH_TYPE_PATH;
    r = sc_pkcs15init_authenticate(pro, card, key_file, SC_AC_OP_DELETE);
    if (r!=SC_SUCCESS)
      goto end;
    pubkey_info = (sc_pkcs15_pubkey_info_t *) data;

    info_file.key_reference = pubkey_info->key_reference;
    info_file.path = pubkey_info->path;
    info_file.type = type;
 
    r = sc_card_ctl(card, SC_CERESCTL_DELETE_FILE, &info_file);
    break;
  case SC_PKCS15_TYPE_CERT_X509:
    cert_info = (sc_pkcs15_cert_info_t *) data;

    info_file.key_reference = 0; /* Certificate don't have key reference */ 
    info_file.path = cert_info->path;
    info_file.type = type;
 
    r = sc_card_ctl(card, SC_CERESCTL_DELETE_FILE, &info_file);
    break;
  default:
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }
  
  end:
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);
  if(key_file)
    sc_file_free(key_file);

  return r;
}



static struct sc_pkcs15init_operations sc_pkcs15init_card_operations = {
        NULL, /* erase card */
	      /* NEW API */
        NULL, /* initialize */
        NULL, /* create a df */
	NULL, /* create a domain */
	NULL, /* select pin reference */
	NULL, /* create a pin object in the given df */
	pkcs15init_card_key_reference, /* select a reference for a private key object */
	pkcs15init_card_create_key, /* Create an empty key object */
	pkcs15init_store_key, /* Store a key on the card */
	pkcs15init_card_generate_key, /* Generate key */
	NULL, /* Encode private/public key */
	NULL, /* Finalize card */
	      /* OLD API */
	NULL, /* initialize */
	NULL, /* Store a new PIN */
	NULL, /* Store a key on the card */
	NULL, /* Create a file based on a PKCS15_TYPE_xxx */
	NULL, /* Generate a new key pair */
	NULL, /* dummy poiner */
	pkcs15init_card_delete_object  /* Delete object */
};

static struct sc_pkcs15init_operations * sc_get_driver(void)
{ 
  return &sc_pkcs15init_card_operations;
}

/*
 * Called when module is loaded
 * you should return a pointer to sc_get_mymodule_driver();
 */ 
void * sc_module_init(const char *drivername)  
{   
  if (!drivername) 
    return NULL;
  if (strcmp(P15_MODULE_NAME, drivername))
    return NULL;

  return sc_get_driver;  
}

SC_IMPLEMENT_DRIVER_VERSION(P15_MODULE_VERSION)







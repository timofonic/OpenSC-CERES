/*
 * pkcs15init_default.c: Ceres card specific calls for pkcs15init library.
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "pkcs15init_default.h"
#include "../common/strlcpy.h"

/* Default ID for new key/pin */
#define DEFAULT_ID                      0x45
#define DEFAULT_PIN_FLAGS               0x03
#define DEFAULT_PRKEY_ACCESS_FLAGS      0x1d
#define DEFAULT_PRKEY_FLAGS             0x03
#define DEFAULT_PUBKEY_FLAGS            0x02
#define DEFAULT_CERT_FLAGS              0x02
#define DEFAULT_DATA_FLAGS              0x02

int
sc_pkcs15init_get_pin_path(sc_pkcs15_card_t *p15card,
                sc_pkcs15_id_t *auth_id, sc_path_t *path)
{
        sc_pkcs15_object_t *obj;
        int     r;

        r = sc_pkcs15_find_pin_by_auth_id(p15card, auth_id, &obj);
        if (r < 0)
                return r;
        *path = ((sc_pkcs15_pin_info_t *) obj->data)->path;
        return 0;
}


/*
 * Select a path for a new object
 *  1.  If the object is to be protected by a PIN, use the path
 *      given in the PIN auth object
 *  2.  Otherwise, use the path of the application DF
 *  3.  If the profile defines a key-dir template, the new object
 *      should go into a subdirectory of the selected DF:
 *      Instantiate the template, using the ID of the new object
 *      to uniquify the path. Inside the instantiated template,
 *      look for a file corresponding to the type of object we
 *      wish to create ("private-key", "public-key" etc).
 */
int
select_object_path(sc_pkcs15_card_t *p15card, sc_profile_t *profile,
                sc_pkcs15_object_t *obj, sc_pkcs15_id_t *obj_id,
                sc_path_t *path)
{
        sc_file_t       *file;
        const char      *name;
        int             r;
        char            pbuf[SC_MAX_PATH_STRING_SIZE];

        /* For cards with a pin-domain profile, we need
         * to put the key below the DF of the specified PIN */
        memset(path, 0, sizeof(*path));
        if (obj->auth_id.len) {
                r = sc_pkcs15init_get_pin_path(p15card, &obj->auth_id, path);
                if (r < 0)
                        return r;
        } else {
                *path = profile->df_info->file->path;
        }

        /* If the profile specifies a key directory template,
         * instantiate it now and create the DF
         */
        switch (obj->type & SC_PKCS15_TYPE_CLASS_MASK) {
        case SC_PKCS15_TYPE_PRKEY:
                name = "private-key";
                break;
        case SC_PKCS15_TYPE_PUBKEY:
                name = "public-key";
                break;
        case SC_PKCS15_TYPE_CERT:
                name = "certificate";
                break;
        case SC_PKCS15_TYPE_DATA_OBJECT:
                name = "data";
                break;
        default:
                return 0;
        }

        r = sc_path_print(pbuf, sizeof(pbuf), path);
        if (r != SC_SUCCESS)
                pbuf[0] = '\0';

        sc_debug(p15card->card->ctx,
                "key-domain.%s @%s (auth_id.len=%d)\n",
                name, pbuf, obj->auth_id.len);
        r = sc_profile_instantiate_template(profile,
                                        "key-domain", path,
                                        name, obj_id, &file);
        if (r < 0) {
                if (r == SC_ERROR_TEMPLATE_NOT_FOUND)
                        return 0;
                return r;
        }

        *path = file->path;
        sc_file_free(file);
        return 0;
}

/*
 * Find out whether the card was initialized using an SO PIN,
 * and if so, set the profile information
 */
int
set_so_pin_from_card(struct sc_pkcs15_card *p15card, struct sc_profile *profile)
{
        struct sc_pkcs15_pin_info *pin;
        struct sc_pkcs15_object *obj;
        int             r;

        r = sc_pkcs15_find_so_pin(p15card, &obj);
        if (r == 0) {
                pin = (struct sc_pkcs15_pin_info *) obj->data;
                return sc_keycache_set_pin_name(&pin->path,
                                pin->reference,
                                SC_PKCS15INIT_SO_PIN);
        }

        /* If the card doesn't have an SO PIN, we simply zap the
         * naming info from the cache */
        if (r == SC_ERROR_OBJECT_NOT_FOUND)
                return sc_keycache_set_pin_name(NULL, -1, SC_PKCS15INIT_SO_PIN);

        return r;
}


int
select_id(sc_pkcs15_card_t *p15card, int type, sc_pkcs15_id_t *id,
                int (*can_reuse)(const sc_pkcs15_object_t *, void *),
                void *data, sc_pkcs15_object_t **reuse_obj)
{
        unsigned int nid = DEFAULT_ID;
        sc_pkcs15_id_t unused_id;
        struct sc_pkcs15_object *obj;
        int r;
	

        if (reuse_obj)
                *reuse_obj = NULL;

        /* If the user provided an ID, make sure we can use it */
        if (id->len != 0) {
                r = sc_pkcs15_find_object_by_id(p15card, type, id, &obj);
                if (r == SC_ERROR_OBJECT_NOT_FOUND)
                        return 0;
                if (strcmp(obj->label, "deleted"))
                        return SC_ERROR_ID_NOT_UNIQUE;
                if (can_reuse != NULL && !can_reuse(obj, data))
                        return SC_ERROR_INCOMPATIBLE_OBJECT;
                if (reuse_obj)
                        *reuse_obj = obj;
                return 0;
        }

        memset(&unused_id, 0, sizeof(unused_id));
        while (nid < 255) {
                id->value[0] = nid++;
                id->len = 1;

                r = sc_pkcs15_find_object_by_id(p15card, type, id, &obj);
                if (r == SC_ERROR_OBJECT_NOT_FOUND) {
                        /* We don't have an object of that type yet.
			 * If we're allocating a PRKEY object, make
                         * sure there's no conflicting pubkey or cert
                         * object either. */
                        if (type == SC_PKCS15_TYPE_PRKEY) {
                                sc_pkcs15_search_key_t search_key;

                                memset(&search_key, 0, sizeof(search_key));
                                search_key.class_mask =
                                        SC_PKCS15_SEARCH_CLASS_PUBKEY |
                                        SC_PKCS15_SEARCH_CLASS_CERT;
                                search_key.id = id;

                                r = sc_pkcs15_search_objects(p15card,
                                                &search_key,
                                                NULL, 0);
                                /* If there is a pubkey or cert with
                                 * this ID, skip it. */
                                if (r > 0)
                                        continue;
                        }
                        if (!unused_id.len)
                                unused_id = *id;
                        continue;
                }

                /* Check if we can reuse a deleted object */
                if (!strcmp(obj->label, "deleted")
                 && (can_reuse == NULL || can_reuse(obj, data))) {
                        if (reuse_obj)
                                *reuse_obj = obj;
                        return 0;
                }
        }

        if (unused_id.len) {
                *id = unused_id;
                return 0;
        }

        return SC_ERROR_TOO_MANY_OBJECTS;
}
  

static struct sc_pkcs15_df *
find_df_by_type(struct sc_pkcs15_card *p15card, unsigned int type)
{
        struct sc_pkcs15_df *df = p15card->df_list;

        while (df != NULL && df->type != type)
                df = df->next;
        return df;
}

static int
sc_pkcs15init_update_odf(struct sc_pkcs15_card *p15card,
                struct sc_profile *profile)
{
        struct sc_card  *card = p15card->card;
        u8              *buf = NULL;
        size_t          size;
        int             r;

        sc_debug(card->ctx, "called\n");
        r = sc_pkcs15_encode_odf(card->ctx, p15card, &buf, &size);
        if (r >= 0)
                r = sc_pkcs15init_update_file(profile, card,
                               p15card->file_odf, buf, size);
        if (buf)
                free(buf);
        return r;
}

sc_pkcs15_object_t *
sc_pkcs15init_card_new_object(int type, const char *label, sc_pkcs15_id_t *auth_id, void *data)
{
        sc_pkcs15_object_t      *object;
        unsigned int            data_size = 0;

        object = (sc_pkcs15_object_t *) calloc(1, sizeof(*object));
        if (object == NULL)
                return NULL;
        object->type = type;

        switch (type & SC_PKCS15_TYPE_CLASS_MASK) {
        case SC_PKCS15_TYPE_AUTH:
                object->flags = DEFAULT_PIN_FLAGS;
                data_size = sizeof(sc_pkcs15_pin_info_t);
                break;
        case SC_PKCS15_TYPE_PRKEY:
                object->flags = DEFAULT_PRKEY_FLAGS;
                data_size = sizeof(sc_pkcs15_prkey_info_t);
                break;
        case SC_PKCS15_TYPE_PUBKEY:
                object->flags = DEFAULT_PUBKEY_FLAGS;
                data_size = sizeof(sc_pkcs15_pubkey_info_t);
                break;
        case SC_PKCS15_TYPE_CERT:
                object->flags = DEFAULT_CERT_FLAGS;
                data_size = sizeof(sc_pkcs15_cert_info_t);
                break;
        case SC_PKCS15_TYPE_DATA_OBJECT:
                object->flags = DEFAULT_DATA_FLAGS;
                data_size = sizeof(sc_pkcs15_data_info_t);
                break;
        }

        if (data_size) {
                object->data = calloc(1, data_size);
                if (data)
                        memcpy(object->data, data, data_size);
        }

        if (label)
                strlcpy(object->label, label, sizeof(object->label));
        if (auth_id)
                object->auth_id = *auth_id;

        return object;
}


/*
 * Map X509 keyUsage extension bits to PKCS#15 keyUsage bits
 */
typedef struct {
        unsigned long x509_usage;
        unsigned int p15_usage;
} sc_usage_map;

static sc_usage_map x509_to_pkcs15_private_key_usage[16] = {
        { SC_PKCS15INIT_X509_DIGITAL_SIGNATURE,
          SC_PKCS15_PRKEY_USAGE_SIGN | SC_PKCS15_PRKEY_USAGE_SIGNRECOVER },
        { SC_PKCS15INIT_X509_NON_REPUDIATION, SC_PKCS15_PRKEY_USAGE_NONREPUDIATION },
        { SC_PKCS15INIT_X509_KEY_ENCIPHERMENT, SC_PKCS15_PRKEY_USAGE_UNWRAP },
        { SC_PKCS15INIT_X509_DATA_ENCIPHERMENT, SC_PKCS15_PRKEY_USAGE_DECRYPT },
        { SC_PKCS15INIT_X509_KEY_AGREEMENT, SC_PKCS15_PRKEY_USAGE_DERIVE },
        { SC_PKCS15INIT_X509_KEY_CERT_SIGN,
          SC_PKCS15_PRKEY_USAGE_SIGN | SC_PKCS15_PRKEY_USAGE_SIGNRECOVER },
        { SC_PKCS15INIT_X509_CRL_SIGN,
          SC_PKCS15_PRKEY_USAGE_SIGN | SC_PKCS15_PRKEY_USAGE_SIGNRECOVER }
};

static sc_usage_map x509_to_pkcs15_public_key_usage[16] = {
        { SC_PKCS15INIT_X509_DIGITAL_SIGNATURE,
          SC_PKCS15_PRKEY_USAGE_VERIFY | SC_PKCS15_PRKEY_USAGE_VERIFYRECOVER },
        { SC_PKCS15INIT_X509_NON_REPUDIATION, SC_PKCS15_PRKEY_USAGE_NONREPUDIATION },
        { SC_PKCS15INIT_X509_KEY_ENCIPHERMENT, SC_PKCS15_PRKEY_USAGE_WRAP },
        { SC_PKCS15INIT_X509_DATA_ENCIPHERMENT, SC_PKCS15_PRKEY_USAGE_ENCRYPT },
        { SC_PKCS15INIT_X509_KEY_AGREEMENT, SC_PKCS15_PRKEY_USAGE_DERIVE },
        { SC_PKCS15INIT_X509_KEY_CERT_SIGN,
          SC_PKCS15_PRKEY_USAGE_VERIFY | SC_PKCS15_PRKEY_USAGE_VERIFYRECOVER },
        { SC_PKCS15INIT_X509_CRL_SIGN,
          SC_PKCS15_PRKEY_USAGE_VERIFY | SC_PKCS15_PRKEY_USAGE_VERIFYRECOVER }
};

static int
sc_pkcs15init_map_usage(unsigned long x509_usage, int _private)
{
        unsigned int    p15_usage = 0, n;
        sc_usage_map   *map;

        map = _private ? x509_to_pkcs15_private_key_usage
                      : x509_to_pkcs15_public_key_usage;
        for (n = 0; n < 16; n++) {
                if (x509_usage & map[n].x509_usage)
                        p15_usage |= map[n].p15_usage;
        }
        return p15_usage;
}


/*
 * Compute modulus length
 */
size_t
sc_pkcs15init_keybits(sc_pkcs15_bignum_t *bn)
{
        unsigned int    mask, bits;

        if (!bn || !bn->len)
                return 0;
        bits = bn->len << 3;
        for (mask = 0x80; !(bn->data[0] & mask); mask >>= 1)
                bits--;
        return bits;
}


struct file_info *
sc_profile_card_find_file_by_path(struct sc_profile *pro, const sc_path_t *path)
{
        struct file_info *fi;
        struct sc_file  *fp;

        for (fi = pro->ef_list; fi; fi = fi->next) {
                fp = fi->file;
                if (fp->path.len == path->len
                 && !memcmp(fp->path.value, path->value, path->len))
                        return fi;
        }
        return NULL;
}


int
sc_profile_card_get_file_by_path(struct sc_profile *profile,
                const sc_path_t *path, sc_file_t **ret)
{
        struct file_info *fi;

        if ((fi = sc_profile_card_find_file_by_path(profile, path)) == NULL)
                return SC_ERROR_FILE_NOT_FOUND;
        sc_file_dup(ret, fi->file);
        if (*ret == NULL)
                return SC_ERROR_OUT_OF_MEMORY;
        return 0;
}


/*
 * Update any PKCS15 DF file (except ODF and DIR)
 */
int sc_pkcs15init_card_update_any_df(sc_pkcs15_card_t *p15card,
                sc_profile_t *profile,
                sc_pkcs15_df_t *df,
                int is_new)
{
  struct sc_card  *card = p15card->card;
  sc_file_t       *file = df->file, *pfile = NULL;
  u8              *buf = NULL;
  size_t          bufsize;
  int             update_odf = is_new, r = 0;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering sc_pkcs15init_card_update_any_df\n");

  if (!sc_profile_card_get_file_by_path(profile, &df->path, &pfile))
    file = pfile;

  r = sc_pkcs15_card_encode_df(card->ctx, p15card, df, &buf, &bufsize);
  if (r >= 0) {
    r = sc_pkcs15init_card_update_file(profile, card,
				       file, buf, bufsize);

    if (profile->pkcs15.encode_df_length) {
      df->path.count = bufsize;
      df->path.index = 0;
      update_odf = 1;
    }
    free(buf);
  }
  if (pfile)
    sc_file_free(pfile);

  /* Now update the ODF if we have to */
  if (r >= 0 && update_odf)
    r = sc_pkcs15init_update_odf(p15card, profile);

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving sc_pkcs15init_card_update_any_df\n");

  return r;
}

int sc_pkcs15init_card_update_file(struct sc_profile *profile, sc_card_t *card,
				   sc_file_t *file, void *data, unsigned int datalen)
{
  struct sc_file  *info = NULL;
  void            *copy = NULL;
  int             r = SC_SUCCESS, need_to_zap = 0;
  char            pbuf[SC_MAX_PATH_STRING_SIZE];
  sc_path_t       p15_path;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering sc_pkcs15init_card_update_file\n");

  /* Put into PKCS#15 directory if file path is relative */
  if (file == NULL)
    goto spcuf_err;

  if (file->path.len == 2) {
    sc_format_path("3F005015", &p15_path);
    r = sc_select_file(card, &p15_path, NULL);
    if (r != SC_SUCCESS)
      goto spcuf_err;
  }
  r = sc_path_print(pbuf, sizeof(pbuf), &file->path);
  if (r != SC_SUCCESS)
    pbuf[0] = '\0';
  sc_debug(card->ctx, "called, path=%s, %u bytes\n", pbuf, datalen);

  sc_ctx_suppress_errors_on(card->ctx);
  if ((r = sc_select_file(card, &file->path, &info)) < 0) {
    sc_ctx_suppress_errors_off(card->ctx);
    /* Create file if it doesn't exist */
    if (file->size < datalen)
      file->size = datalen;
    if (r != SC_ERROR_FILE_NOT_FOUND
	|| (r = sc_pkcs15init_create_file(profile, card, file)) < 0
	|| (r = sc_select_file(card, &file->path, &info)) < 0)
      return r;
  } else {
    sc_ctx_suppress_errors_off(card->ctx);
    need_to_zap = 1;
  }

  if (info->size < datalen) {
    char pbuf[SC_MAX_PATH_STRING_SIZE];

    r = sc_path_print(pbuf, sizeof(pbuf), &file->path);
    if (r != SC_SUCCESS)
      pbuf[0] = '\0';
    sc_error(card->ctx,
	     "File %s too small (require %u, have %u) - "
	     "please increase size in profile", pbuf,
	     datalen, info->size);
    sc_file_free(info);
    return SC_ERROR_TOO_MANY_OBJECTS;
  } else if (info->size > datalen && need_to_zap) {
    /* zero out the rest of the file - we may have shrunk
     * the file contents */
    copy = calloc(1, info->size);
    if (copy == NULL) {
      sc_file_free(info);
      return SC_ERROR_OUT_OF_MEMORY;
    }
    memcpy(copy, data, datalen);
    datalen = info->size;
    data = copy;
  }

  /* Present authentication info needed */
  r = sc_pkcs15init_authenticate(profile, card, file, SC_AC_OP_UPDATE);

  if (r >= 0 && datalen) {
    r = sc_update_binary(card, 0, (const u8 *) data, datalen, 0);
  }

 spcuf_err:

  if (copy)
    free(copy);
  sc_file_free(info);

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving sc_pkcs15init_card_update_file\n");

  return r;
}

/*
 * Add an object to one of the pkcs15 directory files.
 */
int sc_pkcs15init_card_add_object(struct sc_pkcs15_card *p15card,
				  struct sc_profile *profile,
				  unsigned int df_type,
				  struct sc_pkcs15_object *object)
{
  struct sc_card  *card = p15card->card;
  struct sc_pkcs15_df *df;
  struct sc_file  *file = NULL;
  int is_new = 0, r = 0;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering sc_pkcs15init_card_add_object\n");

  sc_debug(card->ctx, "called, DF %u obj %p\n", df_type, object);

  df = find_df_by_type(p15card, df_type);
  if (df != NULL) {
    file = df->file;
  } else {
    file = profile->df[df_type];
    if (file == NULL) {
      sc_error(card->ctx,
	       "Profile doesn't define a DF file %u",
	       df_type);
      return SC_ERROR_NOT_SUPPORTED;
    }
    sc_pkcs15_add_df(p15card, df_type, &file->path, file);
    df = find_df_by_type(p15card, df_type);
    assert(df != NULL);
    is_new = 1;

    /* Mark the df as enumerated, so libopensc doesn't try
     * to load the file at a most inconvenient moment */
    df->enumerated = 1;
  }

  if (object == NULL) {
    /* Add nothing; just instantiate this directory file */
  } else if (object->df == NULL) {
    object->df = df;
    r = sc_pkcs15_add_object(p15card, object);
    if (r < 0)
      return r;
  } else {
    /* Reused an existing object */
    assert(object->df == df);
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Calling sc_pkcs15init_card_update_any_df and leabing sc_pkcs15init_card_add_object\n");

  return sc_pkcs15init_card_update_any_df(p15card, profile, df, is_new);
}

/*
 * Check if a given pkcs15 pubkey object can be reused
 */
static int
can_reuse_pubkey_obj(const sc_pkcs15_object_t *obj, void *data)
{
        sc_pkcs15_pubkey_info_t *key, *new_key;
        sc_pkcs15_object_t      *new_obj;

        new_obj = (sc_pkcs15_object_t *) data;
        if (obj->type != new_obj->type)
                return 0;

        key = (sc_pkcs15_pubkey_info_t *) obj->data;
        new_key = (sc_pkcs15_pubkey_info_t *) new_obj->data;
        if (key->modulus_length != new_key->modulus_length)
                return 0;

        /* Some cards don't enforce key usage, so we might as
         * well allow the user to change it on those cards.
         * Not yet implemented */
        if (key->usage != new_key->usage)
                return 0;

        /* Make sure the PIN is the same */
        if (!sc_pkcs15_compare_id(&obj->auth_id, &new_obj->auth_id))
                return 0;

        return 1;
}


/*
 * Store a public key
 */
int sc_pkcs15init_card_store_public_key(struct sc_pkcs15_card *p15card,
					struct sc_profile *profile,
					struct sc_pkcs15init_pubkeyargs *keyargs,
					struct sc_pkcs15_object **res_obj)
{
  struct sc_pkcs15_object *object;
  struct sc_pkcs15_pubkey_info *key_info;
  sc_pkcs15_pubkey_t key;
  sc_pkcs15_der_t der_encoded;
  sc_path_t       *path;
  const char      *label;
  unsigned int    keybits, type, usage;
  int             r;
  struct sc_card  *card = p15card->card;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering sc_pkcs15init_card_store_public_key\n");

  if (!res_obj || !keyargs)
    return SC_ERROR_NOT_SUPPORTED;

  /* Create a copy of the key first */
  key = keyargs->key;

  switch (key.algorithm) {
  case SC_ALGORITHM_RSA:
    keybits = sc_pkcs15init_keybits(&key.u.rsa.modulus);
    type = SC_PKCS15_TYPE_PUBKEY_RSA; break;
#ifdef SC_PKCS15_TYPE_PUBKEY_DSA
  case SC_ALGORITHM_DSA:
    keybits = sc_pkcs15init_keybits(&key.u.dsa.q);
    type = SC_PKCS15_TYPE_PUBKEY_DSA; break;
#endif
  default:
    sc_error(p15card->card->ctx, "Unsupported key algorithm.\n");
    return SC_ERROR_NOT_SUPPORTED;
  }

  if ((usage = keyargs->usage) == 0) {
    usage = SC_PKCS15_PRKEY_USAGE_SIGN;
    if (keyargs->x509_usage)
      usage = sc_pkcs15init_map_usage(keyargs->x509_usage, 0);
  }
  if ((label = keyargs->label) == NULL)
    label = "Public Key";

  /* Set up the pkcs15 object. If we find below that we should
   * reuse an existing object, we'll dith this one. */
  object = sc_pkcs15init_card_new_object(type, label, &keyargs->auth_id, NULL);
  if (object == NULL)
    return SC_ERROR_OUT_OF_MEMORY;

  key_info = (sc_pkcs15_pubkey_info_t *) object->data;
  key_info->usage = usage;
  key_info->modulus_length = keybits;

  /* Select a Key ID if the user didn't specify one, otherwise
   * make sure it's unique */
  *res_obj = NULL;
  r = select_id(p15card, SC_PKCS15_TYPE_PUBKEY, &keyargs->id,
		can_reuse_pubkey_obj, object, res_obj);
  if (r < 0)
    return r;

  /* If we reuse an existing object, update it */
  if (*res_obj) {
    sc_pkcs15_free_pubkey_info(key_info);
    key_info = NULL;
    sc_pkcs15_free_object(object);
    object = *res_obj;

    strlcpy(object->label, label, sizeof(object->label));
  } else {
    key_info->id = keyargs->id;
    *res_obj = object;
  }

  /* DER encode public key components */
  r = sc_pkcs15_ceres_encode_pubkey(p15card->card->ctx, &key,
				    &der_encoded.value, &der_encoded.len);
  if (r < 0)
    return r;

  /* Now create key file and store key */
  r = sc_pkcs15init_card_store_data(p15card, profile,
				    object, &keyargs->id,
				    &der_encoded, &key_info->path);
  
  path = &key_info->path;
  if (path->count == 0) {
    path->index = 0;
    path->count = -1;
  }

  /* Update the PuKDF */
  if (r >= 0)
    r = sc_pkcs15init_card_add_object(p15card, profile,
				      SC_PKCS15_PUKDF, object);

  if (r >= 0 && res_obj)
    *res_obj = object;

  if (der_encoded.value)
    free(der_encoded.value);

  profile->dirty = 1;

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving sc_pkcs15init_card_store_public_key\n");

  return r;
}

int sc_pkcs15init_card_store_data(struct sc_pkcs15_card *p15card,
				  struct sc_profile *profile,
				  sc_pkcs15_object_t *object, sc_pkcs15_id_t *id,
				  sc_pkcs15_der_t *data,
				  sc_path_t *path)
{
  struct sc_file  *file = NULL;
  int             r;
  unsigned int idx = -1;
  struct sc_card  *card = p15card->card;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering sc_pkcs15init_card_store_data\n");

  /* Set the SO PIN reference from card */
  if ((r = set_so_pin_from_card(p15card, profile)) < 0)
    return r;

  if (profile->ops->new_file == NULL) {
    /* New API */
    r = select_object_path(p15card, profile,
			   object, id,
			   path);
    if (r < 0)
      return r;

    r = sc_profile_get_file_by_path(profile, path, &file);
    if (r < 0)
      return r;
  } else {

    /* Get the number of objects of this type already on this card */
    idx = sc_pkcs15_get_objects(p15card,
                                object->type & SC_PKCS15_TYPE_CLASS_MASK,
                                NULL, 0);

    /* Allocate data file */
    r = profile->ops->new_file(profile, p15card->card,
			       object->type, idx, &file);
    if (r < 0) {
      sc_error(p15card->card->ctx, "Unable to allocate file");
      goto done;
    }
  }
  if (file->path.count == 0) {
    file->path.index = 0;
    file->path.count = -1;
  }
  r = sc_pkcs15init_card_update_file(profile, p15card->card,
				     file, data->value, data->len);

  *path = file->path;

 done:   if (file)
    sc_file_free(file);

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving sc_pkcs15init_card_store_data\n");
  return r;
}

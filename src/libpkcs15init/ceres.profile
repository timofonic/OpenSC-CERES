#
# PKCS15 r/w profile for Ceres cards
#
cardinfo {
    max-pin-length	= 16;
    pin-encoding	= ascii-numeric;
    pin-pad-char	= 0x00;
}

PIN user-pin {
    reference	= 0x00;
}

pkcs15 {
    # Put certificates into the CDF itself?
    direct-certificates = no;
    # Put the DF length into the ODF file?
    encode-df-length    = no;
    # Have a lastUpdate field in the EF(TokenInfo)?
    do-last-update              = no;
}

# Additional filesystem info.
# This is added to the file system info specified in the
# main profile.
filesystem {
        DF MF {
                DF PKCS15-AppDF {
                        template key-domain {
                                EF private-key {
                                        file-id = 2000;
                                        ACL = *=CHV1;
                                }
                                EF public-key {
                                    file-id     = 2100;
                                    ACL         = *=CHV1, READ=NONE, UPDATE=NONE;
                                }       
                                EF certificate {
                                        file-id  = 2200;
                                        ACL      = *=CHV1, READ=NONE, UPDATE=NONE;                       
                                }
                                EF data {
                                        file-id  = 2300;
                                        ACL      = *=CHV1, READ=NONE, UPDATE=NONE;                       
                                }
                                EF privdata {
                                        file-id  = 2400;
                                        ACL      = *=CHV1, READ=NONE, UPDATE=NONE;                       
                                }
                        }
			EF virtual-prkey {
				file-id = 6001;
                                ACL      = *=CHV1;                       
			}
			EF virtual-pukey {
				file-id = 6002;
                                ACL      = *=CHV1, READ=NONE, UPDATE=NONE;                       
			}
			EF virtual-cdf {
				file-id = 6004;
                                ACL      = *=CHV1, READ=NONE, UPDATE=NONE;                       
			}
			EF virtual-obj {
				file-id = 6000;
                                ACL      = *=CHV1, READ=NONE, UPDATE=NONE;                       
			}
                }
        }
}


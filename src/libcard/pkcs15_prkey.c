/*
 * pkcs15_prkey.c: PKCS #15 Ceres private key functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda 
 *
 */

#include "../include/internal.h"
#include <opensc/asn1.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ltdl.h>
#include <opensc/log.h>
#include <opensc/pkcs15.h>
#include "pkcs15_default.h"
static const struct sc_asn1_entry c_asn1_com_key_attr[] = {
        { "iD",          SC_ASN1_PKCS15_ID, SC_ASN1_TAG_OCTET_STRING, 0, NULL, NULL },
        { "usage",       SC_ASN1_BIT_FIELD_3, SC_ASN1_TAG_BIT_STRING, 0, NULL, NULL },
        { "native",      SC_ASN1_BOOLEAN, SC_ASN1_TAG_BOOLEAN, SC_ASN1_OPTIONAL, NULL, NULL },
        { "accessFlags", SC_ASN1_BIT_FIELD, SC_ASN1_TAG_BIT_STRING, SC_ASN1_OPTIONAL, NULL, NULL },
        { "keyReference",SC_ASN1_INTEGER, SC_ASN1_TAG_INTEGER, SC_ASN1_OPTIONAL, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_com_prkey_attr[] = {
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_rsakey_attr[] = {
        { "value",         SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
        { "modulusLength", SC_ASN1_INTEGER_2, SC_ASN1_TAG_INTEGER, 0, NULL, NULL },
        { "keyInfo",       SC_ASN1_INTEGER, SC_ASN1_TAG_INTEGER, SC_ASN1_OPTIONAL, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_prk_rsa_attr[] = {
        { "privateRSAKeyAttributes", SC_ASN1_STRUCT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
       { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_dsakey_i_p_attr[] = {
        { "path",       SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_dsakey_value_attr[] = {
        { "path",       SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
        { "pathProtected",SC_ASN1_STRUCT, SC_ASN1_CTX | 1 | SC_ASN1_CONS, 0, NULL, NULL},
        { NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_dsakey_attr[] = {
        { "value",      SC_ASN1_CHOICE, 0, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_prk_dsa_attr[] = {
        { "privateDSAKeyAttributes", SC_ASN1_STRUCT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_prkey[] = {
        { "privateRSAKey", SC_ASN1_PKCS15_OBJECT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
        { "privateDSAKey", SC_ASN1_PKCS15_OBJECT,  2 | SC_ASN1_CTX | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

int sc_pkcs15_ceres_decode_prkdf_entry(struct sc_pkcs15_card *p15card,
				       struct sc_pkcs15_object *obj,
				       const u8 ** buf, size_t *buflen)
{
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_pkcs15_ceres_decode_prkdf_entry\n");

        sc_context_t *ctx = p15card->card->ctx;
	struct sc_card *card = p15card->card; 
        struct sc_pkcs15_prkey_info info;
        int r;
        size_t usage_len = sizeof(info.usage);
        size_t af_len = sizeof(info.access_flags);
        struct sc_asn1_entry asn1_com_key_attr[6], asn1_com_prkey_attr[1];
        struct sc_asn1_entry asn1_rsakey_attr[4], asn1_prk_rsa_attr[2];
        struct sc_asn1_entry asn1_dsakey_attr[2], asn1_prk_dsa_attr[2],
                        asn1_dsakey_i_p_attr[2],
                        asn1_dsakey_value_attr[3];
        struct sc_asn1_entry asn1_prkey[3];
        struct sc_asn1_pkcs15_object rsa_prkey_obj = { obj, asn1_com_key_attr,
                                                       asn1_com_prkey_attr, asn1_rsakey_attr };
        struct sc_asn1_pkcs15_object dsa_prkey_obj = { obj, asn1_com_key_attr,
                                                       asn1_com_prkey_attr, asn1_dsakey_attr };

        sc_copy_asn1_entry(c_asn1_prkey, asn1_prkey);

        sc_copy_asn1_entry(c_asn1_prk_rsa_attr, asn1_prk_rsa_attr);
        sc_copy_asn1_entry(c_asn1_rsakey_attr, asn1_rsakey_attr);
        sc_copy_asn1_entry(c_asn1_prk_dsa_attr, asn1_prk_dsa_attr);
        sc_copy_asn1_entry(c_asn1_dsakey_attr, asn1_dsakey_attr);
        sc_copy_asn1_entry(c_asn1_dsakey_value_attr, asn1_dsakey_value_attr);
        sc_copy_asn1_entry(c_asn1_dsakey_i_p_attr,
                                        asn1_dsakey_i_p_attr);

        sc_copy_asn1_entry(c_asn1_com_prkey_attr, asn1_com_prkey_attr);
        sc_copy_asn1_entry(c_asn1_com_key_attr, asn1_com_key_attr);

        sc_format_asn1_entry(asn1_prkey + 0, &rsa_prkey_obj, NULL, 0);
        sc_format_asn1_entry(asn1_prkey + 1, &dsa_prkey_obj, NULL, 0);

        sc_format_asn1_entry(asn1_prk_rsa_attr + 0, asn1_rsakey_attr, NULL, 0);
        sc_format_asn1_entry(asn1_prk_dsa_attr + 0, asn1_dsakey_attr, NULL, 0);

        sc_format_asn1_entry(asn1_rsakey_attr + 0, &info.path, NULL, 0);
        sc_format_asn1_entry(asn1_rsakey_attr + 1, &info.modulus_length, NULL, 0);

        sc_format_asn1_entry(asn1_dsakey_attr + 0, asn1_dsakey_value_attr, NULL, 0);
        sc_format_asn1_entry(asn1_dsakey_value_attr + 0, &info.path, NULL, 0);
        sc_format_asn1_entry(asn1_dsakey_value_attr + 1, asn1_dsakey_i_p_attr, NULL, 0);
        sc_format_asn1_entry(asn1_dsakey_i_p_attr + 0, &info.path, NULL, 0);


        sc_format_asn1_entry(asn1_com_key_attr + 0, &info.id, NULL, 0);
        sc_format_asn1_entry(asn1_com_key_attr + 1, &info.usage, &usage_len, 0);
        sc_format_asn1_entry(asn1_com_key_attr + 2, &info.native, NULL, 0);
        sc_format_asn1_entry(asn1_com_key_attr + 3, &info.access_flags, &af_len, 0);
        sc_format_asn1_entry(asn1_com_key_attr + 4, &info.key_reference, NULL, 0);
	

        /* Fill in defaults */
        memset(&info, 0, sizeof(info));
        info.key_reference = -1;
        info.native = 1;

        r = sc_asn1_ceres_decode_choice(ctx, asn1_prkey, *buf, *buflen, buf, buflen);
        if (r == SC_ERROR_ASN1_END_OF_CONTENTS)
                return r;
        SC_TEST_RET(ctx, r, "ASN.1 decoding failed");
        if (asn1_prkey[0].flags & SC_ASN1_PRESENT) {
                obj->type = SC_PKCS15_TYPE_PRKEY_RSA;
        } else if (asn1_prkey[1].flags & SC_ASN1_PRESENT) {
                obj->type = SC_PKCS15_TYPE_PRKEY_DSA;
                /* If the value was indirect-protected, mark the path */
                if (asn1_dsakey_i_p_attr[0].flags & SC_ASN1_PRESENT)
                        info.path.type = SC_PATH_TYPE_PATH_PROT;
        } else {
                sc_error(ctx, "Neither RSA or DSA key in PrKDF entry.\n");
                SC_FUNC_RETURN(ctx, 0, SC_ERROR_INVALID_ASN1_OBJECT);
        }

	if (info.modulus_length == 128 || info.modulus_length == 1024 ){
		sc_path_t current_path_bak = DRVDATA(card)->current_path;
		
		/* we backup use_virtual_fs */
		int old_use_virtual_fs = ceres_is_virtual_fs_active(card);

		/* we want to use card without virtual fs */
		ceres_set_virtual_fs_state(card, 0);
				
		sc_path_t key_path;
		sc_format_path("3F003F11", &key_path);

 		r = ceres_select_file(card, &key_path, NULL);
  		SC_TEST_RET(card->ctx, r, "Seleccion Directorio Claves fallida");
		
		u8 data[1];
	  	sc_apdu_t  apdu;
 		u8        resbuf[256];

		data[0] = 0x14;
  		memset(&apdu, 0, sizeof(apdu));
  		apdu.cla = 0x90;
  		apdu.cse = SC_APDU_CASE_4_SHORT;
  		apdu.ins = (u8) 0x56;
  		apdu.p1 = (u8) 0x40; /* we must use only 0x80 or 0x40 in this command */
  		apdu.p2 = info.key_reference;
  		apdu.lc = 0x01;
  		apdu.data = data;
  		apdu.datalen = 0x01;
		apdu.resp = resbuf;
  		apdu.resplen = sizeof(resbuf);
		apdu.le = 256;
		r = ceres_transmit_apdu(card, &apdu);
  		SC_TEST_RET(card->ctx, r, "APDU transmit failed");

  		if((apdu.sw1==0x66)&&(apdu.sw2==0x88)) {
		    sc_error(card->ctx, "The securized message value is incorrect\n");
		    return SC_ERROR_UNKNOWN;
		}
		if(apdu.sw1==0x6A && (apdu.sw2==0x88 || apdu.sw2==0x80 || apdu.sw2==0x89)){
		    sc_error(card->ctx, "File/Key already exists!\n");
		    return SC_ERROR_OBJECT_ALREADY_EXISTS;
		  }
		if(apdu.sw1==0x62 && apdu.sw2==0x83) {
			sc_error(card->ctx, "Invalid file!\n");
			return SC_ERROR_INVALID_FILE;
	         }
		if(apdu.sw1==0x6A && apdu.sw2==0x84) {
		    sc_error(card->ctx, "Not enough memory!\n");
		    return SC_ERROR_NOT_ENOUGH_MEMORY;
		}



  		if (apdu.resplen == 256){
			info.modulus_length = 2048;
		}
		r = ceres_select_file(card, &current_path_bak, NULL);
		SC_TEST_RET(card->ctx, r, "Seleccion Diretorio fallida");

		ceres_set_virtual_fs_state(card, old_use_virtual_fs);

	}



        obj->data = malloc(sizeof(info));
        if (obj->data == NULL)
                SC_FUNC_RETURN(ctx, 0, SC_ERROR_OUT_OF_MEMORY);


        memcpy(obj->data, &info, sizeof(info));

	if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_pkcs15_ceres_decode_prkdf_entry\n");

        return 0;
}

int sc_pkcs15_ceres_encode_prkdf_entry(sc_context_t *ctx,
				       const struct sc_pkcs15_object *obj,
				       u8 **buf, size_t *buflen)
{
  if (ctx->debug) sc_debug(ctx, "Entering function sc_pkcs15_ceres_encode_prkdf_entry\n");

        struct sc_asn1_entry asn1_com_key_attr[6], asn1_com_prkey_attr[1];
        struct sc_asn1_entry asn1_rsakey_attr[4], asn1_prk_rsa_attr[2];
        struct sc_asn1_entry asn1_dsakey_attr[2], asn1_prk_dsa_attr[2],
                        asn1_dsakey_value_attr[3],
                        asn1_dsakey_i_p_attr[2];
        struct sc_asn1_entry asn1_prkey[3];
        struct sc_asn1_pkcs15_object rsa_prkey_obj = { (struct sc_pkcs15_object *) obj, asn1_com_key_attr,
                                                       asn1_com_prkey_attr, asn1_rsakey_attr };
        struct sc_asn1_pkcs15_object dsa_prkey_obj = { (struct sc_pkcs15_object *) obj, asn1_com_key_attr,
                                                       asn1_com_prkey_attr, asn1_dsakey_attr };
        struct sc_pkcs15_prkey_info *prkey =
                (struct sc_pkcs15_prkey_info *) obj->data;
        int r;
        size_t af_len, usage_len;

        sc_copy_asn1_entry(c_asn1_prkey, asn1_prkey);

        sc_copy_asn1_entry(c_asn1_prk_rsa_attr, asn1_prk_rsa_attr);
        sc_copy_asn1_entry(c_asn1_rsakey_attr, asn1_rsakey_attr);
        sc_copy_asn1_entry(c_asn1_prk_dsa_attr, asn1_prk_dsa_attr);
        sc_copy_asn1_entry(c_asn1_dsakey_attr, asn1_dsakey_attr);
        sc_copy_asn1_entry(c_asn1_dsakey_value_attr, asn1_dsakey_value_attr);
        sc_copy_asn1_entry(c_asn1_dsakey_i_p_attr, asn1_dsakey_i_p_attr);

        sc_copy_asn1_entry(c_asn1_com_prkey_attr, asn1_com_prkey_attr);
        sc_copy_asn1_entry(c_asn1_com_key_attr, asn1_com_key_attr);

        switch (obj->type) {
        case SC_PKCS15_TYPE_PRKEY_RSA:
                sc_format_asn1_entry(asn1_prkey + 0, &rsa_prkey_obj, NULL, 1);
                sc_format_asn1_entry(asn1_prk_rsa_attr + 0, asn1_rsakey_attr, NULL, 1);
                sc_format_asn1_entry(asn1_rsakey_attr + 0, &prkey->path, NULL, 1);
                sc_format_asn1_entry(asn1_rsakey_attr + 1, &prkey->modulus_length, NULL, 1);
                break;
        case SC_PKCS15_TYPE_PRKEY_DSA:
                sc_format_asn1_entry(asn1_prkey + 1, &dsa_prkey_obj, NULL, 1);
                sc_format_asn1_entry(asn1_prk_dsa_attr + 0, asn1_dsakey_value_attr, 0, 1);
                if (prkey->path.type != SC_PATH_TYPE_PATH_PROT) {
                        /* indirect: just add the path */
                        sc_format_asn1_entry(asn1_dsakey_value_attr + 0,
                                        &prkey->path, NULL, 1);
                } else {
                        /* indirect-protected */
                        sc_format_asn1_entry(asn1_dsakey_value_attr + 1,
                                        asn1_dsakey_i_p_attr, NULL, 1);
                        sc_format_asn1_entry(asn1_dsakey_i_p_attr + 0,
                                        &prkey->path, NULL, 1);
                }
                break;
        default:
                sc_error(ctx, "Invalid private key type: %X\n", obj->type);
                SC_FUNC_RETURN(ctx, 0, SC_ERROR_INTERNAL);
                break;
        }
        sc_format_asn1_entry(asn1_com_key_attr + 0, &prkey->id, NULL, 1);
        usage_len = sizeof(prkey->usage);
        sc_format_asn1_entry(asn1_com_key_attr + 1, &prkey->usage, &usage_len, 1);
        sc_format_asn1_entry(asn1_com_key_attr + 2, &prkey->native, NULL, 1);

        if (prkey->access_flags) {
                af_len = sizeof(prkey->access_flags);
                sc_format_asn1_entry(asn1_com_key_attr + 3, &prkey->access_flags, &af_len, 1);
        }
        if (prkey->key_reference >= 0)
                sc_format_asn1_entry(asn1_com_key_attr + 4, &prkey->key_reference, NULL, 1);


        r = sc_asn1_ceres_encode(ctx, asn1_prkey, buf, buflen);

  if (ctx->debug) sc_debug(ctx, "Leaving function sc_pkcs15_ceres_encode_prkdf_entry\n");

        return r;
}

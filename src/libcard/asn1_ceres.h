/*
 * asn1_ceres.c: ASN.1 Ceres decoding functions (DER)
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef ASN1_CERES_H
#define ASN1_CERES_H

int asn1_ceres_encode_path(sc_context_t *ctx, const sc_path_t *path,
                           u8 **buf, size_t *bufsize, int depth);

#endif /* ASN1_CERES_H */
	    

/*
 * pkcs15_cert.c: PKCS #15 Ceres certificate functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "../include/internal.h"
#include "../common/util.h"
#include <opensc/asn1.h>
#include <asn1_ceres.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ltdl.h>
#include <opensc/log.h>
#include <opensc/pkcs15.h>
#include "pkcs15_default.h"
#include <openssl/sha.h> 

static int parse_x509_cert(sc_context_t *ctx, const u8 *buf, size_t buflen, struct sc_pkcs15_cert *cert)
{
        int r;
        struct sc_algorithm_id pk_alg, sig_alg;
        sc_pkcs15_der_t pk = { NULL, 0 };
        struct sc_asn1_entry asn1_version[] = {
                { "version", SC_ASN1_INTEGER, SC_ASN1_TAG_INTEGER, 0, &cert->version, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry asn1_pkinfo[] = {
                { "algorithm",          SC_ASN1_ALGORITHM_ID,  SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, &pk_alg, NULL },
                { "subjectPublicKey",   SC_ASN1_BIT_STRING_NI, SC_ASN1_TAG_BIT_STRING, SC_ASN1_ALLOC, &pk.value, &pk.len },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry asn1_x509v3[] = {
                { "certificatePolicies",        SC_ASN1_OCTET_STRING, SC_ASN1_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
                { "subjectKeyIdentifier",       SC_ASN1_OCTET_STRING, SC_ASN1_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
                { "crlDistributionPoints",      SC_ASN1_OCTET_STRING, SC_ASN1_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL | SC_ASN1_ALLOC, &cert->crl, &cert->crl_len },
                { "authorityKeyIdentifier",     SC_ASN1_OCTET_STRING, SC_ASN1_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
                { "keyUsage",                   SC_ASN1_BOOLEAN, SC_ASN1_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry asn1_extensions[] = {
                { "x509v3",             SC_ASN1_STRUCT,    SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, asn1_x509v3, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry asn1_tbscert[] = {
                { "version",            SC_ASN1_STRUCT,    SC_ASN1_CTX | 0 | SC_ASN1_CONS, SC_ASN1_OPTIONAL, asn1_version, NULL },
                { "serialNumber",       SC_ASN1_OCTET_STRING, SC_ASN1_TAG_INTEGER, SC_ASN1_ALLOC, &cert->serial, &cert->serial_len },
                { "signature",          SC_ASN1_STRUCT,    SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
                { "issuer",             SC_ASN1_OCTET_STRING, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_ALLOC, &cert->issuer, &cert->issuer_len },
                { "validity",           SC_ASN1_STRUCT,    SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
                { "subject",            SC_ASN1_OCTET_STRING, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_ALLOC, &cert->subject, &cert->subject_len },
                { "subjectPublicKeyInfo",SC_ASN1_STRUCT,   SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, asn1_pkinfo, NULL },
                { "extensions",         SC_ASN1_STRUCT,    SC_ASN1_CTX | 3 | SC_ASN1_CONS, SC_ASN1_OPTIONAL, asn1_extensions, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry asn1_cert[] = {
                { "tbsCertificate",     SC_ASN1_STRUCT,    SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, asn1_tbscert, NULL },
                { "signatureAlgorithm", SC_ASN1_ALGORITHM_ID, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, &sig_alg, NULL },
                { "signatureValue",     SC_ASN1_BIT_STRING, SC_ASN1_TAG_BIT_STRING, 0, NULL, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        const u8 *obj;
        size_t objlen;

        memset(cert, 0, sizeof(*cert));
        obj = sc_asn1_verify_tag(ctx, buf, buflen, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS,
                                 &objlen);
        if (obj == NULL) {
                sc_error(ctx, "X.509 certificate not found\n");
                return SC_ERROR_INVALID_ASN1_OBJECT;
        }
        cert->data_len = objlen + (obj - buf);
        r = sc_asn1_ceres_decode(ctx, asn1_cert, obj, objlen, NULL, NULL);
        SC_TEST_RET(ctx, r, "ASN.1 parsing of certificate failed");

        cert->version++;

        cert->key.algorithm = pk_alg.algorithm;
        pk.len >>= 3;   /* convert number of bits to bytes */
        cert->key.data = pk;

        r = sc_pkcs15_decode_pubkey(ctx, &cert->key, pk.value, pk.len);
        if (r < 0)
                free(pk.value);
        sc_asn1_clear_algorithm_id(&pk_alg);
        sc_asn1_clear_algorithm_id(&sig_alg);

        return r;
}

int get_ckaid_from_certificate( sc_card_t *card, const u8 *data, const size_t data_size, sc_pkcs15_id_t *card_ckaid )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_cert cert;

  assert(data!=NULL && card_ckaid!=NULL);

  memset(&cert, 0, sizeof(struct sc_pkcs15_cert));
  /* parse certificate der data to get public key */
  r = parse_x509_cert(card->ctx, data, data_size, &cert);
  if(r)
    goto end;

  /* get card_ckaid from public key */
  if (cert.key.u.rsa.modulus.len<=0) {
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }
    
  SHA1(cert.key.u.rsa.modulus.data, cert.key.u.rsa.modulus.len, card_ckaid->value);
  card_ckaid->len = 0x14;

 end:
  return r;
}

int sc_card_pkcs15_read_certificate(struct sc_pkcs15_card *p15card,
                               const struct sc_pkcs15_cert_info *info,
                               struct sc_pkcs15_cert **cert_out)
{
        int r;
        struct sc_pkcs15_cert *cert;
        u8 *data = NULL;
        size_t len;

        assert(p15card != NULL && info != NULL && cert_out != NULL);
        SC_FUNC_CALLED(p15card->card->ctx, 1);

        if (info->path.len) {
                r = sc_pkcs15_read_file(p15card, &info->path, &data, &len, NULL);
                if (r)
                        return r;
        } else {
                sc_pkcs15_der_t copy;

                sc_der_copy(&copy, &info->value);
                data = copy.value;
                len = copy.len;
        }
        cert = (struct sc_pkcs15_cert *) malloc(sizeof(struct sc_pkcs15_cert));
        if (cert == NULL) {
                free(data);
                return SC_ERROR_OUT_OF_MEMORY;
        }
        memset(cert, 0, sizeof(struct sc_pkcs15_cert));
        if (parse_x509_cert(p15card->card->ctx, data, len, cert)) {
                free(data);
                free(cert);
                return SC_ERROR_INVALID_ASN1_OBJECT;
        }
        cert->data = data;
        *cert_out = cert;
        return 0;
}

static const struct sc_asn1_entry c_asn1_cred_ident[] = {
        { "idType",     SC_ASN1_INTEGER,      SC_ASN1_TAG_INTEGER, 0, NULL, NULL },
        { "idValue",    SC_ASN1_OCTET_STRING, SC_ASN1_TAG_OCTET_STRING, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_com_cert_attr[] = {
        { "iD",         SC_ASN1_PKCS15_ID, SC_ASN1_TAG_OCTET_STRING, 0, NULL, NULL },
        { "authority",  SC_ASN1_BOOLEAN,   SC_ASN1_TAG_BOOLEAN, SC_ASN1_OPTIONAL, NULL, NULL },
        { "identifier", SC_ASN1_STRUCT,    SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_x509_cert_value_choice[] = {
        { "path",       SC_ASN1_PATH,      SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
        { "direct",     SC_ASN1_OCTET_STRING, SC_ASN1_CTX | 0 | SC_ASN1_CONS, SC_ASN1_OPTIONAL | SC_ASN1_ALLOC, NULL, NULL },        { NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_x509_cert_attr[] = {
        { "value",      SC_ASN1_CHOICE, 0, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_type_cert_attr[] = {
  { "x509CertificateAttributes", SC_ASN1_CHOICE, 0, 0, NULL, NULL },
  { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_cert[] = {
        { "x509Certificate", SC_ASN1_PKCS15_OBJECT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

int sc_pkcs15_ceres_decode_cdf_entry(struct sc_pkcs15_card *p15card,
                               struct sc_pkcs15_object *obj,
                               const u8 ** buf, size_t *buflen)
{
if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_pkcs15_ceres_decode_cdf_entry\n");

        sc_context_t *ctx = p15card->card->ctx;
        struct sc_pkcs15_cert_info info;
        struct sc_asn1_entry    asn1_cred_ident[3], asn1_com_cert_attr[4],
                                asn1_x509_cert_attr[2], asn1_type_cert_attr[2],
                                asn1_cert[2], asn1_x509_cert_value_choice[3];
        struct sc_asn1_pkcs15_object cert_obj = { obj, asn1_com_cert_attr, NULL,
						  asn1_type_cert_attr };
        sc_pkcs15_der_t *der = &info.value;
        u8 id_value[128];
        int id_type;
        size_t id_value_len = sizeof(id_value);
        int r = SC_SUCCESS;

        sc_copy_asn1_entry(c_asn1_cred_ident, asn1_cred_ident);
        sc_copy_asn1_entry(c_asn1_com_cert_attr, asn1_com_cert_attr);
        sc_copy_asn1_entry(c_asn1_x509_cert_attr, asn1_x509_cert_attr);
        sc_copy_asn1_entry(c_asn1_x509_cert_value_choice, asn1_x509_cert_value_choice);
        sc_copy_asn1_entry(c_asn1_type_cert_attr, asn1_type_cert_attr);
        sc_copy_asn1_entry(c_asn1_cert, asn1_cert);

        sc_format_asn1_entry(asn1_cred_ident + 0, &id_type, NULL, 0);
        sc_format_asn1_entry(asn1_cred_ident + 1, &id_value, &id_value_len, 0);
        sc_format_asn1_entry(asn1_com_cert_attr + 0, &info.id, NULL, 0);
        sc_format_asn1_entry(asn1_com_cert_attr + 1, &info.authority, NULL, 0);
        sc_format_asn1_entry(asn1_com_cert_attr + 2, asn1_cred_ident, NULL, 0);
        sc_format_asn1_entry(asn1_x509_cert_attr + 0, asn1_x509_cert_value_choice, NULL, 0);
        sc_format_asn1_entry(asn1_x509_cert_value_choice + 0, &info.path, NULL, 0);
        sc_format_asn1_entry(asn1_x509_cert_value_choice + 1, &der->value, &der->len, 0);
        sc_format_asn1_entry(asn1_type_cert_attr + 0, asn1_x509_cert_attr, NULL, 0);
        sc_format_asn1_entry(asn1_cert + 0, &cert_obj, NULL, 0);

        /* Fill in defaults */
        memset(&info, 0, sizeof(info));
        info.authority = 0;

        r = sc_asn1_ceres_decode(ctx, asn1_cert, *buf, *buflen, buf, buflen);
        /* In case of error, trash the cert value (direct coding) */
        if (r < 0 && der->value)
                free(der->value);
        if (r == SC_ERROR_ASN1_END_OF_CONTENTS)
                return r;
        SC_TEST_RET(ctx, r, "ASN.1 decoding failed");
        obj->type = SC_PKCS15_TYPE_CERT_X509;
        obj->data = malloc(sizeof(info));
        if (obj->data == NULL)
                SC_FUNC_RETURN(ctx, 0, SC_ERROR_OUT_OF_MEMORY);
        memcpy(obj->data, &info, sizeof(info));
   
	if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_pkcs15_ceres_decode_cdf_entry\n");

        return r;
}

int sc_pkcs15_ceres_encode_cdf_entry(sc_context_t *ctx,
                               const struct sc_pkcs15_object *obj,
                               u8 **buf, size_t *bufsize)
{
  if (ctx->debug) sc_debug(ctx, "Entering function sc_pkcs15_ceres_encode_cdf_entry\n");

        struct sc_asn1_entry    asn1_com_cert_attr[4], asn1_type_cert_attr[2],
                                asn1_cert[2], asn1_x509_cert_value_choice[3];
        struct sc_pkcs15_cert_info *infop = (sc_pkcs15_cert_info_t *) obj->data;
        sc_pkcs15_der_t *der = &infop->value;
        struct sc_asn1_pkcs15_object cert_obj = { (struct sc_pkcs15_object *) obj,
                                                        asn1_com_cert_attr, NULL,
                                                        asn1_type_cert_attr };


	struct sc_pkcs15_cert cert;


	u8 * issuer = NULL;
	int issuer_len = 0;
	u8 * subject = NULL;
	int subject_len = 0;
	u8 * serial = NULL;
	int serial_len = 0;
	u8 * buf_tmp = NULL;
	int buf_tmp_len = 0;
	u8 * tmp = NULL;
	int tmp_len = 0;
	u8 * buf_part1 = NULL;
	int buf_part1_len = 0;
	u8 * buf_part2 = NULL;
	int buf_part2_len = 0;
	int split_len = 1;
	u8 * aux_split = NULL;
	tlv_t tlv_tmp;
	int tlv_size = 0;
        int r;

        sc_copy_asn1_entry(c_asn1_com_cert_attr, asn1_com_cert_attr);
        sc_copy_asn1_entry(c_asn1_x509_cert_value_choice, asn1_x509_cert_value_choice);
	sc_copy_asn1_entry(c_asn1_type_cert_attr, asn1_type_cert_attr);
        sc_copy_asn1_entry(c_asn1_cert, asn1_cert);

        sc_format_asn1_entry(asn1_com_cert_attr + 0, (void *) &infop->id, NULL, 1);
        if (infop->authority)
                sc_format_asn1_entry(asn1_com_cert_attr + 1, (void *) &infop->authority, NULL, 1);

	if (infop->path.len || !der->value) {
                sc_format_asn1_entry(asn1_x509_cert_value_choice + 0, &infop->path, NULL, 1);
        } else {
                sc_format_asn1_entry(asn1_x509_cert_value_choice + 1, der->value, &der->len, 1);
        }

       	sc_format_asn1_entry(asn1_type_cert_attr + 0, &asn1_x509_cert_value_choice, NULL, 1);
        sc_format_asn1_entry(asn1_cert + 0, (void *) &cert_obj, NULL, 1);

        r = sc_asn1_ceres_encode(ctx, asn1_cert, buf, bufsize);
	if (r != SC_SUCCESS)
		goto end;

	/* We need to add CKA_ISSUER, CKA_SUBJECT and CKA_SERIAL_NUMBER */
	/* Parse cert to obtain info */

	r = parse_x509_cert(ctx, (const u8 *) infop->value.value, infop->value.len, &cert);
	if (r != SC_SUCCESS)
		goto end;

	issuer_len = cert.issuer_len;
	issuer = calloc(1, issuer_len);
	if (issuer == NULL) {
		r = SC_ERROR_OUT_OF_MEMORY;
		goto end;
	}

	memcpy(issuer, cert.issuer, issuer_len);


	subject_len = cert.subject_len;
	subject = calloc(1, subject_len);
	if (subject == NULL) {
		r = SC_ERROR_OUT_OF_MEMORY;
		goto end;
	}

	memcpy(subject, cert.subject, subject_len);


	serial_len = cert.serial_len;
	serial = calloc(1, serial_len);
	if (serial == NULL) {
		r = SC_ERROR_OUT_OF_MEMORY;
		goto end;
	}

	memcpy(serial, cert.serial, serial_len);


	/* Add tlv headers to CKA_ISSUER  (i.e. A0 81 xx   30 81 xx) */
	memset(&tlv_tmp, 0, sizeof(tlv_t));
	r = buf2tlv (0x30, issuer, issuer_len, &tlv_tmp);
	if (r != SC_SUCCESS)
		goto end;

	buf_tmp_len = tlv2buf_normal (&tlv_tmp, &buf_tmp);
	free_tlv(&tlv_tmp);
	
        r = buf2tlv (0xA0, buf_tmp, buf_tmp_len, &tlv_tmp);
        if (r != SC_SUCCESS)
                goto end;

	if (buf_tmp!=NULL) {
		memset(buf_tmp, 0, buf_tmp_len);
		free(buf_tmp);
		buf_tmp = NULL;
	}

	buf_tmp_len = tlv2buf_normal (&tlv_tmp, &buf_tmp);
	free_tlv(&tlv_tmp);

	tmp = (u8 *) realloc(tmp, buf_tmp_len + tmp_len);
	memcpy(tmp + tmp_len, buf_tmp, buf_tmp_len);
	tmp_len += buf_tmp_len;


	/* Add tlv headers to CKA_SUBJECT  (i.e. A1 81 xx   30 81 xx) */
	r = buf2tlv (0x30, subject, subject_len, &tlv_tmp);
	if (r != SC_SUCCESS)
		goto end;

	if (buf_tmp!=NULL) {
		memset(buf_tmp, 0, buf_tmp_len);
		free(buf_tmp);
		buf_tmp = NULL;
	}

	buf_tmp_len = tlv2buf_normal (&tlv_tmp, &buf_tmp);
	free_tlv(&tlv_tmp);

        r = buf2tlv (0xA1, buf_tmp, buf_tmp_len, &tlv_tmp);
        if (r != SC_SUCCESS)
                goto end;

	if (buf_tmp!=NULL) {
		memset(buf_tmp, 0, buf_tmp_len);
		free(buf_tmp);
		buf_tmp = NULL;
	}

	buf_tmp_len = tlv2buf_normal (&tlv_tmp, &buf_tmp);
	free_tlv(&tlv_tmp);

	tmp = (u8 *) realloc(tmp, buf_tmp_len + tmp_len);
	memcpy(tmp + tmp_len, buf_tmp, buf_tmp_len);
	tmp_len += buf_tmp_len;


	/* Add tlv headers to CKA_SERIAL_NUMBER  (i.e. 02 03   02 01   xx) */
	r = buf2tlv (0x02, serial, serial_len, &tlv_tmp);
	if (r != SC_SUCCESS)
		goto end;

	if (buf_tmp!=NULL) {
		memset(buf_tmp, 0, buf_tmp_len);
		free(buf_tmp);
		buf_tmp = NULL;
	}


	buf_tmp_len = tlv2buf_normal (&tlv_tmp, &buf_tmp);
	free_tlv(&tlv_tmp);

	tmp = (u8 *) realloc(tmp, buf_tmp_len + tmp_len);
	memcpy(tmp + tmp_len, buf_tmp, buf_tmp_len);
	tmp_len += buf_tmp_len;

	/* Recalc new buf length */
	/* Split buf at tag A1 */
	
	aux_split = *buf + split_len;
	if (*aux_split < 0x80){
		split_len += 2;
		aux_split = *buf + split_len;	/* First sequence */
		split_len += *aux_split + 2;
		aux_split = *buf + split_len;	/* Second sequence */
		split_len += *aux_split + 1;

		/* Split without TagLength, only value */

		/* Add tmp buffer at end of buf_part2  */
		buf_part2_len = *bufsize - split_len + tmp_len - 2;
		buf_part2 = calloc(1, buf_part2_len);
		if(!buf_part2) {
			r = SC_ERROR_OUT_OF_MEMORY;
			goto end;
		}
		memcpy(buf_part2, *buf + split_len + 2, buf_part2_len - tmp_len);
		memcpy(buf_part2 + buf_part2_len - tmp_len, tmp, tmp_len);

		/* Calc new length for A1 tag  */
		r = buf2tlv (0xA1, buf_part2, buf_part2_len, &tlv_tmp);
		if (r != SC_SUCCESS)
			goto end;

		buf_part2_len = tlv2buf_normal (&tlv_tmp, &buf_part2);
		free_tlv(&tlv_tmp);


		/* Add buf_part2 at end of buf_part1  */
		buf_part1_len = buf_part2_len + split_len - 2;
		buf_part1 = calloc(1, buf_part1_len);
		if(!buf_part1) {
			r = SC_ERROR_OUT_OF_MEMORY;
			goto end;
		}
		memcpy(buf_part1, *buf + 2, split_len - 2);
		memcpy(buf_part1 + split_len - 2, buf_part2, buf_part2_len);

		/* Calc new length for 30 tag  */
		r = buf2tlv (0x30, buf_part1, buf_part1_len, &tlv_tmp);
		if (r != SC_SUCCESS)
			goto end;

		buf_part1_len = tlv2buf_normal (&tlv_tmp, &buf_part1);
		free_tlv(&tlv_tmp);
	}
	else {
		//We have > 0x80 so we have to jump 2 or more
		split_len += (*aux_split - 0x80) +2;
		aux_split = *buf + split_len;	/* First sequence */
		
		if ((*aux_split) >= 0x80){
		  if (*aux_split == 0x82){
		    split_len += *(aux_split+1)<<8;
		    split_len += *(aux_split+2)+2; //We add 2 because its the header
		    tlv_size=2;
		  }
		  else{
		    split_len += *(aux_split + 1)+2; //same 2 here
		    tlv_size=1;
		  }
		  aux_split = *buf + split_len;	/* Second sequence */
		  split_len += *(aux_split + 1); //CHECK IT!!! --> WHY THIS WORK???
		}
		else{
			split_len += *(aux_split) + 2;
			aux_split = *buf + split_len -1;
			split_len += (*(aux_split +1))-1;
		}
				/* Split without TagLength, only value */

		/* Add tmp buffer at end of buf_part2  */
		buf_part2_len = *bufsize - split_len + tmp_len - 4;
		buf_part2 = calloc(1, buf_part2_len);
		if(!buf_part2) {
			r = SC_ERROR_OUT_OF_MEMORY;
			goto end;
		}
		memcpy(buf_part2, *buf + split_len + 4, buf_part2_len - tmp_len );
		memcpy(buf_part2 + buf_part2_len - tmp_len, tmp, tmp_len);
		/* Calc new length for A1 tag  */
		r = buf2tlv (0xA1, buf_part2, buf_part2_len, &tlv_tmp);
		if (r != SC_SUCCESS)
			goto end;

		buf_part2_len = tlv2buf_normal (&tlv_tmp, &buf_part2);
		free_tlv(&tlv_tmp);


		/* Add buf_part2 at end of buf_part1  */
		if(tlv_size==2)
		  buf_part1_len = buf_part2_len + split_len;
		else
		  buf_part1_len = buf_part2_len + split_len -1;
		
		buf_part1 = calloc(1, buf_part1_len);
		if(!buf_part1) {
			r = SC_ERROR_OUT_OF_MEMORY;
			goto end;
		}
		if(tlv_size==2){
		  memcpy(buf_part1, *buf + (2+2), split_len);
		  memcpy(buf_part1 + split_len, buf_part2, buf_part2_len);
		  buf_part1_len = buf_part1_len+1;
		}
		else{
		  memcpy(buf_part1, *buf + (2+1), split_len - 1);
		  memcpy(buf_part1 + split_len - 1, buf_part2, buf_part2_len);
		  //buf_part1_len++;
		}
		/* Calc new length for 30 tag  */
		r = buf2tlv (0x30, buf_part1, buf_part1_len, &tlv_tmp);
		if (r != SC_SUCCESS)
			goto end;

		buf_part1_len = tlv2buf_normal (&tlv_tmp, &buf_part1);
		free_tlv(&tlv_tmp);

	}
	
	/* Add CKA's needed to pkcs15_cert */
	*buf = (u8 *) realloc(*buf, buf_part1_len);
	memcpy(*buf, buf_part1, buf_part1_len);
	*bufsize = buf_part1_len;
end:

	if (tmp!=NULL) {
		memset(tmp, 0, tmp_len);
		free(tmp);
		tmp = NULL;
	}
	if (issuer!=NULL) {
		memset(issuer, 0, issuer_len);
		free(issuer);
		issuer = NULL;
	}
	if (subject!=NULL) {
		memset(subject, 0, subject_len);
		free(subject);
		subject = NULL;
	}
	if (serial!=NULL) {
		memset(serial, 0, serial_len);
		free(serial);
		serial = NULL;
	}
	if (buf_tmp!=NULL) {
		memset(buf_tmp, 0, buf_tmp_len);
		free(buf_tmp);
		buf_tmp = NULL;
	}
	if (buf_part1!=NULL) {
		memset(buf_part1, 0, buf_part1_len);
		free(buf_part1);
		buf_part1 = NULL;
	}
	if (buf_part2!=NULL) {
		memset(buf_part2, 0, buf_part2_len);
		free(buf_part2);
		buf_part2 = NULL;
	}
	aux_split = NULL;

	if (ctx->debug) sc_debug(ctx, "Leaving function sc_pkcs15_ceres_encode_cdf_entry\n");
        return r;
}

int get_real_certificate_length( struct sc_pkcs15_card *p15card,
				 struct sc_pkcs15_cert_info *cert_info ) 
{
  int r = SC_SUCCESS, ii, jj;
  u8 *data = NULL;
  size_t len, nbytes, real_len = 0;
  sc_card_t *card = p15card->card;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function get_real_certificate_length\n"); 

  assert(p15card != NULL && cert_info != NULL);
  SC_FUNC_CALLED(p15card->card->ctx, 1);
  
  if (cert_info->path.len) {
    /* Inhabilitate the use of pkcs15 cache in order to 
       execute a read binary directly to the card */
    DRVDATA(card)->use_pkcs15_cache=0;
    r = sc_pkcs15_read_file(p15card, &cert_info->path, &data, &len, NULL);
    /* Restore the use of pkcs15 cache */
    DRVDATA(card)->use_pkcs15_cache=1;
    if (r) {
      sc_error(card->ctx, "Error on pkcs15_read_file return r=%d\n", r);
      goto grcl_err;
    }
    
    /* data is certificate file coded in ASN1 */
    /* 0x30 0x8X or 0xlength ... ... */
    ii=1;
    nbytes = data[ii] & 0x7f;
    if (data[ii++] & 0x80) {
      unsigned int a = 0;
      if (nbytes > 4)
	goto grcl_err;
      for (jj = 0; jj < nbytes; jj++) {
	a <<= 8;
	a |= data[ii];
	ii++;
      }
      real_len = a;
    }
    /* That is asn1 struct length + length bytes + header (0x30 0x8X) */
    cert_info->path.count = real_len + nbytes + 2;
    if (DRVDATA(card)->last_file_selected==NULL) {
      r = SC_ERROR_FILE_NOT_FOUND;
      sc_error(card->ctx, "Last file selected is NULL\n");
      goto grcl_err;
    }

    /* we add certificate file info if we have not got yet */
    r = set_cert_file_path( card, &(DRVDATA(card)->last_file_selected->path) );
    if (r!=SC_SUCCESS && r!=SC_ERROR_FILE_ALREADY_EXISTS) 
	goto grcl_err;
    if (r != SC_ERROR_FILE_ALREADY_EXISTS) {
      r = set_uncompressed_len( card, 
				&(DRVDATA(card)->last_file_selected->path),
				cert_info->path.count );
      if (r!=SC_SUCCESS)
	goto grcl_err;      
      r = set_cert_stored_on_card( card, &(DRVDATA(card)->last_file_selected->path), 1 );
      if (r!=SC_SUCCESS)
	goto grcl_err;
    }
  } else {
    goto grcl_err;
  }
  
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function get_real_certificate_length\n");
  return r;

 grcl_err:
  sc_error(card->ctx, "Error while reading certificate file\n");
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function get_real_certificate_length\n");
  return r;
}

/*!
 * \file file_compression.h
 * \brief File compression functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef FILE_COMPRESSION_H
#define FILE_COMPRESSION_H

#include <opensc/opensc.h>

int file_uncompress_data(struct sc_card *card, u8 * data, size_t length, u8 **uncompressed_data, size_t *uncompressed_data_length );

int file_compress_data(struct sc_card *card, 
		       u8 * uncompressed_data, size_t uncompressed_data_length, 
		       u8 **compressed_data, size_t *compressed_data_length );

#endif /* FILE_COMPRESSION_H */


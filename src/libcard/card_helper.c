/*!
 * \file card_helper.c
 * \brief Card helper routines
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "card_helper.h"
#include "base_card.h"
#include <opensc/log.h>
#include <assert.h>
#include <string.h>
#include "pkcs15_default.h"
#include "card_sync.h"
#include "../common/util.h"


int ceres_helper_read_file(sc_card_t *card, const sc_path_t *path, u8 **buffer, size_t *length)
{
  int r = SC_SUCCESS;
  sc_file_t *file = NULL;
  unsigned char *card_data = NULL;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  SC_FUNC_CALLED(card->ctx, 1);
  
  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

  if(!buffer || !length) {
    r = SC_ERROR_INVALID_ARGUMENTS;
    goto end;
  }

  if(*buffer) {
    free(*buffer);
    *buffer = NULL;
  }

  /* get file */
  r = ceres_select_file(card, path, &file);
  if(r != SC_SUCCESS)
    goto end;

  if(file->size <= 0) {
    r = SC_ERROR_FILE_TOO_SMALL;
    goto end;
  }
  
  card_data = malloc(file->size);
  if(!card_data) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto end;
  }

  r = sc_read_binary(card, 0, card_data, file->size, 0);
  if(r < 0)
    goto end;

  *buffer = card_data;
  card_data = NULL;
  *length = r;
  r = SC_SUCCESS;

 end:
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  if(file) {
    sc_file_free(file);
    file = NULL;
  }
  
  if(card_data) {
    free(card_data);
    card_data = NULL;
  }


  SC_FUNC_RETURN(card->ctx, 1, r); 
}

int ceres_helper_update_file(sc_card_t *card, const sc_path_t *path, u8 *buffer, size_t length)
{
  int r = SC_SUCCESS;
  sc_file_t *file = NULL;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  SC_FUNC_CALLED(card->ctx, 1);
  
  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

  if(!buffer || length<=0) {
    r = SC_ERROR_INVALID_ARGUMENTS;
    goto chuf_end;
  }

  /* get file */
  r = ceres_select_file(card, path, &file);
  if(r != SC_SUCCESS)
    goto chuf_end;
  
  if(file->size <= 0) {
    r = SC_ERROR_FILE_TOO_SMALL;
    goto chuf_end;
  }
  
  if (file->size<length) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto chuf_end;
  }

  r = sc_update_binary(card, 0, buffer, length, 0);
  if(r < 0)
    goto chuf_end;
  if (r == length)
    r = SC_SUCCESS;

 chuf_end:
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  if(file) {
    sc_file_free(file);
    file = NULL;
  }

 SC_FUNC_RETURN(card->ctx, 1, r); 
}

/* Auxiliar function to convert and increment by 1 from char* (37,30,30,30) to sc_path_t (7001) */
void decode_inc_path_aux(const u8* buf, const int buf_len, sc_path_t* path)
{
  char _path[buf_len];  /* 37 30 30 3x  */
  u8 val_u, val_d;
  
  memcpy (_path, buf, buf_len);
  
  val_u = _path[buf_len - 1];
  val_d = _path[buf_len - 2];

  /* Transform from char to correct hex value */
  if (val_u <= 0x39)
    val_u -= 0x30;
  else if (val_u <= 0x46)
    val_u -= 0x37;
  else
    val_u -= 0x57;

  if (val_d <= 0x39)
    val_d -= 0x30;
  else if (val_d <= 0x46)
    val_d -= 0x37;
  else
    val_d -= 0x57;

  /* Increment by 1 */
  val_u++;

  if (val_u == 0x0a)
    _path[buf_len - 1] = 0x41;
  else if (val_u == 0x10) {
    _path[buf_len - 1] = 0x00;

    /* Cert file id never will be greater than 70FF */
    val_d++;
    if (val_d == 0x0a)
      _path[buf_len - 2] = 0x41;
    else 
      _path[buf_len - 2]++;
  }
  else 
    _path[buf_len - 1]++;

  sc_format_path(_path, path);
}


void encode_path_aux(const sc_path_t path, u8* id_buf, int id_buf_len)
{
  u8 val_u, val_d;
  val_u = val_d = 0x00;
  int i, j = 0;


  /* Processing path byte to byte '70' '01' */
  for (i = 0; i < 2; i++) {

    val_u = path.value[i];

    /* Transform to digits '7' '0' */
    while (val_u > 0x0F) {
      val_d++;
      val_u -= 0x10;
    }
  
    /* Transform to ASCII */
    if (val_u < 0x0a)
      val_u += 0x30;
    else
      val_u += 0x37;

    if (val_d < 0x0a)
      val_d += 0x30;
    else
      val_d += 0x37;

    id_buf[j] = val_d;
    j++;
    id_buf[j] = val_u;
    j++;

    val_u = 0x00;
    val_d = 0x00;
  } 

}

int ceres_helper_create_data_object_file(sc_card_t *card, struct _virtual_file_t *virtual_file, 
				 size_t fdata_object_len, struct _virtual_file_t **data_object_virtual_file)
{
  /* At this moment we won't reuse Unused Space. May be in future */
  int r = SC_SUCCESS;
  sc_path_t fdata_object_path;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */
  int id_buf_len = 0;
  u8 *buf=NULL, *id_buf=NULL;

  assert(card!=NULL && virtual_file!=NULL && data_object_virtual_file!=NULL);

  SC_FUNC_CALLED(card->ctx, 1);
  
  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

  if(*data_object_virtual_file) {
    virtual_file_free(*data_object_virtual_file);
    *data_object_virtual_file = NULL;
  }

  memset(&fdata_object_path, 0, sizeof(struct sc_path));

  sc_path_t id_path;
  /* move to ID EF to obtain a new file id */
  sc_format_path("3F0060A1", &id_path);
  r = sc_select_file(card, &id_path, NULL);
  if(r != SC_SUCCESS)
    goto end;
      
  /* read last file id  */
  id_buf_len = 4;
  id_buf = calloc (1, id_buf_len);
  id_buf_len = sc_read_binary(card, 0, id_buf, id_buf_len, 0); /* Coded in ASCII 37 30 30 3x */
  
  sc_path_t temp_path;
  /* move to Data Objects DF */
  sc_format_path("3F006081", &temp_path);
  r = sc_select_file(card, &temp_path, NULL);
  if(r != SC_SUCCESS)
    goto end;

  /* we start at last file ID used + 1 */
  decode_inc_path_aux(id_buf, id_buf_len, &fdata_object_path);

  do {
    r = ceres_create_data_object_file( card, &fdata_object_path, fdata_object_len );
    if (r == SC_ERROR_OBJECT_ALREADY_EXISTS) {
      fdata_object_path.value[1]++;	
    }
    if(r!=SC_SUCCESS && r!=SC_ERROR_OBJECT_ALREADY_EXISTS)
      goto end;
  } while (r!=SC_SUCCESS);

  /* update last file id  */
  memset (id_buf, 0, id_buf_len);
  encode_path_aux(fdata_object_path, id_buf, id_buf_len);
  r = sc_select_file(card, &id_path, NULL);
  if(r != SC_SUCCESS)
    goto end;

  r = sc_update_binary(card, 0, id_buf, id_buf_len, 0); /* Coded in ASCII 37 30 30 3x */
  if(r < 0)
    goto end;
 

  sc_append_path(&temp_path, &fdata_object_path);
  sc_path_set_ceres(&fdata_object_path, temp_path.type, temp_path.value, temp_path.len, temp_path.index, fdata_object_len);
  r = SC_SUCCESS;
  
  /* create data object file into vfs */
  r = virtual_fs_append_new_virtual_file( DRVDATA(card)->virtual_fs, 
					  &fdata_object_path, 
					  virtual_file->data, 
					  virtual_file->data_size, 
					  virtual_file->data_size, 
					  1, 
					  virtual_file_sync_state_synced, 
					  ceres_sync_card_to_virtual_fs_data_object_file_callback, 
					  virtual_file_sync_state_sync_pending, 
					  ceres_sync_virtual_fs_to_card_data_object_file_callback );
  if(r != SC_SUCCESS)
    goto end;

  /* retrieve just created virtual file */
  *data_object_virtual_file = virtual_fs_find_by_path( DRVDATA(card)->virtual_fs, &fdata_object_path );

end: 
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  if(buf) {
    free(buf);
    buf=NULL;
  }
  if (id_buf) {
    free(id_buf);
    id_buf=NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r); 
}


int ceres_helper_create_cert_file(sc_card_t *card, struct _virtual_file_t *virtual_file, 
				 size_t fcert_len, struct _virtual_file_t **certificate_virtual_file)
{
  int r = SC_SUCCESS;
  sc_path_t fcert_path, unusedspace_path;
  sc_pkcs15_unusedspace_t *unused_space=NULL;
  sc_pkcs15_card_t *temp_p15card = NULL;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */
  int id_buf_len = 0;
  u8 *buf=NULL, *usf_buf=NULL, *id_buf=NULL;
  size_t buflen=0;
  sc_file_t *unusedspace_file=NULL;

  assert(card!=NULL && virtual_file!=NULL && certificate_virtual_file!=NULL);

  SC_FUNC_CALLED(card->ctx, 1);
  
  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

  if(*certificate_virtual_file) {
    virtual_file_free(*certificate_virtual_file);
    *certificate_virtual_file = NULL;
  }

  memset(&fcert_path, 0, sizeof(struct sc_path));

  /* 2. Look for a suitable file on UnusedSpace struct
     which fits the final certificate file size.
     2.1 If found, take file's path to reuse it.
     2.2 If not, create a file on Certificate Directory
         with final certificate len as its size
  */
    
  /* we create a fake p15card structure */
  temp_p15card = sc_pkcs15_card_new();
  temp_p15card->card = card;

  r = sc_get_unusedspace( temp_p15card );
  if (r!=SC_SUCCESS)
    goto chccf_end;
   
  r = sc_find_free_unusedspace( temp_p15card, fcert_len, &unused_space );
  if (r!=SC_SUCCESS)
    goto chccf_end;

  if(unused_space) {
    /* we got a path */
    r = sc_path_set_ceres ( &fcert_path, 
		      unused_space->path.type, 
		      unused_space->path.value,
		      unused_space->path.len,
		      unused_space->path.index,
		      unused_space->path.count );
    if (r!=SC_SUCCESS)
      goto chccf_end;
  } else {           
  
    sc_path_t id_path;
    /* move to ID EF to obtain a new cert file id */
    sc_format_path("3F0060A1", &id_path);
    r = sc_select_file(card, &id_path, NULL);
    if(r != SC_SUCCESS)
      goto chccf_end;
      
    /* read last cert file id  */
    id_buf_len = 4;
    id_buf = calloc (1, id_buf_len);
    id_buf_len = sc_read_binary(card, 0, id_buf, id_buf_len, 0); /* Coded in ASCII 37 30 30 3x */
   
    sc_path_t temp_path;
    /* move to certificate DF */
    sc_format_path("3F006061", &temp_path);
    r = sc_select_file(card, &temp_path, NULL);
    if(r != SC_SUCCESS)
      goto chccf_end;

    /* we start at last file ID used + 1 */
    decode_inc_path_aux(id_buf, id_buf_len, &fcert_path);

    do {
      r = ceres_create_cert_file( card, &fcert_path, fcert_len );
      if (r == SC_ERROR_OBJECT_ALREADY_EXISTS) {
	fcert_path.value[1]++;	
      }
      if(r!=SC_SUCCESS && r!=SC_ERROR_OBJECT_ALREADY_EXISTS)
	goto chccf_end;
    } while (r!=SC_SUCCESS);

    /* update last cert file id  */
    memset (id_buf, 0, id_buf_len);
    encode_path_aux(fcert_path, id_buf, id_buf_len);
    r = sc_select_file(card, &id_path, NULL);
    if(r != SC_SUCCESS)
      goto chccf_end;

    r = sc_update_binary(card, 0, id_buf, id_buf_len, 0); /* Coded in ASCII 37 30 30 3x */
    if(r < 0)
      goto chccf_end;
 

    sc_append_path(&temp_path, &fcert_path);
    sc_path_set_ceres(&fcert_path, temp_path.type, temp_path.value, temp_path.len, temp_path.index, fcert_len);
    r = SC_SUCCESS;
  }    

  /* create certificate file into vfs */
  r = virtual_fs_append_new_virtual_file( DRVDATA(card)->virtual_fs, 
					  &fcert_path, 
					  virtual_file->data, 
					  virtual_file->data_size, 
					  virtual_file->data_size, 
					  1, 
					  virtual_file_sync_state_synced, 
					  ceres_sync_card_to_virtual_fs_certificate_file_callback, 
					  virtual_file_sync_state_sync_pending, 
					  ceres_sync_virtual_fs_to_card_certificate_file_callback );
  if(r != SC_SUCCESS)
    goto chccf_end;

  /* retrieve just created virtual file */
  *certificate_virtual_file = virtual_fs_find_by_path( DRVDATA(card)->virtual_fs, &fcert_path );

 chccf_end:
  if (unused_space) {
    /* Delete UnusedSpace object if reused 
     * and also frees reserved memory
     */
    sc_pkcs15_remove_unusedspace(temp_p15card, unused_space);

    /* It is necessary to write unusedspace DF changes to card */
    if(buf) {
      memset(buf,0, buflen);
      free(buf);
      buf=NULL;
      buflen=0;
    }
	
    /* Is necessary write to the card the changes of unusedspace file */
    if (temp_p15card->unusedspace_list) {
      r = ceres_pkcs15_encode_unusedspace(temp_p15card->card->ctx, temp_p15card, &buf, &buflen);
      if (r < 0)
        goto end;
    }
   	
    memset(&unusedspace_path, 0, sizeof(struct sc_path));
    sc_format_path("3F0050155033", &unusedspace_path);

    r = sc_select_file(card, &unusedspace_path, &unusedspace_file);
    if(r!=SC_SUCCESS)
      goto end;
    if (unusedspace_file) {
      if (unusedspace_file->size<buflen) {
        r = SC_ERROR_OUT_OF_MEMORY;
        goto end;
      }
      usf_buf = calloc(1, unusedspace_file->size);
      if (!usf_buf) {
        r = SC_ERROR_OUT_OF_MEMORY;
        goto end;
      }
      /* copies encoded unused space and set to zeroes
         all other memory */
      memcpy(usf_buf, buf, buflen);

    } else {
      /* unused space file not created */
      r = SC_ERROR_OUT_OF_MEMORY;
      goto end;
    }

    r = sc_update_binary(temp_p15card->card, 0, usf_buf, unusedspace_file->size, 0);
    if(r < 0)
      goto end;
    if (r == unusedspace_file->size)
      r = SC_SUCCESS;
  }
end: 
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  if(buf) {
    free(buf);
    buf=NULL;
  }
  if (usf_buf) {
    free(usf_buf);
    usf_buf=NULL;
  }
  if(unusedspace_file)
    sc_file_free(unusedspace_file);
  if (temp_p15card) { 
    /* set to NULL without freeing because we reused structure */
    temp_p15card->card = NULL;
    
    /* now free temp structure */
    sc_pkcs15_card_free(temp_p15card);
    temp_p15card = NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r); 
}

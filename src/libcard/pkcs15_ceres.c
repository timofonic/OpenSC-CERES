/*
 * Ceres specific operation for PKCS #15 initialization
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda 
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "pkcs15_default.h"

#define DF_ID "\x3F\x00\x60\xA1"
#define EF_UNUSED_SPACE_ID "\x3F\x00\x50\x15\x50\x33"
#define MAX_INFO_SIZE 256
#define CERES_CARD_LABEL     "ceres" 


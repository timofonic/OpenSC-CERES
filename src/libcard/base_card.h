/*
 * base_card.h: Support for Ceres card
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef BASE_CARD_H
#define BASE_CARD_H

#include <stdlib.h>
#include <opensc/pkcs15.h>
#include <../include/ceres_cardctl.h>
#include "../common/card_structures.h"
#include "virtual_fs.h"
#include "map.h"
#include "map_helper.h"


/* definitions */
#define MODULE_VERSION "0.11.8"
#define MODULE_DESC "FNMT Ceres card" 
#define MODULE_NAME "ceres"

#define SC_CARD_TYPE_CERES_INFINEON	0
#define SC_CARD_TYPE_CERES_ST	        1
#define CARD_CHIP_NAME		"ceres"

#define CARD_MF_TYPE		0x00
#define CARD_DF_TYPE		0x01
#define CARD_EF_TYPE		0x02
#define CARD_SEL_ID		0x00
#define CARD_SEL_AID		0x04
#define CARD_FID_MF		0x3F00
#define CARD_MF_NAME		"Master.File"

#define SC_PKCS15_ODF           0xC0
#define SC_PKCS15_TOKENINFO     0xC1
#define SC_PKCS15_UNUSED        0xC2


/* key reference */
#define CERES_ST_KEY_REFERENCE_MIN 0x1
#define CERES_INFINEON_KEY_REFERENCE_MIN 0x20
#define CERES_KEY_REFERENCE_MAX 0xff


struct card_priv_data {
  /* this variable holds secure channel state */
  enum {
    secure_channel_not_created = 0, /* set when secure channel hasn't been created yet */
    secure_channel_creating, /* set by card_create_secure_channel when it has begun creating
				the secure channel but it hasn't succeeded yet */
    secure_channel_created /* set by card_create_secure_channel when it has succeeded
			      creating the secure channel */
  } secure_channel_state;
  u8 kenc[16];
  u8 kmac[16];
  u8 ssc[8];
  int card_type;
  int rsa_key_ref;		
  unsigned long sign_alg_flags;


  /* This variable stores the path of the last file selected on the card */
  sc_file_t *last_file_selected;
  int use_pkcs15_cache; /* flag to habilitate/inhabilitate the use of pkcs15 cache */

  /* 
     This variable holds a list of certificates files with its 
     path and respective compressed and uncompressed length.
  */
  cert_file_t *list_cert_files;    
  
  /* This variable sets whether we have called ceres_create_file or ceres_select_file */
  int func;

  /* virtual fs variables */
  sc_path_t current_path; /*!< current path */
  virtual_fs_t *virtual_fs; /*!< virtual fs */
  int use_virtual_fs; /*!< use virtual fs in operations */

  /* mapped variables */
  map_path_to_path_t *virtual_fs_to_card_path_map; /*!< maps virtual_fs sc_path_t * to card sc_path_t * */
  map_id_to_id_t *virtual_fs_to_card_ckaid_map; /*< maps CKA_ID virtual_fs sc_pkcs15_id * to card sc_pkcs15_id * */
  map_id_to_der_t *cdf_card_ckaid_to_card_der_map; /*< maps CDF card CKA_ID to card der encoded asn1 */
  map_id_to_der_t *prkdf_card_ckaid_to_card_der_map; /*< maps PrKDF card CKA_ID to card der encoded asn1 */
  map_id_to_der_t *pukdf_card_ckaid_to_card_der_map; /*< maps PuKDF card CKA_ID to card der encoded asn1 */
  map_path_to_id_t *card_path_to_card_ckaid_map; /*< maps card certificate file path to card certificate ckaid */
  map_path_to_id_t *card_ckaid_to_card_keyinfo_map; /*< Maps a ckaid to a card keys key_usage and acces_flags */
  map_path_to_key_info_t *card_path_to_card_keyinfo_map;/*< Maps a path to the key info*/
};

/* useful macros */
#define DRVDATA(card) ((struct card_priv_data *) ((card)->drv_data))

/* function declarations */
int ceres_get_serialnr(sc_card_t *card, sc_serial_number_t *serial);
int ceres_transmit_apdu(sc_card_t *card, sc_apdu_t *tx);
int ceres_select_file(struct sc_card *card, const struct sc_path *in_path, struct sc_file **file);
int ceres_create_data_object_file( sc_card_t *card, sc_path_t *path, size_t size );
int ceres_create_cert_file( sc_card_t *card, sc_path_t *path, size_t size );
int ask_user_auth();

/* Compatibility with other cards */
int ceres_assure_secure_channel(struct sc_card *card);

int ceres_is_virtual_fs_active(struct sc_card *card);
void ceres_set_virtual_fs_state(struct sc_card *card, int active);

/* To be deleted */
void ceres_dump_buffer(sc_context_t *ctx, const char* label, const u8* data, size_t len);

int ceres_parse_standard_pkcs15( sc_card_t *card,
				card_pkcs15_df_t *p15_df,
				sc_pkcs15_df_t *df, 
				sc_pkcs15_card_t **temp_p15card );

#endif /* BASE_CARD_H */

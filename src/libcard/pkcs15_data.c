/*
 * pkcs15_data.c: Ceres data object functions
 *
 * Copyright (C) 2009-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 */




#include "../include/internal.h"
#include "../common/util.h"
#include <opensc/asn1.h>
#include <asn1_ceres.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ltdl.h>
#include <opensc/log.h>
#include <opensc/pkcs15.h>
#include "pkcs15_default.h"
#include <openssl/sha.h>



static const struct sc_asn1_entry     c_asn1_data_object[] = {
        { "dataObject", SC_ASN1_OCTET_STRING, SC_ASN1_TAG_OCTET_STRING, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

int sc_pkcs15_ceres_read_data_object(struct sc_pkcs15_card *p15card,
			       const struct sc_pkcs15_data_info *info,
			       struct sc_pkcs15_data **data_object_out)
{
	int r;
	struct sc_pkcs15_data *data_object;
	u8 *data = NULL;
	size_t len;
	
	if (p15card == NULL || info == NULL || data_object_out == NULL)
		return SC_ERROR_INVALID_ARGUMENTS;
	SC_FUNC_CALLED(p15card->card->ctx, 1);

	r = sc_pkcs15_read_file(p15card, &info->path, &data, &len, NULL);
	if (r)
		return r;
	data_object = (struct sc_pkcs15_data *) malloc(sizeof(struct sc_pkcs15_data));
	if (data_object == NULL) {
		free(data);
		return SC_ERROR_OUT_OF_MEMORY;
	}
	memset(data_object, 0, sizeof(struct sc_pkcs15_data));
	
	data_object->data = data;
	data_object->data_len = len;
	*data_object_out = data_object;
	return 0;
}

static const struct sc_asn1_entry c_asn1_data[] = {
	{ "data", SC_ASN1_PKCS15_OBJECT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
	{ NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_com_data_attr[] = {
	{ "appName", SC_ASN1_TAG_TELETEXSTRING, SC_ASN1_TAG_TELETEXSTRING, SC_ASN1_OPTIONAL, NULL, NULL },
	{ "appOID", SC_ASN1_OBJECT, SC_ASN1_TAG_OBJECT, SC_ASN1_OPTIONAL, NULL, NULL },
	{ NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_type_data_attr[] = {
	{ "path", SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
	{ NULL, 0, 0, 0, NULL, NULL }
};

int sc_pkcs15_ceres_decode_dodf_entry(struct sc_pkcs15_card *p15card,
			       struct sc_pkcs15_object *obj,
			       const u8 ** buf, size_t *buflen)
{
        sc_context_t *ctx = p15card->card->ctx;
	struct sc_pkcs15_data_info info;
	struct sc_asn1_entry	asn1_com_data_attr[3],
				asn1_type_data_attr[2],
				asn1_data[2];
	struct sc_asn1_pkcs15_object data_obj = { obj, asn1_com_data_attr, NULL,
					     asn1_type_data_attr };
	size_t label_len = sizeof(info.app_label);
	int r;

	sc_copy_asn1_entry(c_asn1_com_data_attr, asn1_com_data_attr);
	sc_copy_asn1_entry(c_asn1_type_data_attr, asn1_type_data_attr);
	sc_copy_asn1_entry(c_asn1_data, asn1_data);
	
	sc_format_asn1_entry(asn1_com_data_attr + 0, &info.app_label, &label_len, 0);
	sc_format_asn1_entry(asn1_com_data_attr + 1, &info.app_oid, NULL, 0);
	sc_format_asn1_entry(asn1_type_data_attr + 0, &info.path, NULL, 0);
	sc_format_asn1_entry(asn1_data + 0, &data_obj, NULL, 0);

	/* Fill in defaults */
	memset(&info, 0, sizeof(info));
	info.app_oid.value[0] = -1;

	r = sc_asn1_ceres_decode(ctx, asn1_data, *buf, *buflen, buf, buflen);
	if (r == SC_ERROR_ASN1_END_OF_CONTENTS)
		return r;
	SC_TEST_RET(ctx, r, "ASN.1 decoding failed");

	obj->type = SC_PKCS15_TYPE_DATA_OBJECT;
	obj->data = malloc(sizeof(info));
	if (obj->data == NULL)
		SC_FUNC_RETURN(ctx, 0, SC_ERROR_OUT_OF_MEMORY);
	memcpy(obj->data, &info, sizeof(info));

	return 0;
}

int sc_pkcs15_ceres_encode_dodf_entry(sc_context_t *ctx,
			       const struct sc_pkcs15_object *obj,
			       u8 **buf, size_t *bufsize)
{
	struct sc_asn1_entry	asn1_com_data_attr[4],
				asn1_type_data_attr[2],
				asn1_data[2];
	struct sc_pkcs15_data_info *info;
	struct sc_asn1_pkcs15_object data_obj = { (struct sc_pkcs15_object *) obj,
							asn1_com_data_attr, NULL,
							asn1_type_data_attr };
	size_t label_len;

	info = (struct sc_pkcs15_data_info *) obj->data;

	/* FIX IT!!!! Fixed in order to be compatible with Windows PKCS11*/
//	label_len = strlen(info->app_label);
	
	info->app_label[0] = 0x30;
	info->app_label[1] = 0x30;
	info->app_label[2] = 0x30;
	info->app_label[3] = 0x30;
	info->app_label[4] = 0x00;
	label_len = strlen(info->app_label);

	sc_copy_asn1_entry(c_asn1_com_data_attr, asn1_com_data_attr);
	sc_copy_asn1_entry(c_asn1_type_data_attr, asn1_type_data_attr);
	sc_copy_asn1_entry(c_asn1_data, asn1_data);
	
	if (label_len) {
		sc_format_asn1_entry(asn1_com_data_attr + 0,
				&info->app_label, &label_len, 1);
	}
	if (info->app_oid.value[0] != -1) {
		sc_format_asn1_entry(asn1_com_data_attr + 1,
				&info->app_oid, NULL, 1);
	}
	sc_format_asn1_entry(asn1_type_data_attr + 0, &info->path, NULL, 1);
	sc_format_asn1_entry(asn1_data + 0, &data_obj, NULL, 1);

	return sc_asn1_ceres_encode(ctx, asn1_data, buf, bufsize);
}


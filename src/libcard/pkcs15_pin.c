/*
 * pkcs15_pin.c: PKCS #15 Ceres PIN functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "../include/internal.h"
#include <opensc/asn1.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ltdl.h>
#include <opensc/log.h>
#include <opensc/pkcs15.h>
#include "pkcs15_default.h"

static const struct sc_asn1_entry c_asn1_com_ao_attr[] = {
        { "authId",       SC_ASN1_PKCS15_ID, SC_ASN1_TAG_OCTET_STRING, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};
static const struct sc_asn1_entry c_asn1_pin_attr[] = {
        { "pinFlags",     SC_ASN1_BIT_FIELD, SC_ASN1_TAG_BIT_STRING, 0, NULL, NULL },
        { "pinType",      SC_ASN1_ENUMERATED, SC_ASN1_TAG_ENUMERATED, 0, NULL, NULL },
        { "minLength",    SC_ASN1_INTEGER, SC_ASN1_TAG_INTEGER, 0, NULL, NULL },
        { "storedLength", SC_ASN1_INTEGER, SC_ASN1_TAG_INTEGER, 0, NULL, NULL },
        { "maxLength",    SC_ASN1_INTEGER, SC_ASN1_TAG_INTEGER, SC_ASN1_OPTIONAL, NULL, NULL },
        { "pinReference", SC_ASN1_INTEGER, SC_ASN1_CTX | 0, SC_ASN1_OPTIONAL, NULL, NULL },
        { "padChar",      SC_ASN1_OCTET_STRING, SC_ASN1_TAG_OCTET_STRING, SC_ASN1_OPTIONAL, NULL, NULL },
        { "lastPinChange",SC_ASN1_GENERALIZEDTIME, SC_ASN1_TAG_GENERALIZEDTIME, SC_ASN1_OPTIONAL, NULL, NULL },
        { "path",         SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, SC_ASN1_OPTIONAL, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

static const struct sc_asn1_entry c_asn1_pin[] = {
        { "pin", SC_ASN1_PKCS15_OBJECT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
        { NULL, 0, 0, 0, NULL, NULL }
};

int sc_pkcs15_ceres_decode_aodf_entry(struct sc_pkcs15_card *p15card,
				      struct sc_pkcs15_object *obj,
				      const u8 ** buf, size_t *buflen)
{
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_pkcs15_ceres_decode_aodf_entry\n");
        sc_context_t *ctx = p15card->card->ctx;
        struct sc_pkcs15_pin_info info;
        int r;
        size_t flags_len = sizeof(info.flags);
        size_t padchar_len = 1;
        struct sc_asn1_entry asn1_com_ao_attr[2], asn1_pin_attr[10];
        struct sc_asn1_entry asn1_pin[2];
	struct sc_asn1_pkcs15_object pin_obj = { obj, asn1_com_ao_attr, NULL, asn1_pin_attr };

        sc_copy_asn1_entry(c_asn1_pin, asn1_pin);
        sc_copy_asn1_entry(c_asn1_pin_attr, asn1_pin_attr);
        sc_copy_asn1_entry(c_asn1_com_ao_attr, asn1_com_ao_attr);

        sc_format_asn1_entry(asn1_pin + 0, &pin_obj, NULL, 0);

        sc_format_asn1_entry(asn1_pin_attr + 0, &info.flags, &flags_len, 0);
        sc_format_asn1_entry(asn1_pin_attr + 1, &info.type, NULL, 0);
        sc_format_asn1_entry(asn1_pin_attr + 2, &info.min_length, NULL, 0);
        sc_format_asn1_entry(asn1_pin_attr + 3, &info.stored_length, NULL, 0);
        sc_format_asn1_entry(asn1_pin_attr + 4, &info.max_length, NULL, 0);
        sc_format_asn1_entry(asn1_pin_attr + 5, &info.reference, NULL, 0);
        sc_format_asn1_entry(asn1_pin_attr + 6, &info.pad_char, &padchar_len, 0);
        /* We don't support lastPinChange yet. */
        sc_format_asn1_entry(asn1_pin_attr + 8, &info.path, NULL, 0);

        sc_format_asn1_entry(asn1_com_ao_attr + 0, &info.auth_id, NULL, 0);

        /* Fill in defaults */
        memset(&info, 0, sizeof(info));
        info.reference = 0;
        info.tries_left = -1;

        r = sc_asn1_ceres_decode(ctx, asn1_pin, *buf, *buflen, buf, buflen);
	        if (r == SC_ERROR_ASN1_END_OF_CONTENTS)
                return r;
        SC_TEST_RET(ctx, r, "ASN.1 decoding failed");
        info.magic = SC_PKCS15_PIN_MAGIC;
        obj->type = SC_PKCS15_TYPE_AUTH_PIN;
        obj->data = malloc(sizeof(info));
        if (obj->data == NULL)
                SC_FUNC_RETURN(ctx, 0, SC_ERROR_OUT_OF_MEMORY);
        if (info.max_length == 0) {
                if (p15card->card->max_pin_len != 0)
                        info.max_length = p15card->card->max_pin_len;
                else if (info.stored_length != 0)
                        info.max_length = info.type != SC_PKCS15_PIN_TYPE_BCD ?
                                info.stored_length : 2 * info.stored_length;
                else
                        info.max_length = 8; /* shouldn't happen */
        }

        info.flags &= (0xFFFF ^ SC_PKCS15_PIN_FLAG_SO_PIN);
        if(info.path.len==0)
        {
                info.path.type = SC_PATH_TYPE_PATH;
                info.path.value[0] = 0x3F;
                info.path.value[1] = 0x00;
                info.path.len = 2;
                info.path.index = 0;
                info.path.count = 0;
        }

        memcpy(obj->data, &info, sizeof(info));

	if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_pkcs15_ceres_decode_aodf_entry\n");
        return 0;
}


/*
 * base_card.c: Support for Ceres card
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <zlib.h>
#include "../include/internal.h" 
#include <opensc/opensc.h>
#include <opensc/cardctl.h>
#include <opensc/log.h>
#include <opensc/asn1.h>
#include <opensc/pkcs15.h>
#include "base_card.h"
#include "pkcs15_default.h"
#include "pkcs15_standard.h"
#include "../common/util.h"
#include "virtual_fs.h"
#include "virtual_pkcs15_fs.h"
#include "card_sync.h"

#define CARD_SELECT_FILE 0
#define CARD_CREATE_FILE 1


static void ceres_add_acl_entry(sc_card_t *card, sc_file_t *file, int op, u8 byte);
static void ceres_parse_sec_attr(sc_card_t *card, sc_file_t *file, const u8 * buf, size_t len);
static int ceres_iso_build_pin_apdu(sc_card_t *card, sc_apdu_t *apdu,
			      struct sc_pin_cmd_data *data, u8 *buf, size_t buf_len);
static int ceres_build_pin_apdu(struct sc_card *card,
			       struct sc_apdu *apdu,
			       struct sc_pin_cmd_data *data);
static int ceres_iso_pin_cmd(sc_card_t *card, struct sc_pin_cmd_data *data,
		       int *tries_left);
int ceres_get_serialnr(sc_card_t *card, sc_serial_number_t *serial);
int ceres_envelope_transmit (sc_card_t *card, sc_apdu_t *tx);
int ceres_transmit_apdu(sc_card_t *card, sc_apdu_t *tx);
static int ceres_middle_delete_file (struct sc_card *card, const sc_path_t *card_path);
static int ceres_delete_file (struct sc_card *card, const struct sc_ceresctl_card_delete_file_info *file_info);
static int ceres_check_sw(sc_card_t *card, unsigned int sw1, unsigned int sw2);
static int ceres_generate_key( struct sc_card *card, struct sc_ceresctl_card_genkey_info *genkey_info );
static int ceres_get_public_key( struct sc_card *card, struct sc_ceresctl_card_genkey_info *genkey_info );
static int ceres_delete_key(struct sc_card *card, const unsigned int key_reference);
static int ceres_store_key_component(struct sc_card *card, struct sc_ceresctl_card_store_key_component_info *component_info );

static struct sc_atr_table card_atrs[] = {
  /* ATR Ceres Siemens Infineon 19 */
  /*"3B:EF:00:00:40:14:80:25:43:45:52:45:53:57:01:16:01:01:03:90:00";*/
  /* ATR Ceres Siemens Infineon 20 */
  /*"3B:EF:00:00:40:14:80:25:43:45:52:45:53:57:05:60:01:02:03:90:00";*/
  {
    "3b:ef:00:00:40:14:80:25:43:45:52:45:53:57:00:00:01:00:03:90:00",
    "FF:FF:00:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:00:00:FF:00:FF:FF:FF",
    CARD_CHIP_NAME, 
    SC_CARD_TYPE_CERES_INFINEON,
    0,
    NULL
  },
  /* ATR Ceres ST v2: */
  /*"3B 7F 38 00 00 00 6A 43 45 52 45 53 02 2C 34 02 11 03 90 00"*/
  /* New ST v3 CERES_WG10*/
  /*"3B 7F 38 00 00 00 6A 43 45 52 45 53 02 2C 34 03 11 03 90 00";*/
  {
    "3b:7f:00:00:00:00:6a:43:45:52:45:53:02:2c:34:00:00:03:90:00",
    "FF:FF:00:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:00:00:FF:FF:FF",
    CARD_CHIP_NAME, 
    SC_CARD_TYPE_CERES_ST,
    0,
    NULL
  }
};

static struct sc_card_operations card_ops;
static const struct sc_card_operations *iso_ops = NULL;

static struct sc_card_driver card_drv = {
  MODULE_DESC,
  MODULE_NAME,
  &card_ops,
  NULL, 0, NULL
};


static int _ceres_match_atr_table(sc_context_t *ctx, struct sc_atr_table *table, u8 *atr, size_t atr_len)
{
	u8 *card_atr_bin = atr;
	size_t card_atr_bin_len = atr_len;
	char card_atr_hex[3 * SC_MAX_ATR_SIZE];
	size_t card_atr_hex_len;
	unsigned int i = 0;

	if (ctx == NULL || table == NULL || atr == NULL)
		return -1;
	sc_bin_to_hex(card_atr_bin, card_atr_bin_len, card_atr_hex, sizeof(card_atr_hex), ':');
	card_atr_hex_len = strlen(card_atr_hex);

	if (ctx->debug >= 4)
		sc_debug(ctx, "ATR     : %s\n", card_atr_hex);

	for (i = 0; table[i].atr != NULL; i++) {
		const char *tatr = table[i].atr;
		const char *matr = table[i].atrmask;
		size_t tatr_len = strlen(tatr);
		u8 mbin[SC_MAX_ATR_SIZE], tbin[SC_MAX_ATR_SIZE];
		size_t mbin_len, tbin_len, s, matr_len;
		size_t fix_hex_len = card_atr_hex_len;
		size_t fix_bin_len = card_atr_bin_len;

		if (ctx->debug >= 4)
			sc_debug(ctx, "ATR try : %s\n", tatr);

		if (tatr_len != fix_hex_len) {
			if (ctx->debug >= 5)
				sc_debug(ctx, "ignored - wrong length\n", tatr);
			continue;
		}
		if (matr != NULL) {
			if (ctx->debug >= 4)
				sc_debug(ctx, "ATR mask: %s\n", matr);

			matr_len = strlen(matr);
			if (tatr_len != matr_len)
				continue;
			tbin_len = sizeof(tbin);
			sc_hex_to_bin(tatr, tbin, &tbin_len);
			mbin_len = sizeof(mbin);
			sc_hex_to_bin(matr, mbin, &mbin_len);
			if (mbin_len != fix_bin_len) {
				sc_error(ctx,"length of atr and atr mask do not match - ignored: %s - %s", tatr, matr);
                                continue;
                        }
                        for (s = 0; s < tbin_len; s++) {
                                /* reduce tatr with mask */
                                tbin[s] = (tbin[s] & mbin[s]);
                                /* create copy of card_atr_bin masked) */
                                mbin[s] = (card_atr_bin[s] & mbin[s]);
                        }
                        if (memcmp(tbin, mbin, tbin_len) != 0)
                                continue;
                } else {
                        if (strncasecmp(tatr, card_atr_hex, tatr_len) != 0)
                                continue;
                }
                return i;
        }
        return -1;
}



int _ceres_match_atr_(sc_card_t *card, struct sc_atr_table *table, int *type_out)
{
        int res;

        if (card == NULL)
                return -1;
        res = _ceres_match_atr_table(card->ctx, table, card->atr, card->atr_len);
        if (res < 0)
                return res;
        if (type_out != NULL)
                *type_out = table[res].type;
        return res;
}




static int ceres_match_card(struct sc_card *card)
{ 
  int i;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_match_card\n");

  i = _ceres_match_atr_(card, card_atrs, &card->type);
  if (i < 0) {
    if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_match_card: returning 0\n"); 
    return 0;
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_match_card: returning 1\n"); 

  return 1;
}

int _ceres_card_add_algorithm(sc_card_t *card, const sc_algorithm_info_t *info)
{
        sc_algorithm_info_t *p;

        assert(sc_card_valid(card) && info != NULL);
        p = (sc_algorithm_info_t *) realloc(card->algorithms, (card->algorithm_count + 1) * sizeof(*info));
        if (!p) {
                if (card->algorithms)
                        free(card->algorithms);
                card->algorithms = NULL;
                card->algorithm_count = 0;
                return SC_ERROR_OUT_OF_MEMORY;
        }
        card->algorithms = p;
        p += card->algorithm_count;
        card->algorithm_count++;
        *p = *info;
        return 0;
}



int _ceres_card_add_rsa_alg(sc_card_t *card, unsigned int key_length,
                         unsigned long flags, unsigned long exponent)
{
        sc_algorithm_info_t info;

        memset(&info, 0, sizeof(info));
        info.algorithm = SC_ALGORITHM_RSA;
        info.key_length = key_length;
        info.flags = flags;
        info.u._rsa.exponent = exponent;

        return _ceres_card_add_algorithm(card, &info);
}




static int ceres_init(struct sc_card *card)
{ 
  struct card_priv_data * priv;
  int i, id, r=0;
  unsigned long flags;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_init\n");

  card->drv_data = priv = (struct card_priv_data *) malloc(sizeof(struct card_priv_data));
  if (card->drv_data == NULL) {    
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  memset( priv, 0, sizeof( *priv ) );
  priv->secure_channel_state = secure_channel_not_created;
  priv->last_file_selected = sc_file_new(); /* Init last_file_selected struct */
  priv->use_pkcs15_cache = 1;
  priv->list_cert_files = NULL; 
  priv->func = CARD_SELECT_FILE;


  /* Maps a path from virtual_fs to card */
  priv->virtual_fs_to_card_path_map = map_path_to_path_new();
  if(!priv->virtual_fs_to_card_path_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  /* Maps a ckaid from virtual_fs to card */
  priv->virtual_fs_to_card_ckaid_map = map_id_to_id_new();
  if(!priv->virtual_fs_to_card_ckaid_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  /* Maps a card ckaid to der encoding for cdf */
  priv->cdf_card_ckaid_to_card_der_map = map_id_to_der_new();
  if(!priv->cdf_card_ckaid_to_card_der_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  /* Maps a card ckaid to der encoding for prkdf */
  priv->prkdf_card_ckaid_to_card_der_map = map_id_to_der_new();
  if(!priv->prkdf_card_ckaid_to_card_der_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  /* Maps a card ckaid to der encoding for pukdf */
  priv->pukdf_card_ckaid_to_card_der_map = map_id_to_der_new();
  if(!priv->pukdf_card_ckaid_to_card_der_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  /* Maps a card certificate file path to card certificate ckaid */
  priv->card_path_to_card_ckaid_map = map_path_to_id_new();
  if(!priv->card_path_to_card_ckaid_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  /* Maps a ckaid to a card keys key_usage and acces_flags */
  priv->card_ckaid_to_card_keyinfo_map = map_ckaid_to_keyinfo_new();
  if(!priv->card_ckaid_to_card_keyinfo_map) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_err;
  }

  priv->card_path_to_card_keyinfo_map = map_path_to_key_info_new();
  if(!priv->card_path_to_card_keyinfo_map){
	r = SC_ERROR_OUT_OF_MEMORY;
	goto ci_err;
  }

  sc_format_path("3F00", &priv->current_path); /* set current path to 3F00 */
  priv->virtual_fs = virtual_fs_new();
  if(!priv->virtual_fs) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto ci_end;
  }

  /* activate virtual_fs */
  ceres_set_virtual_fs_state(card, 1);
  
  r = virtual_pkcs15_fs_init(priv->virtual_fs);
  if (r!=SC_SUCCESS) {
    sc_error (card->ctx, "Couldn't initialize PKCS#15 virtual fs\n");
    goto ci_end;
  }

  i = _ceres_match_atr_(card, card_atrs, &id);
  if (i < 0) {
    sc_error (card->ctx, "no correct id parsed!! Id:%d\n", id);
    goto ci_err;
  }

  card->name = "ceres";

  flags = SC_ALGORITHM_RSA_RAW;
  flags |= SC_ALGORITHM_RSA_HASH_NONE;
  flags |= SC_ALGORITHM_ONBOARD_KEY_GEN;

  switch(id) {
  case(SC_CARD_TYPE_CERES_INFINEON):    
    _ceres_card_add_rsa_alg(card, 1024, flags, 0);
    sc_debug (card->ctx, "id parsed!! Id:%d\n", id);
    break;
  case(SC_CARD_TYPE_CERES_ST):
    /*    _ceres_card_add_rsa_alg(card, 512, flags, 0);
	  _ceres_card_add_rsa_alg(card, 768, flags, 0);*/
    _ceres_card_add_rsa_alg(card, 1024, flags, 0);
    _ceres_card_add_rsa_alg(card, 2048, flags, 0);
    sc_debug (card->ctx, "id parsed!! Id:%d\n", id);
    break;
  default:
    sc_error (card->ctx, "no correct id parsed!! Id:%d\n", id);
    goto ci_err;
  }

  /*  card->drv_data = priv; */
  card->cla = 0x00;	

  card->type=id;
  priv->card_type = id;		

  /* State that we have an RNG */
  card->caps |= SC_CARD_CAP_RNG;


 ci_err:
 ci_end:
  if(r != SC_SUCCESS) {
    if(priv) {
      if(priv->virtual_fs_to_card_path_map) {
	map_free(priv->virtual_fs_to_card_path_map);
	priv->virtual_fs_to_card_path_map = NULL;
      }

      if(priv->virtual_fs_to_card_ckaid_map) {
	map_free(priv->virtual_fs_to_card_ckaid_map);
	priv->virtual_fs_to_card_ckaid_map = NULL;
      }

      if(priv->cdf_card_ckaid_to_card_der_map) {
	map_free(priv->cdf_card_ckaid_to_card_der_map);
	priv->cdf_card_ckaid_to_card_der_map = NULL;
      }

      if(priv->prkdf_card_ckaid_to_card_der_map) {
	map_free(priv->prkdf_card_ckaid_to_card_der_map);
	priv->prkdf_card_ckaid_to_card_der_map = NULL;
      }

      if(priv->pukdf_card_ckaid_to_card_der_map) {
	map_free(priv->pukdf_card_ckaid_to_card_der_map);
	priv->pukdf_card_ckaid_to_card_der_map = NULL;
      }

      if(priv->virtual_fs) {
        virtual_fs_free(priv->virtual_fs);
        priv->virtual_fs = NULL;
      }
      free(priv);
      priv = NULL;
    }
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}

static int ceres_finish(struct sc_card *card)
{ 
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_finish\n");


  if (DRVDATA(card)) {
    if(DRVDATA(card)->virtual_fs) {
      virtual_fs_free(DRVDATA(card)->virtual_fs);
      DRVDATA(card)->virtual_fs = NULL;
    }
    if(DRVDATA(card)->virtual_fs_to_card_path_map) {
      map_free(DRVDATA(card)->virtual_fs_to_card_path_map);
      DRVDATA(card)->virtual_fs_to_card_path_map = NULL;
    }
    if(DRVDATA(card)->virtual_fs_to_card_ckaid_map) {
      map_free(DRVDATA(card)->virtual_fs_to_card_ckaid_map);
      DRVDATA(card)->virtual_fs_to_card_ckaid_map = NULL;
    }
    if(DRVDATA(card)->cdf_card_ckaid_to_card_der_map) {
      map_free(DRVDATA(card)->cdf_card_ckaid_to_card_der_map);
      DRVDATA(card)->cdf_card_ckaid_to_card_der_map = NULL;
    }
    if(DRVDATA(card)->prkdf_card_ckaid_to_card_der_map) {
      map_free(DRVDATA(card)->prkdf_card_ckaid_to_card_der_map);
      DRVDATA(card)->prkdf_card_ckaid_to_card_der_map = NULL;
    }
    if(DRVDATA(card)->pukdf_card_ckaid_to_card_der_map) {
      map_free(DRVDATA(card)->pukdf_card_ckaid_to_card_der_map);
      DRVDATA(card)->pukdf_card_ckaid_to_card_der_map = NULL;
    }
    if(DRVDATA(card)->card_path_to_card_ckaid_map) {
      map_free(DRVDATA(card)->card_path_to_card_ckaid_map);
      DRVDATA(card)->card_path_to_card_ckaid_map = NULL;
    }    

    /* Wipe out private card struct memory */
    memset( card->drv_data, 0, sizeof( *card->drv_data ) );
    free(card->drv_data);
  }
  card->drv_data = NULL;

 
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_finish\n"); 
  return 0;
}



/* virtual fs functions */
int ceres_is_virtual_fs_active(struct sc_card *card)
{
  return DRVDATA(card)->use_virtual_fs;
}

void ceres_set_virtual_fs_state(struct sc_card *card, int active)
{
  if(active) {
    DRVDATA(card)->use_virtual_fs = 1;
    card->max_send_size = 0xffff;
    card->max_recv_size = 0xffff;
    if (card->ctx->debug) sc_debug(card->ctx, "virtual_fs mode activated\n");
  } else {
    DRVDATA(card)->use_virtual_fs = 0;
    card->max_send_size = 0xf0;
    card->max_recv_size = 0xf0;
    if (card->ctx->debug) sc_debug(card->ctx, "virtual_fs mode deactivated\n");
  }
}

static int ceres_set_security_env(struct sc_card *card,
				 const struct sc_security_env *env,
				 int se_num)
{
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_set_security_env");

  if (env->flags & SC_SEC_ENV_ALG_PRESENT) {
		
    if (env->algorithm != SC_ALGORITHM_RSA) {
      sc_error(card->ctx, "La tarjeta Ceres sólo soporta el algoritmo RSA.\n");
      return SC_ERROR_NOT_SUPPORTED;
    }		
    if (((env->algorithm_flags & SC_ALGORITHM_RSA_HASHES) != 0) && !(env->algorithm_flags & SC_ALGORITHM_RSA_HASH_SHA1))
      {
	sc_error(card->ctx, "La tarjeta Ceres sólo soporta algoritmo RSA con SHA1.\n");
	return SC_ERROR_NOT_SUPPORTED;
      }
  }
  
  if (env->flags & SC_SEC_ENV_KEY_REF_PRESENT) {
    if(env->key_ref_len>1)
      {
	sc_error(card->ctx, "Identificador de clave erróneo.\n");
	return SC_ERROR_NOT_SUPPORTED;
      }
    
    DRVDATA(card)->rsa_key_ref = env->key_ref[0];		
  }
  
  if (env->flags & SC_SEC_ENV_KEY_REF_PRESENT) {
    if(env->key_ref_len>1)
      {
	sc_error(card->ctx, "Identificador de clave erróneo.\n");
	return SC_ERROR_NOT_SUPPORTED;
      }
    
    DRVDATA(card)->rsa_key_ref = env->key_ref[0];
  }
  

  if (card->ctx->debug) sc_debug(card->ctx, "Key_ref= 0x%X", env->key_ref[0]);

  DRVDATA(card)->sign_alg_flags = env->algorithm_flags;

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_set_security_env");
  
  return SC_SUCCESS;
}

static int ceres_compute_signature(struct sc_card *card,
				  const u8 * data, size_t datalen,
				  u8 * out, size_t outlen)
{ 
  struct sc_apdu apdu;		
  int r;	

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_compute_signature\n");

  assert(card != NULL && data != NULL && out != NULL);

  /* check if serial channel has been created and create it if not */
  if((r = ceres_assure_secure_channel(card)) != SC_SUCCESS)
    return r;

 
  /* Pasos:	
     1.- Se cargan los datos (raw o hash) en memoria, Load Data.
     2.- Se firman los datos */
		
  /* CLA: 0x90  
   * INS: 0x58  LOAD DATA
   * P1:  0x00  
   * P2:  0x00*/	

  /* do the apdu thing */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_3_SHORT;
  apdu.ins = 0x58;
  apdu.p1 = 0x00;
  apdu.p2 = 0x00;
  apdu.lc = datalen;
  apdu.data = data;
  apdu.datalen = datalen;	

  r = ceres_transmit_apdu(card, &apdu);
  if (r!=SC_SUCCESS) {
    datalen = -1;
    goto dcs_err;
  }

  /* CLA: 0x90  
   * INS: 0x5A  SIGN DATA
   * P1:  0x80  
   * P2:  Id clave */	

  /* do the apdu thing */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_2_SHORT;
  apdu.ins = 0x5A;
  apdu.p1 = 0x80;
  apdu.p2 = DRVDATA(card)->rsa_key_ref;	
  apdu.le = datalen; 
  apdu.resp = out;
  apdu.resplen = outlen;	

  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");

  if (apdu.resplen == 0)
    return sc_check_sw(card, apdu.sw1, apdu.sw2);

 dcs_err:

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_compute_signature\n"); 
 
  return apdu.resplen;
}

static int ceres_decipher(struct sc_card *card, const u8 *data, size_t datalen,
			 u8 *out, size_t outlen)
{ 
  int r;
  struct sc_apdu apdu;		
  
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_decipher\n");

  assert(card != NULL && data != NULL && out != NULL);
  if (datalen != 128)
    SC_FUNC_RETURN(card->ctx, 4, SC_ERROR_INVALID_ARGUMENTS);
  
  /* check if serial channel has been created and create it if not */
  if((r = ceres_assure_secure_channel(card)) != SC_SUCCESS)
    return r;
		
  /* CLA: 0x90  
   * INS: 0x74  IMPORT KEY
   * P1:  0x40  
   * P2:  Id clave */	

  /* do the apdu thing */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_4_SHORT;
  apdu.ins = 0x74;
  apdu.p1 = 0x40;
  apdu.p2 = DRVDATA(card)->rsa_key_ref;
  apdu.lc = datalen;
  apdu.data = data;
  apdu.datalen = datalen;
  apdu.le = outlen/8;
  apdu.resp = out;
  apdu.resplen = outlen;	

  r = ceres_transmit_apdu(card, &apdu); 

  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  
  if (apdu.sw1 == 0x90 && apdu.sw2 == 0x00) {
    size_t len = apdu.resplen > outlen ? outlen : apdu.resplen;
    if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_decipher\n");
    SC_FUNC_RETURN(card->ctx, 2, len);
  }
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_decipher\n");
  SC_FUNC_RETURN(card->ctx, 2, sc_check_sw(card, apdu.sw1, apdu.sw2));
}

static int ceres_get_challenge(struct sc_card *card, u8 *rnd, size_t len)
{ 
  int r;
  struct sc_apdu apdu;
  u8 buf[22];

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_get_challenge\n");

  /* check if serial channel has been created and create it if not */
  if((r = ceres_assure_secure_channel(card)) != SC_SUCCESS)
    return r;
  
  sc_format_apdu(card, &apdu, SC_APDU_CASE_2_SHORT,
		 0x84, 0x00, 0x00);
  apdu.le = 20;
  apdu.resp = buf;
  apdu.resplen = 20;	/* include SW's */

  while (len > 0) {
    int n = len > 20 ? 20 : len;
		
    r = ceres_transmit_apdu(card, &apdu);
    SC_TEST_RET(card->ctx, r, "APDU transmit failed");
    if (apdu.resplen != 20)
      return ceres_check_sw(card, apdu.sw1, apdu.sw2);
    memcpy(rnd, apdu.resp, n);
    len -= n;
    rnd += n;
  }	

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_get_challenge\n"); 

  return 0;
}

static int ceres_pin_cmd(sc_card_t *card, struct sc_pin_cmd_data *data, int *tries_left)
{ 
  int r;
  sc_apdu_t local_apdu;

	
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_pin_cmd\n");

  /* check if serial channel has been created and create it if not */
  if((r = ceres_assure_secure_channel(card)) != SC_SUCCESS)
    return r;

  /*!
    Clear padding flag.
  */
  data->flags &= ~SC_PIN_CMD_NEED_PADDING;
  
  data->apdu = &local_apdu;
  data->pin1.len = (strlen((char *)data->pin1.data)>16)? 16 : strlen((char *)data->pin1.data);
  r = ceres_build_pin_apdu(card, data->apdu, data);
  if (r < 0)
    return r;

  r = ceres_iso_pin_cmd(card, data, tries_left);
  
  /* remove reference to stack variable apdu */
  memset(&local_apdu, 0, sizeof(local_apdu));
  data->apdu = NULL;

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_pin_cmd r=0x%X\n", r);
  return r;
}

int ceres_iso_select_file(sc_card_t *card,
		    const sc_path_t *in_path,
		    sc_file_t **file_out)
{
  sc_context_t *ctx;
  sc_apdu_t apdu;
  u8 buf[SC_MAX_APDU_BUFFER_SIZE];
  u8 pathbuf[SC_MAX_PATH_SIZE], *path = pathbuf;
  int r = SC_SUCCESS, pathlen;
  sc_file_t *file = NULL;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_iso_select_file\n");
  
  assert(card != NULL && in_path != NULL);
  ctx = card->ctx;
  memcpy(path, in_path->value, in_path->len);
  pathlen = in_path->len;

  sc_format_apdu(card, &apdu, SC_APDU_CASE_4_SHORT, 0xA4, 0, 0);       
  
  switch (in_path->type) {
  case SC_PATH_TYPE_FILE_ID:
    apdu.p1 = 0;
    if (pathlen != 2) {
      if (card->ctx->debug) sc_debug(card->ctx, "ERROR: Invalid arguments! pathlen != 2\n");
      return SC_ERROR_INVALID_ARGUMENTS;
    }
    break;
  case SC_PATH_TYPE_DF_NAME:
    apdu.p1 = 4;
    break;
  case SC_PATH_TYPE_PATH:
    /* transform to file id */
    if(pathlen%2) {
      if (card->ctx->debug) sc_debug(card->ctx, "ERROR: Invalid arguments! pathlen not multiple of 2\n");
      return SC_ERROR_INVALID_ARGUMENTS;
    }
    
    while(pathlen>0) {
      sc_path_t temp_path;
      if(path[0] == 0x3f && path[1] == 0x00) {
	temp_path.type = SC_PATH_TYPE_DF_NAME;
	strcpy((char *)temp_path.value, CARD_MF_NAME);
	temp_path.len = sizeof(CARD_MF_NAME) - 1;
      } else {
	temp_path.type = SC_PATH_TYPE_FILE_ID;
	temp_path.value[0] = path[0];
	temp_path.value[1] = path[1];
	temp_path.len = 2;
      }
      r = ceres_iso_select_file(card, &temp_path, file_out);
      if(r != SC_SUCCESS) {
	goto end;
      }
      pathlen-=2;
      path+=2;
    }
    goto end;
    break;
  default:
    if (card->ctx->debug) sc_debug(card->ctx, "ERROR: Invalid arguments! default case %d\n", in_path->type);
    SC_FUNC_RETURN(card->ctx, 2, SC_ERROR_INVALID_ARGUMENTS);
  }
  apdu.p2 = 0;		/* first record, return FCI */
  apdu.lc = pathlen;
  apdu.data = path;
  apdu.datalen = pathlen;

  if (file_out != NULL) {
    apdu.resp = buf;
    apdu.resplen = sizeof(buf);
    apdu.le = 256;
  } else {
    apdu.resplen = 0;
    apdu.le = 0;
    apdu.cse = SC_APDU_CASE_3_SHORT;
  }
  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  if (file_out == NULL) {
    if (apdu.sw1 == 0x61)
      SC_FUNC_RETURN(card->ctx, 2, 0);
    SC_FUNC_RETURN(card->ctx, 2, ceres_check_sw(card, apdu.sw1, apdu.sw2));
  }

  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
  if (r)
    SC_FUNC_RETURN(card->ctx, 2, r);

  switch (apdu.resp[0]) {
  case 0x6F:
    file = sc_file_new();
    if (file == NULL)
      SC_FUNC_RETURN(card->ctx, 0, SC_ERROR_OUT_OF_MEMORY);
    file->path = *in_path;
    if (card->ops->process_fci == NULL) {
      sc_file_free(file);
      SC_FUNC_RETURN(card->ctx, 2, SC_ERROR_NOT_SUPPORTED);
    }
    if (apdu.resp[1] <= apdu.resplen)
      card->ops->process_fci(card, file, apdu.resp+2, apdu.resp[1]);
    *file_out = file;
    break;
  case 0x00:	/* proprietary coding */
    SC_FUNC_RETURN(card->ctx, 2, SC_ERROR_UNKNOWN_DATA_RECEIVED);
  default:
    SC_FUNC_RETURN(card->ctx, 2, SC_ERROR_UNKNOWN_DATA_RECEIVED);
  }

 end:
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_iso_select_file\n");
  return 0;
}

int ceres_select_file(struct sc_card *card, const struct sc_path *in_path,
		     struct sc_file **file)
{   
  int r = SC_SUCCESS, ii;
  sc_path_t temp_path;
  sc_path_t next_path;
  virtual_file_t *virtual_file = NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(ceres_is_virtual_fs_active(card)) {
    /* virtual fs usage */
    if(!in_path || in_path->len < 2 || (in_path->len%2) == 1) {
      /* non-existant or bad in_path */
      r = SC_ERROR_INVALID_ARGUMENTS;
      goto csf_end;
    }

    if((in_path->type != SC_PATH_TYPE_FILE_ID) &&
       (in_path->type != SC_PATH_TYPE_PATH)) {
      /* we don't support string names in path for now */
      r = SC_ERROR_INVALID_ARGUMENTS;
      goto csf_end;
    }

    /* set current path to next_path */
    memcpy(&next_path, &DRVDATA(card)->current_path, sizeof(next_path));
  
    /* iterate through path */
    for(ii=0; ii<in_path->len; ii+=2) {
      if((in_path->value[ii]==0x3f) && (in_path->value[ii+1]==0x00)) {
	/* we go to root again */
	sc_format_path("3F00", &next_path);
      } else {
	/* build a temporal path with current file */
	/* the casting is needed tp remove warning. sc_path_set should const param id because it remains unmodified */
	r = sc_path_set_ceres(&temp_path, in_path->type, (unsigned char *)in_path->value+ii, 2, 0, 0);
	if(r!=SC_SUCCESS) 
	  goto csf_end;
	
	/* append to current file */
	r = sc_append_path(&next_path, &temp_path);
	if(r!=SC_SUCCESS) 
	  goto csf_end;
      }

      /* we have a path we should select */
      if (card->ctx->debug) sc_debug(card->ctx, "Selecting %s\n", sc_print_path(&next_path));
      virtual_file = virtual_fs_find_by_path(DRVDATA(card)->virtual_fs, &next_path);
      if(virtual_file) {
	/* file exists! */
	if (card->ctx->debug) sc_debug(card->ctx, "File selected successfully\n");

	/* set current path to this new path */
	memcpy(&DRVDATA(card)->current_path, &next_path, sizeof(next_path));
      } else {
	/* file doesn't exist! */
	r = SC_ERROR_FILE_NOT_FOUND;
	if (card->ctx->debug) sc_debug(card->ctx, "File selection failed\n");
	goto csf_end;
      }
      
    }

    if(!virtual_file) {
      /* this should be impossible.
	 we should really have a virtual_file here.
	 if not something happened with the internal logic */
      r = SC_ERROR_INTERNAL;
      goto csf_end;
    }

    /* we now synchronize file because this gets a correct size for it */
    r = virtual_file_data_synchronize(virtual_file, card, virtual_file_sync_type_card_to_virtual_fs, DRVDATA(card)->virtual_fs);
    if (r != SC_SUCCESS) {
      sc_error(card->ctx, "Synchronization failed\n");
      goto csf_end;
    }
  
    if(file) {
      /* we have to create a file structure */
      *file = sc_file_new();
      if(!*file) {
	r=SC_ERROR_OUT_OF_MEMORY;
	goto csf_end;
      }

      /* fill file */
      r = virtual_file_export_file(virtual_file, *file);
      if(r != SC_SUCCESS) {
	sc_file_free(*file);
	*file = NULL;
      }
    }
  } else {
    /* not virtual fs select file */
    r = ceres_iso_select_file(card, in_path, file);
  }  
  
 csf_end:
  SC_FUNC_RETURN(card->ctx, 1, r); 

}

static int ceres_check_sw(sc_card_t *card, unsigned int sw1, unsigned int sw2)
{
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_check_sw\n"); 

  if((sw1==0x66)&&(sw2==0x88)) {
    sc_error(card->ctx, "The securized message value is incorrect\n");
    return SC_ERROR_UNKNOWN;
  }
  if(sw1==0x6A && (sw2==0x88 || sw2==0x80 || sw2==0x89)){
    sc_error(card->ctx, "File/Key already exists!\n");
    return SC_ERROR_OBJECT_ALREADY_EXISTS;
  }  
  if(sw1==0x62 && sw2==0x83) {
    sc_error(card->ctx, "Invalid file!\n");   
    return SC_ERROR_INVALID_FILE;
  }
  if(sw1==0x6A && sw2==0x84) {
    sc_error(card->ctx, "Not enough memory!\n");
    return SC_ERROR_NOT_ENOUGH_MEMORY;
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_check_sw\n"); 
  
  return iso_ops->check_sw(card,sw1,sw2);
}

static int ceres_process_fci(sc_card_t *card, sc_file_t *file,
			    const u8 *buf, size_t buflen)
{
  int r = SC_SUCCESS;

  SC_FUNC_CALLED(card->ctx, 1);

  r = iso_ops->process_fci(card, file, buf, buflen);
  if (r!=SC_SUCCESS)
    goto cpf_err;

  if (file->prop_attr_len >= 10) {

    /* Examine file type */
    switch (file->prop_attr[0]) {
    case 0x01:
      file->type = SC_FILE_TYPE_WORKING_EF;
      file->ef_structure = SC_FILE_EF_TRANSPARENT;
      break;
    case 0x15:
      file->type = SC_FILE_TYPE_WORKING_EF;
      break;
    case 0x38: /* 0x38 is DF */
      file->type = SC_FILE_TYPE_DF;
      break;
    }
    /* File identifier */
    file->id = (file->prop_attr[1] << 8) | file->prop_attr[2];

    /* File size */
    file->size = (file->prop_attr[3] << 8) | file->prop_attr[4];

    /* Parse acces conditions bytes (4) from propietary information */
    ceres_parse_sec_attr(card, file, (file->prop_attr)+5, 4);
  }

 cpf_err:
  SC_FUNC_RETURN(card->ctx, 1, r);
}


/**
   Parses standard PKCS#15 and returns the parsed objects
   
   @param card Card Context
   @param p15_df structure with file to parse
   @param df Unitialized df file used to parse structure
   @param temp_p15card Pointer to a NULLified pointer to sc_pkcs15_card_t. A new sc_pkcs15_card will be malloced into this pointer
*/
int ceres_parse_standard_pkcs15( sc_card_t *card,
				card_pkcs15_df_t *p15_df,
				sc_pkcs15_df_t *df, 
				sc_pkcs15_card_t **temp_p15card )
{
  int r = SC_SUCCESS;
  
  /* we init df type */
  memset(df, 0, sizeof(*df));
  df->type = p15_df->type;

  /* we create a fake p15card structure */
  *temp_p15card = sc_pkcs15_card_new();
  if(!(*temp_p15card)) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto cpsp_end;
  }

  /*
    All paths are hardcoded because we don't have access to
    real sc_pkcs15_card_t structure to copy data.
  */
  if(((*temp_p15card)->file_app = sc_file_new()))
    sc_format_path("3F005015", &(*temp_p15card)->file_app->path);
  if(((*temp_p15card)->file_tokeninfo = sc_file_new()))
    sc_format_path("3F0050155032", &(*temp_p15card)->file_tokeninfo->path);
  if(((*temp_p15card)->file_odf = sc_file_new()))
    sc_format_path("3F0050155031", &(*temp_p15card)->file_odf->path);
  if(((*temp_p15card)->file_unusedspace = sc_file_new()))
    sc_format_path("3F0050155033", &(*temp_p15card)->file_unusedspace->path);


  (*temp_p15card)->card = card;
  
  /* parse df */
  r = sc_standard_pkcs15_parse_df((*temp_p15card), df, p15_df->data, p15_df->filled_len);

  cpsp_end:
  if(r != SC_SUCCESS) {
    if(temp_p15card && *temp_p15card) {
      sc_pkcs15_card_free(*temp_p15card);
      *temp_p15card = NULL;
    }
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving ceres_parse_standard_pkcs15 r=%d\n", r);
  return r;
}



/*
  This gets an unused key reference for a key pair just reading the PKCS#15 and getting  a reference that is not used.

  The process:
  * Update internal private key PKCS#15 cache
  * Parse standard private key PKCS#15
  * Find new key reference as an unused reference.
*/
static int ceres_get_new_key_reference( struct sc_card *card, int *new_key_reference )
{
  int r = SC_SUCCESS;
  int ii;
  sc_pkcs15_df_t df;
  sc_path_t prkdf_path;
  sc_pkcs15_card_t *temp_p15card = NULL;
  card_pkcs15_df_t p15_df;
  unsigned char *buffer = NULL;
  int length;
  int initial_key_reference;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_get_new_key_reference\n");

  /* init p15_df structure */
  memset(&p15_df, 0, sizeof(p15_df));
  p15_df.type = SC_PKCS15_PRKDF;

  sc_format_path("3F0050156001", &prkdf_path);
  r = virtual_fs_get_data_by_path(DRVDATA(card)->virtual_fs, &prkdf_path, &buffer, &length);
  if (r != SC_SUCCESS) {
    sc_error(card->ctx, "Synchronization failed\n");
    goto end;
  }

  /* pass buffer ownership to data */
  p15_df.data = buffer;
  buffer = NULL;
  p15_df.data_len = length;
  p15_df.file_len = length;
  p15_df.filled_len = length;

  /* We parse PKCS#15 using a standard parser */
  r = ceres_parse_standard_pkcs15(card, &p15_df, &df, &temp_p15card);
  if(r != SC_SUCCESS) {
    if (card->ctx->debug) sc_debug(card->ctx, "Parsing of standard PKCS#15 failed\n");
    goto end;
  }

  /* now we have the structure in temp_p15card and try to find a key_reference */
  if (card->ctx->debug) sc_debug(card->ctx, "Card type: %d\n", card->type);
  if (card->type == SC_CARD_TYPE_CERES_INFINEON)
    initial_key_reference = CERES_INFINEON_KEY_REFERENCE_MIN;
  else
    initial_key_reference = CERES_ST_KEY_REFERENCE_MIN;

  r = SC_ERROR_TOO_MANY_OBJECTS;
  for(ii=initial_key_reference; ii<=CERES_KEY_REFERENCE_MAX && r!=SC_SUCCESS; ii++) {
    struct sc_pkcs15_object * key;
    
    if(sc_pkcs15_find_prkey_by_reference(temp_p15card, NULL, ii, &key) != SC_SUCCESS) {
      /* we found an unused key_reference... we use this */
      if(new_key_reference)
	*new_key_reference = ii;
      r = SC_SUCCESS;
    }
  }

 end:
  if(temp_p15card) {
    /* set to NULL without freeing because we reused structure */
    temp_p15card->card = NULL;

    /* now free temp structure */
    sc_pkcs15_card_free(temp_p15card);
    temp_p15card = NULL;
  }

  if(p15_df.data) {
    free(p15_df.data);
    p15_df.data = NULL;
  }

  if(buffer) {
    free(buffer);
    buffer = NULL;
  }

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_get_new_key_reference *new_key_reference=0x%X r=%d\n", (new_key_reference) ? *new_key_reference : 0, r);
  return r;
}

static int ceres_create_file(struct sc_card *card, struct sc_file *file)
{
  int r = SC_SUCCESS;
  assert(file!=NULL);

  SC_FUNC_CALLED(card->ctx, 1);

  if(!ceres_is_virtual_fs_active(card)) {
    r = SC_ERROR_NOT_ALLOWED;
    goto end;
  }

  if (card->ctx->debug) sc_debug(card->ctx, "path: %s", sc_print_path(&file->path));
  
  /* we only allow full path */
  if(file->path.type != SC_PATH_TYPE_PATH) {
    r = SC_ERROR_INVALID_ARGUMENTS;
    goto end;
  }

  /*
    We only allow creation of public key and certificate files.
    If not we return SC_ERROR_NOT_ALLOWED.

    Now we admit to create Data objects

    \note We sometimes receive path to 3f005015<file-id> or
    3f00<file-id>. So we accept both.
  */
  if((file->path.len != 0x4) && (file->path.len != 0x6)) {
    /* invalid path */
    r = SC_ERROR_NOT_ALLOWED;
    goto end;
  }

  if((file->path.len == 0x4) && (memcmp(file->path.value, "\x3f\x00", 2) != 0)) {
    /* invalid path */
    r = SC_ERROR_NOT_ALLOWED;
    goto end;
  }

  if((file->path.len == 0x6) && (memcmp(file->path.value, "\x3f\x00\x50\x15", 4) != 0)) {
    /* invalid path */
    r = SC_ERROR_NOT_ALLOWED;
    goto end;
  }

  switch(file->path.value[file->path.len-2]) {
  case 0x20:
  case 0x21:
    /* public or private key */
    /* this is a public key file. it gets no sync. we import public key data along with private keys or generate it with private key */
    r = virtual_fs_append_new_virtual_file(DRVDATA(card)->virtual_fs, &file->path, NULL, 0, file->size, 1, virtual_file_sync_state_unknown, NULL, virtual_file_sync_state_unknown, NULL);
    if(r != SC_SUCCESS)
      goto end;
    if (card->ctx->debug) sc_debug(card->ctx, "Public key file created\n");
    break;
  case 0x22:
    /* certificate */
    /* this is a certificate file. it gets double sync. */
    r = virtual_fs_append_new_virtual_file( DRVDATA(card)->virtual_fs, 
					    &file->path, 
					    NULL, 
					    0, 
					    file->size, 
					    1, 
					    virtual_file_sync_state_synced, 
					    ceres_sync_card_to_virtual_fs_certificate_file_callback, 
					    virtual_file_sync_state_sync_pending, 
					    ceres_sync_virtual_fs_to_card_certificate_file_callback );
    if(r != SC_SUCCESS)
      goto end;
    if (card->ctx->debug) sc_debug(card->ctx, "certificate file created\n");
    break;
  case 0x24:
    /* Data Object (private)*/
    r = virtual_fs_append_new_virtual_file( DRVDATA(card)->virtual_fs,
                                            &file->path,
                                            NULL,
                                            0,
                                            file->size,
                                            1,
                                            virtual_file_sync_state_synced,
                                            ceres_sync_card_to_virtual_fs_data_object_file_callback,
                                            virtual_file_sync_state_sync_pending,
                                            ceres_sync_virtual_fs_to_card_data_object_file_callback );
    if(r != SC_SUCCESS)
      goto end;
    if (card->ctx->debug) sc_debug(card->ctx, "Data Object file created\n");
    break;
  default:
    r = SC_ERROR_NOT_ALLOWED;
    goto end;
  }

 end:
  SC_FUNC_RETURN(card->ctx, 2, r);
}

static int ceres_delete_cert (struct sc_card *card, const sc_path_t *path)
{
  int r = SC_SUCCESS;
  sc_file_t *unusedspace_file=NULL, *certfile = NULL;
  sc_path_t tmp_path, unusedspace_path, cert_prefix;
  sc_pkcs15_card_t *temp_pkcs15_card  = NULL;
  u8 *buf=NULL, *usf_buf=NULL;;
  size_t buflen=0;
     
  sc_format_path("3f006061", &cert_prefix);
  memcpy(&tmp_path,path,sizeof(sc_path_t));
  sc_concatenate_path(&cert_prefix,&cert_prefix,&tmp_path);

  /*!< backup of use_virtual_fs */
  int old_use_virtual_fs = ceres_is_virtual_fs_active(card); 

  assert(card!=NULL && path!=NULL);

  SC_FUNC_CALLED(card->ctx, 1);

 // memcpy(&tmp_path, path, sizeof(struct sc_path));
  memcpy(&tmp_path, &cert_prefix, sizeof(sc_path_t));

  /* Select certificate to knows the real size */
//  r = sc_select_file(card, path, &certfile);
  r = sc_select_file(card, &cert_prefix, &certfile);
  if(r!=SC_SUCCESS)
    goto end;
  if(!certfile) {
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }
  tmp_path.count = certfile->size;
  
  /* create a new pkcs15_card structure */
  temp_pkcs15_card = sc_pkcs15_card_new();
  if(!temp_pkcs15_card) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto end;
  }
  temp_pkcs15_card->card = card;


  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 1);


  /* parse PKCS#15 UNUSED SPACE */
  memset(&unusedspace_path, 0, sizeof(struct sc_path));
  sc_format_path("3F0050155033", &unusedspace_path);
  r = sc_select_file(card, &unusedspace_path, &unusedspace_file);
  if(r!=SC_SUCCESS)
    goto end;
  if(!unusedspace_file) {
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }
  if (unusedspace_file->size<=0) 
    unusedspace_file->size = 600;

  buflen = unusedspace_file->size;
  buf = calloc(1, buflen);
  if(!buf) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto end;
  }
     
  r = sc_read_binary(card, 0, buf, unusedspace_file->size, 0);
  if(r<=0) {
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }

  r = sc_pkcs15_parse_unusedspace(buf, buflen, temp_pkcs15_card);
  if(r!=SC_SUCCESS)
    goto end;

  if(buf) {
    memset(buf,0, buflen);
    free(buf);
    buf=NULL;
    buflen=0;
  }

  /* add a new unused space structure to be reused on possible future needs */
  r = sc_pkcs15_add_unusedspace(temp_pkcs15_card, &tmp_path, NULL);
  if(r!=SC_SUCCESS)
    goto end;

  r = sc_pkcs15_encode_unusedspace(temp_pkcs15_card->card->ctx, temp_pkcs15_card, &buf, &buflen);
  if (r < 0)
    goto end;

  r = sc_select_file(card, &unusedspace_path, &unusedspace_file);
  if(r!=SC_SUCCESS)
    goto end;

  if (unusedspace_file) {
    if (unusedspace_file->size<buflen) {
      r = SC_ERROR_OUT_OF_MEMORY;
      goto end;
    }
    usf_buf = calloc(1, unusedspace_file->size);
    if (!usf_buf) {
      r = SC_ERROR_OUT_OF_MEMORY;
      goto end;
    }
    /* copies encoded unused space and set to zeroes
       all other memory */
    memcpy(usf_buf, buf, buflen);
  } else {
    /* unused space file not created */
    r = SC_ERROR_OUT_OF_MEMORY;
    goto end;
  }
  
  r = sc_update_binary(temp_pkcs15_card->card, 0, usf_buf, unusedspace_file->size, 0);
  if(r < 0)
    goto end;
  if (r == unusedspace_file->size)
    r = SC_SUCCESS;

 end:
  if(buf){
    free(buf);
    buf=NULL;
  }
  if(usf_buf){
    free(usf_buf);
    usf_buf=NULL;
  }
  if(certfile)
    sc_file_free(certfile);
  if(unusedspace_file)
    sc_file_free(unusedspace_file);
  if ( temp_pkcs15_card ) { 
    /* set to NULL without freeing because we reused structure */
    temp_pkcs15_card->card = NULL;
    
    /* now free temp structure */
    sc_pkcs15_card_free(temp_pkcs15_card);
    temp_pkcs15_card = NULL;
  }

  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  SC_FUNC_RETURN(card->ctx, 1, r);  
}

static int ceres_middle_delete_file(struct sc_card *card, const sc_path_t *card_path){

	int r;
	struct sc_ceresctl_card_delete_file_info file_info_parsed;
	unsigned int *key_ref = NULL;
	unsigned int *key_type = NULL;
	sc_path_t tmppath, cert_prefix;
	sc_path_t keys_path;

	assert(card!=NULL && card_path!=NULL);

	sc_format_path("3f006061", &cert_prefix);

	if(card_path->len>1 && card_path->value[0] == 0x70){
	
		file_info_parsed.type =  SC_PKCS15_TYPE_CERT_X509 ;
	
		sc_format_path("3F006061", &tmppath);
	
		ceres_select_file(card,&tmppath,NULL);
	}
	else
	{
		sc_format_path("3f003f11",&keys_path);

		memcpy(&tmppath,card_path,sizeof(sc_path_t));

		sc_concatenate_path(&keys_path, &keys_path,&tmppath);

		map_path_to_key_info_t *path_to_key_info;
		
		path_to_key_info = DRVDATA(card)->card_path_to_card_keyinfo_map;

		r = map_path_to_key_info_find(path_to_key_info,&keys_path,&key_type,&key_ref);

		if (r!=SC_SUCCESS)
		{	
			if (card->ctx->debug) sc_debug(card->ctx, "We haven't find the key in the map_path");
		}
		
		file_info_parsed.key_reference = (unsigned int)key_ref;
		file_info_parsed.type = (unsigned int)key_type;
	}

	file_info_parsed.path = *card_path;
	
	r = ceres_delete_file(card, &file_info_parsed);

	
	return r;
}

static int ceres_delete_file (struct sc_card *card, const struct sc_ceresctl_card_delete_file_info *file_info)
{
  int r = SC_SUCCESS;

  assert(card!=NULL && file_info!=NULL);

  SC_FUNC_CALLED(card->ctx, 1);

  /* last two bytes are type of object and object reference */
  switch(file_info->type) {
   case SC_PKCS15_TYPE_PUBKEY_RSA:
    /* key objects are deleted only on delete private key call */
    /* OpenSC will delete PKCS#15 public key info */
     r = SC_SUCCESS;
     break;
   case SC_PKCS15_TYPE_PRKEY_RSA:  
   /* also deletes public key file */
    r = ceres_delete_key(card, file_info->key_reference);
    break;
   case SC_PKCS15_TYPE_CERT_X509:
    r = ceres_delete_cert(card, &file_info->path);
    break;
   default:
    r = SC_ERROR_INVALID_DATA;
  }

  SC_FUNC_RETURN(card->ctx, 1, r);
}


static int ceres_select_crypto_file (struct sc_card *card)
{
  
  int r = SC_SUCCESS;
  sc_path_t       crypto_path;
  sc_file_t       *crypto_df_file;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  assert(card!=NULL);


 /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

 /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

 /* Selecting ICC Crypto file */
  sc_format_path("3F003F11", &crypto_path);
  r = sc_select_file(card, &crypto_path, &crypto_df_file);
  if(r!=SC_SUCCESS)
    goto out;

 /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  out:
  SC_FUNC_RETURN(card->ctx, 1, r);
}


static int ceres_ctl(struct sc_card *card, unsigned long cmd, void *ptr)
{ 
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_ctl\n");
  
  switch (cmd) {
  case SC_CARDCTL_GET_SERIALNR:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling function ceres_get_serialnr\n"); 
    return ceres_get_serialnr(card, (sc_serial_number_t *) ptr);
  case SC_CERESCTL_GENERATE_KEY:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling function ceres_generate_key\n");
    return ceres_generate_key( card, (struct sc_ceresctl_card_genkey_info *)ptr );
  case SC_CERESCTL_STORE_KEY_COMPONENT:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling funciton ceres_store_key_component\n");
    return ceres_store_key_component(card,(struct sc_ceresctl_card_store_key_component_info *)ptr);
  case SC_CERESCTL_GET_NEW_KEY_REFERENCE:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling funciton ceres_get_new_key_reference\n");
    return ceres_get_new_key_reference(card, (int *)ptr);
  case SC_CERESCTL_CREATE_FILE:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling funciton ceres_create_file\n");
    return ceres_create_file(card, (struct sc_file *)ptr);
  case SC_CERESCTL_DELETE_FILE:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling funciton ceres_delete_file\n");
    return ceres_delete_file(card, (struct sc_ceresctl_card_delete_file_info *)ptr);
  case SC_CERESCTL_SELECT_CRYPTO_FILE:
    if (card->ctx->debug) sc_debug(card->ctx, "Calling funciton ceres_select_crypto_file\n");
    return ceres_select_crypto_file(card);
  default:
    return SC_ERROR_NOT_SUPPORTED;
  }	
  
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_ctl\n"); 
  return 0;
}

static int ceres_read_binary(sc_card_t *card,
			    unsigned int idx, u8 *buf, size_t count,
			    unsigned long flags)
{
  int r = SC_SUCCESS;
  virtual_file_t *virtual_file = NULL;
  sc_apdu_t apdu;
  u8 recvbuf[SC_MAX_APDU_BUFFER_SIZE];
 
  SC_FUNC_CALLED(card->ctx, 1);
  
  if(ceres_is_virtual_fs_active(card)) {
    /* we get file from our virtual_fs */
    virtual_file = virtual_fs_find_by_path(DRVDATA(card)->virtual_fs, &DRVDATA(card)->current_path);
    if(!virtual_file) {
      /* this should be impossible.
	 we should really have a virtual_file here.
	 if not something happened with the internal logic */
      r = SC_ERROR_INTERNAL;
      goto crb_end;
    }

    if(!virtual_file->is_ef) {
      r = SC_ERROR_NOT_ALLOWED;
      goto crb_end;
    }
    
    /* synchronizes if needed from the card to the virtual fs */
    r = virtual_file_data_synchronize(virtual_file, card, virtual_file_sync_type_card_to_virtual_fs, DRVDATA(card)->virtual_fs);
    if (r != SC_SUCCESS) {
      sc_error(card->ctx, "Synchronization failed\n");
      goto crb_end;
    }

    r = virtual_file_data_read(virtual_file, idx, buf, count);
  } else {
    /* we get file as usual from the card */
    assert(count <= card->max_recv_size);
    sc_format_apdu(card, &apdu, SC_APDU_CASE_2_SHORT, 0xB0,
                   (idx >> 8) & 0x7F, idx & 0xFF);
    apdu.le = count;
    apdu.resplen = count;
    apdu.resp = recvbuf;
    
    r = ceres_transmit_apdu(card, &apdu);
    SC_TEST_RET(card->ctx, r, "APDU transmit failed");
    if (apdu.resplen == 0)
      SC_FUNC_RETURN(card->ctx, 2, ceres_check_sw(card, apdu.sw1, apdu.sw2));
    memcpy(buf, recvbuf, apdu.resplen);
  }
  
 crb_end:
  if (r == SC_SUCCESS)
    r = count;

  SC_FUNC_RETURN(card->ctx, 1, r);
}

static int ceres_update_binary(sc_card_t *card,
                              unsigned int idx, const u8 *buf,
                              size_t count, unsigned long flags)
{
  virtual_file_t *virtual_file = NULL;
  int r = SC_SUCCESS;

  SC_FUNC_CALLED(card->ctx, 1);

  if(ceres_is_virtual_fs_active(card)) {

    /* we get file from our virtual_fs */
    virtual_file = virtual_fs_find_by_path(DRVDATA(card)->virtual_fs, &DRVDATA(card)->current_path);
    if(!virtual_file) {
      /* this should be impossible.
	 we should really have a virtual_file here.
	 if not something happened with the internal logic */
      r = SC_ERROR_INTERNAL;
      goto end;
    }

    if(!virtual_file->is_ef) {
      r = SC_ERROR_NOT_ALLOWED;
      goto end;
    }
  
    /* synchronizes if needed from the card to the virtual fs */
    r = virtual_file_data_synchronize(virtual_file, card, virtual_file_sync_type_card_to_virtual_fs, DRVDATA(card)->virtual_fs);
    if (r != SC_SUCCESS) {
      sc_error(card->ctx, "Synchronization failed\n");
      goto end;
    }

    /* the update binary in our virtual fs */
    r = virtual_file_data_update(virtual_file, idx, buf, count);
    if(r != SC_SUCCESS) {
      sc_error(card->ctx, "Data update failed\n");
      goto end;
    }

    /* synchronizes if needed from the virtual fs to the card */
    r = virtual_file_data_synchronize(virtual_file, card, virtual_file_sync_type_virtual_fs_to_card, DRVDATA(card)->virtual_fs);
    if (r != SC_SUCCESS) {
      sc_error(card->ctx, "Synchronization failed\n");
      goto end;
    }
  } else {
    r = iso_ops->update_binary( card, idx, buf, count, flags);
    if (r>0 && r == count)
      r = SC_SUCCESS;
  }
 end:

  if (r == SC_SUCCESS)
    r = count;

  SC_FUNC_RETURN(card->ctx, 1, r);

}



static int ceres_logout(struct sc_card *card)
{

  /* reset flag secure channel as not created */
  ((struct card_priv_data *) card->drv_data)->secure_channel_state = secure_channel_not_created;

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_logout");
  return 0;
}

static void ceres_add_acl_entry(sc_card_t *card, sc_file_t *file, int op, u8 byte)
{
  unsigned int method, key_ref = SC_AC_KEY_REF_NONE;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_add_acl_entry\n"); 

  switch (byte >> 4) {
  case 0:
    method = SC_AC_NONE;
    break;
  case 1:
    method = SC_AC_CHV;
    key_ref = byte & 0x0F;	       
    break;
  case 3:
    method = SC_AC_CHV;
    key_ref = byte & 0x0F;	       
    break;
  case 4:
    method = SC_AC_TERM;
    key_ref = byte & 0x0F;
    break;
  case 15:
    method = SC_AC_NEVER;
    break;
  default:
    method = SC_AC_UNKNOWN;
    break;
  }
  sc_file_add_acl_entry(file, op, method, key_ref);

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_add_acl_entry\n"); 
}

static void ceres_parse_sec_attr(sc_card_t *card, sc_file_t *file, const u8 * buf, size_t len)
{
  int i;
  int idx[4];

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_parse_sec_attr\n"); 

  if (len < 4)
    return;
  if (file->type == SC_FILE_TYPE_DF) {
    const int df_idx[4] = {
      SC_AC_OP_CREATE, SC_AC_OP_DELETE ,
      SC_AC_OP_REHABILITATE, SC_AC_OP_INVALIDATE
    };
    for (i = 0; i <4; i++)
      idx[i] = df_idx[i];
  } else {
    const int ef_idx[4] = {
      SC_AC_OP_READ, SC_AC_OP_UPDATE,
      SC_AC_OP_REHABILITATE, SC_AC_OP_INVALIDATE
    };
    for (i = 0; i < 4; i++)
      idx[i] = ef_idx[i];
  }
  for (i = 0; i < 4; i++)
    ceres_add_acl_entry(card, file, idx[i], buf[i]);

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_parse_sec_attr\n"); 
}

static int ceres_iso_build_pin_apdu(sc_card_t *card, sc_apdu_t *apdu,
			      struct sc_pin_cmd_data *data, u8 *buf, size_t buf_len)
{
  int r, len = 0, pad = 0, use_pin_pad = 0, ins, p1 = 0;
  /*!< backup of use_virtual_fs */

  switch (data->pin_type) {
  case SC_AC_CHV:
    break;
  default:
    return SC_ERROR_INVALID_ARGUMENTS;
  }

  
  if (data->flags & SC_PIN_CMD_NEED_PADDING)
    pad = 1;
  if (data->flags & SC_PIN_CMD_USE_PINPAD)
    use_pin_pad = 1;

  data->pin1.offset = 5;

  switch (data->cmd) {
  case SC_PIN_CMD_VERIFY:
    ins = 0x20;
    if ((r = sc_build_pin(buf, buf_len, &data->pin1, pad)) < 0)
      return r;
    len = r;
    break;
  case SC_PIN_CMD_CHANGE:
    ins = 0x24;
    if (data->pin1.len != 0 || use_pin_pad) {
      if ((r = sc_build_pin(buf, buf_len, &data->pin1, pad)) < 0)
	return r;
      len += r;
    } else {
      /* implicit test */
      p1 = 1;
    }
    
    data->pin2.offset = data->pin1.offset + len;
    if ((r = sc_build_pin(buf+len, buf_len-len, &data->pin2, pad)) < 0)
      return r;
    len += r;
    break;
  case SC_PIN_CMD_UNBLOCK:
    ins = 0x2C;
    if (data->pin1.len != 0 || use_pin_pad) {
      if ((r = sc_build_pin(buf, buf_len, &data->pin1, pad)) < 0)
	return r;
      len += r;
    } else {
      p1 |= 0x02;
    }

    if (data->pin2.len != 0 || use_pin_pad) {
      data->pin2.offset = data->pin1.offset + len;
      if ((r = sc_build_pin(buf+len, buf_len-len, &data->pin2, pad)) < 0)
	return r;
      len += r;
    } else {
      p1 |= 0x01;
    }
    break;
  default:
    return SC_ERROR_NOT_SUPPORTED;
  }

  sc_format_apdu(card, apdu, SC_APDU_CASE_3_SHORT,
		 ins, p1, data->pin_reference);

  apdu->lc = len;
  apdu->datalen = len;
  apdu->data = buf;
  apdu->resplen = 0;
  apdu->sensitive = 1;

  return 0;
}

static int ceres_build_pin_apdu(struct sc_card *card,
			       struct sc_apdu *apdu,
			       struct sc_pin_cmd_data *data)
{ 
  u8 sbuf[SC_MAX_APDU_BUFFER_SIZE];
  int r, len=0, pad = 0, cla=0, ins, p1 = 0, p2 = 0;	
  int old_use_virtual_fs = ceres_is_virtual_fs_active(card); 
  sc_path_t pin_path;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_build_pin_apdu\n");

  switch (data->pin_type) {
  case SC_AC_CHV:
    break;
  default:
    return SC_ERROR_INVALID_ARGUMENTS;
  }

  if (data->flags & SC_PIN_CMD_NEED_PADDING)
    pad = 1;

  switch (data->cmd) {
  case SC_PIN_CMD_VERIFY:

    data->pin1.offset = 0;
    if ((r = sc_build_pin(sbuf, sizeof(sbuf), &data->pin1, pad)) < 0)
      return r;
    len = r;

    cla = 0x00;
    ins = 0x20;
    p1 = 0;
    /* ignore pin_reference */
    p2 = 0x00;
    break;
  case SC_PIN_CMD_CHANGE:
  if (card->ctx->debug) sc_debug(card->ctx, "SC_PIN_CMD_CHANGE\n");

    /* we want to use card without virtual fs */
    ceres_set_virtual_fs_state(card, 1);

    memset(&pin_path, 0, sizeof(sc_path_t));
    sc_format_path("3F000000", &pin_path);
    r = ceres_iso_select_file(card, &pin_path, NULL);
    if(r!=SC_SUCCESS)
      return r;

    /* we restore use_virtual_fs */
    ceres_set_virtual_fs_state(card, old_use_virtual_fs);

    cla = 0x90;
    ins = 0x24;
    p1 = 0x00;
    p2 = 0x00;

    sbuf[0] = 0x01;
    sbuf[1] = data->pin1.len;
    memcpy (sbuf+2, data->pin1.data, sizeof(u8) * data->pin1.len);
    sbuf[2+data->pin1.len] = data->pin2.len;
    memcpy (sbuf+3+data->pin1.len, data->pin2.data, sizeof(u8) * data->pin2.len);
    len = data->pin1.len + data->pin2.len + 3;
    break;
 case SC_PIN_CMD_UNBLOCK:
    /* we want to use card without virtual fs */
    ceres_set_virtual_fs_state(card, 1);

    memset(&pin_path, 0, sizeof(sc_path_t));
    sc_format_path("3F000000", &pin_path);
    r = ceres_iso_select_file(card, &pin_path, NULL);
    if(r!=SC_SUCCESS)
      return r;

    /* we restore use_virtual_fs */
    ceres_set_virtual_fs_state(card, old_use_virtual_fs); 
    cla = 0x90;
    ins = 0x2c;
    p1 = 0x00;
    p2 = 0x00;
    memcpy (sbuf, data->pin1.data, sizeof(u8) * data->pin1.len);
    sbuf[sizeof(u8)*data->pin1.len] = data->pin2.len;
    memcpy(sbuf+(sizeof(u8)*data->pin1.len)+1,data->pin2.data,sizeof(u8) * data->pin2.len);
    len = data->pin1.len+data->pin2.len + 1;
    break;
  default:
    return SC_ERROR_NOT_SUPPORTED;
  }

  memset(apdu, 0, sizeof(*apdu));
  apdu->cla = cla;
  apdu->cse = SC_APDU_CASE_3_SHORT;
  apdu->ins = (u8) ins;
  apdu->p1 = (u8) p1;
  apdu->p2 = (u8) p2;

  apdu->lc = len;
  apdu->datalen = len; 
  apdu->data = sbuf;
  apdu->resplen = 0;
  apdu->sensitive = 1;
  apdu->le = 0;

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_build_pin_apdu\n"); 

  return 0;
}



static int ceres_store_key_component_apdu(struct sc_card *card, 
				    struct sc_ceresctl_card_store_key_component_info *component_info, int key_usage )
{

  sc_apdu_t apdu;
  u8 *buf=NULL;
  int r = SC_SUCCESS;

  SC_FUNC_CALLED(card->ctx, 1);

  assert(card!=NULL && component_info!=NULL && component_info->component.value!=NULL);
  memset(&apdu, 0, sizeof(struct sc_apdu));

  sc_format_apdu(card, 
	 &apdu, 
	 SC_APDU_CASE_3_SHORT,
	 (component_info->private_component)? 0x52: 0x50, 
	 key_usage,  
	 component_info->key_id);       

  apdu.cla = 0x90;

  /* this function returns tlv length */
  apdu.lc = tlv2buf_special(&component_info->component, &buf);
  apdu.datalen = apdu.lc;
  apdu.data = buf;
  apdu.resplen = 0;
  apdu.le = 0;

  if (card->ctx->debug) sc_debug(card->ctx, "Sending component:\%X \n",component_info->component.tag);

  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  
  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
  switch (r) {
  case SC_SUCCESS:
    sc_debug (card->ctx, "Key stored correctly!");
    break;

  case SC_ERROR_OBJECT_ALREADY_EXISTS:
    /* Deleting the existing key and generate a new one */
    r = ceres_delete_key (card, component_info->key_id);
    if (r<0)
      goto cgk_store_err;

    r = ceres_transmit_apdu(card, &apdu);
    SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  
    r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
    if (r<0)
      goto cgk_store_err;
    break;

  default:
    sc_error (card->ctx, "Could not store the component!");
    goto cgk_store_err;
  }
  cgk_store_err:
  if (buf) {
    free(buf);
    buf=NULL;
  }
  
  
  SC_FUNC_RETURN(card->ctx, 1, r);
}


static int ceres_store_key_component(struct sc_card *card, 
				    struct sc_ceresctl_card_store_key_component_info *component_info )
{
  int r = SC_SUCCESS;
  int i = 0;
  int key_usage = 0;

  assert(card!=NULL && component_info!=NULL && component_info->component.value!=NULL);

  SC_FUNC_CALLED(card->ctx, 1);

  if (card->type == SC_CARD_TYPE_CERES_ST) {
    key_usage = SC_CARD_KEY_USAGE_SIG | SC_CARD_KEY_USAGE_CIF;
    r = ceres_store_key_component_apdu(card, component_info, key_usage);
  } else {
    for (i = 0; i < 2; i++)
    {
      if (i == 0)
        key_usage = SC_CARD_KEY_USAGE_SIG;
      else
        key_usage = SC_CARD_KEY_USAGE_CIF;

      r = ceres_store_key_component_apdu(card, component_info, key_usage);
    }
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}

static int ceres_iso_pin_cmd(sc_card_t *card, struct sc_pin_cmd_data *data,
		       int *tries_left)
{
  sc_apdu_t local_apdu, *apdu;
  int r;
  u8  sbuf[SC_MAX_APDU_BUFFER_SIZE];
  
  if (tries_left)
    *tries_left = -1;

  /* See if we've been called from another card driver, which is
   * passing an APDU to us (this allows to write card drivers
   * whose PIN functions behave "mostly like ISO" except in some
   * special circumstances.
   */
  if (data->apdu == NULL) {
    r = ceres_iso_build_pin_apdu(card, &local_apdu, data, sbuf, sizeof(sbuf));
    if (r < 0)
      return r;
    data->apdu = &local_apdu;
  }
  apdu = data->apdu;
  /* Transmit the APDU to the card */
  r = ceres_transmit_apdu(card, apdu);
  
  /* Clear the buffer - it may contain pins */
  sc_mem_clear(sbuf, sizeof(sbuf));

  /* Don't pass references to local variables up to the caller. */
  if (data->apdu == &local_apdu)
    data->apdu = NULL;

  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  if (apdu->sw1 == 0x63) {
    if ((apdu->sw2 & 0xF0) == 0xC0 && tries_left != NULL)
      *tries_left = apdu->sw2 & 0x0F;
    return SC_ERROR_PIN_CODE_INCORRECT;
  }
  return ceres_check_sw(card, apdu->sw1, apdu->sw2);
}

int ceres_get_serialnr(sc_card_t *card, sc_serial_number_t *serial)
{
  int r;
  u8  rbuf[17];
  sc_apdu_t apdu;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_get_serialnr\n"); 


  if (card->type != SC_CARD_TYPE_CERES_INFINEON && card->type != SC_CARD_TYPE_CERES_ST)
    return SC_ERROR_NOT_SUPPORTED;

  if (!serial)
    return SC_ERROR_INVALID_ARGUMENTS;
  /* see if we have cached serial number */
  if (card->serialnr.len) {
    memcpy(serial, &card->serialnr, sizeof(*serial));
    return SC_SUCCESS;
  }

  /* check if serial channel has been created and create it if not */
  if((r = ceres_assure_secure_channel(card)) != SC_SUCCESS)
    return r;
    
  /* get serial number via APDU */
  sc_format_apdu(card, &apdu, SC_APDU_CASE_2_SHORT, 0xb8, 0x00, 0x00);
  apdu.cla = 0x90;
  apdu.resp = rbuf;
  apdu.resplen = sizeof(rbuf);
  apdu.le   = 0x11;
  apdu.lc   = 0;
  apdu.datalen = 0;
  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  if (apdu.sw1 != 0x90 || apdu.sw2 != 0x00) {
    if (card->ctx->debug) sc_error(card->ctx, "ERROR: SW1:0x%x, SW2:0x%x\n", apdu.sw1, apdu.sw2); 
    return SC_ERROR_INTERNAL;
  }
  /* cache serial number */
  memcpy(card->serialnr.value, apdu.resp, 7*sizeof(u8)); /*apdu.resplen);*/
  card->serialnr.len = 7*sizeof(u8); /* apdu.resplen; */
  /* copy and return serial number */
  memcpy(serial, &card->serialnr, sizeof(*serial));

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_get_serialnr\n"); 

  return SC_SUCCESS;
}  

int ceres_envelope_transmit (sc_card_t *card, sc_apdu_t *tx)
{
  sc_apdu_t envelope_apdu;
  u8 corrected_tx[1024], envelope_data[1024];
  unsigned int len = 0, temp = 0, length = 0, total = 0;
  int r=0;

  memset(corrected_tx, 0, 1024);
  memset(envelope_data, 0, 1024);

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_envelope_transmit");

  /* set correct p3 and slice commands if necessary */
  if (tx->lc > 255) {       
    corrected_tx[len++] = tx->cla;     /* CLA */
    corrected_tx[len++] = tx->ins;     /* INS */
    corrected_tx[len++] = tx->p1;      /* P1 */
    corrected_tx[len++] = tx->p2;      /* P2 */
    /* code data length */
    corrected_tx[len++] = 0x00;        /* 1st byte */
    corrected_tx[len++] = tx->lc>>8;   /* 2nd byte */
    corrected_tx[len++] = tx->lc&0xff; /* 3rd byte */

    /* add data */
    memcpy(corrected_tx+len,tx->data, tx->lc);

    /* total bytes */
    total = 7+tx->lc;

    /* next block length */
    length = 0;

    /* process all blocks */
    for (temp=0; temp<total; temp+=length) {
      length = ((total-temp)>255) ? 255 : total-temp;      

      /* prepare envelope apdu header */
      sc_format_apdu(card, &envelope_apdu, tx->cse, 0xC2, 0x00, 0x00);

      envelope_apdu.cla = 0x90;
      envelope_apdu.data = envelope_data;

      envelope_apdu.resp = tx->resp;
      envelope_apdu.resplen = tx->resplen;
      envelope_apdu.le = tx->le;
      
      /* P3 */
      envelope_apdu.lc = length;
      envelope_apdu.datalen = length;      

      /* copy next block */
      memcpy(envelope_data, corrected_tx+temp, length);            
      
      /* if secure channel is created, a get_response ALWAYS must be sent */
      if((((struct card_priv_data *) card->drv_data)->secure_channel_state == secure_channel_created) &&
	 (envelope_apdu.cse==SC_APDU_CASE_3_SHORT) &&
	 (envelope_apdu.resplen>0)) {
	envelope_apdu.cse = SC_APDU_CASE_4_SHORT;
	envelope_apdu.le = envelope_apdu.resplen > 255 ? 255 : envelope_apdu.resplen;
      }

      r = sc_transmit_apdu(card, &envelope_apdu);
      if (r != SC_SUCCESS)
	goto dea_err;
    }

    tx->resplen = envelope_apdu.resplen;
    tx->sw1 = envelope_apdu.sw1;
    tx->sw2 = envelope_apdu.sw2;
  } else {
    /* no envelope needed */
    int tmp_cse = tx->cse;

    /* if secure channel is created, a get_response ALWAYS must be sent */
    if((((struct card_priv_data *) card->drv_data)->secure_channel_state == secure_channel_created) &&
       (tmp_cse==SC_APDU_CASE_3_SHORT) &&
       (tx->resplen>0)) {
      tx->cse = SC_APDU_CASE_4_SHORT;
      tx->le = tx->resplen > 255 ? 255 : tx->resplen;
    }
    
    r = sc_transmit_apdu(card, tx);

    tx->cse=tmp_cse;        
  }
  
 dea_err:
  
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_envelope_transmit");
 
  return r;
}
static int card_secure_transmit(sc_card_t *card, sc_apdu_t *tx)
{
  SC_FUNC_RETURN(card->ctx, 1, SC_ERROR_NOT_SUPPORTED);
}
int ceres_transmit_apdu(sc_card_t *card, sc_apdu_t *tx)
{
  int r=0;
  int retries=3;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_transmit_apdu");

  if (((struct card_priv_data *) card->drv_data)->secure_channel_state == secure_channel_created){
    r = card_secure_transmit(card, tx);

    while((((tx->sw1==0x66) && (tx->sw2==0x88)) ||  /* The value of the securized message is incorrect*/
           ((tx->sw1==0x69) && ((tx->sw2==0x87) || (tx->sw2==0x88)))) && /* The value of the securized message is incorrect*/
          (retries != 0)){
        
      r = card_secure_transmit(card, tx);
      retries--;
    }
  } 
  else
    r = ceres_envelope_transmit(card, tx);
  
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_transmit_apdu");
  
  return r;
}

static int ceres_generate_key( sc_card_t *card, struct sc_ceresctl_card_genkey_info *genkey_info )
{
  sc_apdu_t  apdu;
  int     r = SC_SUCCESS;
  tlv_t tlv_tmp;
  u8    *buf = NULL;
  u8	data[2];
  data[0] = (genkey_info->pubkey_len < 512) ? ((genkey_info->pubkey_len * 8)& 0xFF00) >>8 : ((genkey_info->pubkey_len)& 0xFF00) >>8 ;
  data[1] = (genkey_info->pubkey_len < 512) ? (genkey_info->pubkey_len * 8) & 0x00FF : (genkey_info->pubkey_len) & 0x00FF;
  
  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_generate_key");
  
  /* do the apdu thing */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.ins = (u8) 0x76;
  apdu.p1 = genkey_info->key_usage;
  apdu.p2 = genkey_info->key_reference;
  // consider 2048 generation
  if (genkey_info->pubkey_len==256 || genkey_info->pubkey_len==2048 ){ 
     apdu.lc = 4;
     r = buf2tlv(0x80,data,2,&tlv_tmp); //the size of the key
     SC_TEST_RET(card->ctx, r, "APDU creation failed");
     r = tlv2buf_normal(&tlv_tmp,&buf);
     SC_TEST_RET(card->ctx, r, "BUF creation failed");
     apdu.data = buf;
     apdu.datalen = 4;
     apdu.cse = SC_APDU_CASE_3;
  }
  else {
    apdu.lc = 0; /* use default exponent */
    apdu.cse = SC_APDU_CASE_1;
  }
  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  
  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
  switch (r) {
  case SC_SUCCESS:
    sc_debug (card->ctx, "Key generated correctly!");
    break;

  case SC_ERROR_OBJECT_ALREADY_EXISTS:
    /* Deleting the existing key and generate a new one */
    r = ceres_delete_key (card, genkey_info->key_reference);
    if (r<0)
      goto cgk_err;

    r = ceres_transmit_apdu(card, &apdu);
    SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  
    r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
    if (r<0)
      goto cgk_err;
    break;

  default:
    sc_error (card->ctx, "Could not generate new key pair!");
    goto cgk_err;
  }

  r = ceres_get_public_key( card, genkey_info );
  if (r<0) {
    sc_error (card->ctx, "Could not get public key!");
    goto cgk_err;
  }
  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_generate_key");

 cgk_err:

  return r;
}

static int ceres_get_public_key( struct sc_card *card, struct sc_ceresctl_card_genkey_info *genkey_info ) 
{
  int r;
  sc_apdu_t  apdu;
  sc_path_t key_path;
  u8        resbuf[256];
  u8        data[1];
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  SC_FUNC_CALLED(card->ctx, 1);

  sc_format_path("3F003F11", &key_path);
  
  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);
  
  r = ceres_select_file(card, &key_path, NULL);
  SC_TEST_RET(card->ctx, r, "Seleccion Directorio Claves fallida");

  /* GET MODULUS */
  data[0] = 0x14;
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_4_SHORT;
  apdu.ins = (u8) 0x56;
  apdu.p1 = (genkey_info->key_usage & 0x80) ? 0x80 : 40; /* we must use only 0x80 or 0x40 in this command */
  apdu.p2 = genkey_info->key_reference;
  apdu.lc = 0x01;
  apdu.data = data;
  apdu.datalen = 0x01;
  apdu.le = (genkey_info->pubkey_len < 512) ? genkey_info->pubkey_len : genkey_info->pubkey_len / 8;
  
  apdu.resp = resbuf;
  apdu.resplen = sizeof(resbuf);

  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");

  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
  if (r<0) {
    sc_error (card->ctx, "Could not get modulus!");
    goto cgpk_err;
  }  
  
  if ( genkey_info->pubkey == NULL ) {
    if (card->ctx->debug) sc_debug(card->ctx, "Pubkey buffer NULL\n");
    r=SC_ERROR_OUT_OF_MEMORY;
    goto cgpk_err;
  }
  if ( (apdu.resplen!=genkey_info->pubkey_len && genkey_info->pubkey_len < 512) || (apdu.resplen!=genkey_info->pubkey_len/8 && genkey_info->pubkey_len > 512)) {
    if (card->ctx->debug) sc_debug(card->ctx, "APDU response buffer not big enough\n");
    r=SC_ERROR_WRONG_LENGTH;
    goto cgpk_err;
  }
  if (genkey_info->pubkey_len < 512){ //pubkey_len is in bytes or in bits?
  	memcpy(genkey_info->pubkey, resbuf, genkey_info->pubkey_len);
  }
  else
  {
  	memcpy(genkey_info->pubkey, resbuf, genkey_info->pubkey_len/8);
  }
  /* GET EXPONENT */
  data[0] = 0x12;
  apdu.lc = 0x01;
  apdu.data = data;
  apdu.datalen = 0x01;
  apdu.le = (genkey_info->pubkey_len < 512) ? genkey_info->pubkey_len : genkey_info->pubkey_len / 8;
  apdu.resp = resbuf;
  apdu.resplen = sizeof(resbuf);


  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");

  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);
  if (r<0) {
    sc_error (card->ctx, "Could not get exponent!");
    goto cgpk_err;
  }  
  /* At least three bytes of exponent */
  if (apdu.resplen>genkey_info->exponent_len) {
    if (card->ctx->debug) sc_debug(card->ctx, "Exponent response buffer not big enough\n");
    r=SC_ERROR_WRONG_LENGTH;
    goto cgpk_err;
  }

  memcpy(genkey_info->exponent, resbuf, apdu.resplen);
  genkey_info->exponent_len = apdu.resplen;
  
 cgpk_err:
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  SC_FUNC_RETURN(card->ctx, 1, r);
}

static int ceres_delete_key(struct sc_card *card, const unsigned int key_reference)
{
  sc_apdu_t  apdu;
  sc_path_t  key_path;
  int     r;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */
  int aux_sw1 = 0;
  int aux_sw2 = 0;

  SC_FUNC_CALLED(card->ctx, 1);

  sc_format_path("3F003F11", &key_path);
  
  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card, 0);

  r = ceres_select_file(card, &key_path, NULL);
  SC_TEST_RET(card->ctx, r, "Key Path selection failed");
  if (r<0)
    goto cdk_err;

  /* DELETE KEY AS A BOTH USAGES ONE */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_1;
  apdu.ins = (u8) 0x54;
  apdu.p1 = SC_CARD_KEY_USAGE_SIG | SC_CARD_KEY_USAGE_CIF;
  apdu.p2 = key_reference;
  apdu.data = 0;
  apdu.datalen = 0;
  apdu.lc = 0;

  r = sc_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
  if (r<0)
    goto cdk_err;

  /* P1 incorrect */
  if(apdu.sw1==0x6A && apdu.sw2==0x86) {
    /* DELETE KEY AS A KEY SIGNATURE ONE */
    apdu.p1 = SC_CARD_KEY_USAGE_SIG;
    r = sc_transmit_apdu(card, &apdu);
    SC_TEST_RET(card->ctx, r, "APDU transmit failed");
    if (r<0)
      goto cdk_err;
    if(apdu.sw1==0x90 && apdu.sw2==0x00) {
      aux_sw1 = apdu.sw1;
      aux_sw2 = apdu.sw2;
    }

    /* DELETE KEY AS A KEY EXCHANGE ONE */
    apdu.p1 = SC_CARD_KEY_USAGE_CIF;
    r = sc_transmit_apdu(card, &apdu);
    SC_TEST_RET(card->ctx, r, "APDU transmit failed");
    if (r<0)
      goto cdk_err;
  }

  if(aux_sw1==0x90 && aux_sw2==0x00) {
    apdu.sw1 = aux_sw1;
    apdu.sw2 = aux_sw2;
  }
 
  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);

 cdk_err:
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card, old_use_virtual_fs);

  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_create_data_object_file( sc_card_t *card, sc_path_t *path, size_t size )
{
  int r = SC_SUCCESS, ii=0;
  size_t datalen;
  u8 data[SC_MAX_APDU_BUFFER_SIZE];
  struct sc_apdu apdu;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_create_data_object_file\n");

  assert(card!=NULL && path!=NULL && size>0);

  if (path->len !=2) {
    r = SC_ERROR_INVALID_ARGUMENTS;
    goto cccf_err;
  }
  
  memset(data, 0, SC_MAX_APDU_BUFFER_SIZE);
  /* prepare data */
  data[ii] = 0x01;             /* file type=EF               */
  data[++ii] = path->value[0]; /* file id, high byte         */
  data[++ii] = path->value[1]; /* file id, low byte          */
  data[++ii] = size >> 8;      /* file size 2 bytes          */
  data[++ii] = size & 0xFF;
  data[++ii] = 0x10;           /* AC1 (Read Binary):   CHV   */
  data[++ii] = 0x10;           /* AC2 (Update Binary): CHV   */
  data[++ii] = 0xFF;           /* AC3 (Rehabilitar):   Never */
  data[++ii] = 0xFF;           /* AC4 (Invalidar):     Never */
  data[++ii] = 0xFF;           /* AC5 (Admin):         Never */

  datalen = ++ii;

  /* do the apdu thing */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_3_SHORT;
  apdu.ins = 0xE0;
  apdu.p1 = 0x01; /* file_type=EF */
  apdu.p2 = 0x00;
  apdu.lc = datalen;
  apdu.data = data;
  apdu.datalen = datalen;

  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
	
  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);

 cccf_err:

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_create_data_object_file\n");
  return r;
}




int ceres_create_cert_file( sc_card_t *card, sc_path_t *path, size_t size )
{
  int r = SC_SUCCESS, ii=0;
  size_t datalen;
  u8 data[SC_MAX_APDU_BUFFER_SIZE];
  struct sc_apdu apdu;

  if (card->ctx->debug) sc_debug(card->ctx, "Entering function ceres_create_cert_file\n");

  assert(card!=NULL && path!=NULL && size>0);

  if (path->len !=2) {
    r = SC_ERROR_INVALID_ARGUMENTS;
    goto cccf_err;
  }
  
  memset(data, 0, SC_MAX_APDU_BUFFER_SIZE);
  /* prepare data */
  data[ii] = 0x01;             /* file type=EF               */
  data[++ii] = path->value[0]; /* file id, high byte         */
  data[++ii] = path->value[1]; /* file id, low byte          */
  data[++ii] = size >> 8;      /* file size 2 bytes          */
  data[++ii] = size & 0xFF;
  data[++ii] = 0x00;           /* AC1 (Read Binary):   Alw   */
  data[++ii] = 0x00;           /* AC2 (Update Binary): Alw   */
  data[++ii] = 0xFF;           /* AC3 (Rehabilitar):   Never */
  data[++ii] = 0xFF;           /* AC4 (Invalidar):     Never */
  data[++ii] = 0xFF;           /* AC5 (Admin):         Never */

  datalen = ++ii;

  /* do the apdu thing */
  memset(&apdu, 0, sizeof(apdu));
  apdu.cla = 0x90;
  apdu.cse = SC_APDU_CASE_3_SHORT;
  apdu.ins = 0xE0;
  apdu.p1 = 0x01; /* file_type=EF */
  apdu.p2 = 0x00;
  apdu.lc = datalen;
  apdu.data = data;
  apdu.datalen = datalen;

  r = ceres_transmit_apdu(card, &apdu);
  SC_TEST_RET(card->ctx, r, "APDU transmit failed");
	
  r = ceres_check_sw(card, apdu.sw1, apdu.sw2);

 cccf_err:

  if (card->ctx->debug) sc_debug(card->ctx, "Leaving function ceres_create_cert_file\n");
  return r;
}

int ceres_assure_secure_channel(struct sc_card *card) { return SC_SUCCESS; };

static struct sc_card_driver * sc_get_driver(void)
{ 
  struct sc_card_driver *iso_drv = sc_get_iso7816_driver();

  card_ops = *iso_drv->ops;
  card_ops.match_card = ceres_match_card;
  card_ops.init = ceres_init;
  card_ops.finish = ceres_finish;
  if (iso_ops == NULL)
    iso_ops = iso_drv->ops;
  card_ops.create_file = ceres_create_file;
  card_ops.set_security_env = ceres_set_security_env; 

   card_ops.delete_file = ceres_middle_delete_file;

  card_ops.compute_signature = ceres_compute_signature;
  card_ops.decipher = ceres_decipher;
  card_ops.get_challenge = ceres_get_challenge;
  card_ops.pin_cmd = ceres_pin_cmd;
  card_ops.select_file = ceres_select_file;
  card_ops.check_sw=ceres_check_sw;
  card_ops.process_fci = ceres_process_fci;
  card_ops.card_ctl = ceres_ctl;	
  card_ops.read_binary=ceres_read_binary;
  card_ops.update_binary=ceres_update_binary;
  card_ops.logout=ceres_logout;
  
  return &card_drv;
}

/*
 * Called when module is loaded
 * you should return a pointer to sc_get_mymodule_driver();
 */ 
void * sc_module_init(const char *drivername)  
{   
  if (!drivername) 
    return NULL;
  if (strcmp(MODULE_NAME,drivername))
    return NULL;

  return sc_get_driver;  
}

char * sc_module_version()  
{ 
  return MODULE_VERSION;
}

char * sc_driver_version()  
{ 
  return MODULE_VERSION;
}

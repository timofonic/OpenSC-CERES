/*
 * pkcs15_standard.h: Header for definitions related to parsing of
 *                    standard PKCS#15 structures.
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */


#ifndef _PKCS15_STANDARD_H
#define _PKCS15_STANDARD_H

#ifdef __cplusplus
extern "C" {
#endif

#include "base_card.h"

int sc_standard_pkcs15_parse_df(struct sc_pkcs15_card *p15card, 
				sc_pkcs15_df_t *df,
				u8 *buf,
				size_t bufsize);
  
int sc_standard_pkcs15_encode_any_df(sc_context_t *ctx,
				     struct sc_pkcs15_card *p15card,
				       const unsigned int df_type,
				     u8 **buf_out, size_t *bufsize_out);

int sc_standard_pkcs15_encode_other_df(sc_context_t *ctx,
				       struct sc_pkcs15_card *p15card,
				       const unsigned int df_type,
				       u8 **buf_out, size_t *bufsize_out);  
#ifdef __cplusplus
}
#endif

#endif /* _PKCS15_STANDARD_H */

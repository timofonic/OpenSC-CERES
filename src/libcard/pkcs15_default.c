/*
 * pkcs15_default.c: PKCS #15 Ceres general functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "pkcs15_default.h"

static const struct sc_asn1_entry c_asn1_toki_ceres[] = {
  { "version",        SC_ASN1_INTEGER,      SC_ASN1_TAG_INTEGER, 0, NULL, NULL },
  { "serialNumber",   SC_ASN1_OCTET_STRING, SC_ASN1_TAG_OCTET_STRING, 0, NULL, NULL },
  { "manufacturerID", SC_ASN1_UTF8STRING, SC_ASN1_TAG_TELETEXSTRING, SC_ASN1_OPTIONAL, NULL, NULL },
  { "label",	    SC_ASN1_UTF8STRING, SC_ASN1_TAG_TELETEXSTRING, SC_ASN1_OPTIONAL, NULL, NULL },
  { "tokenflags",	    SC_ASN1_BIT_FIELD,   SC_ASN1_TAG_BIT_STRING, 0, NULL, NULL },
  { "seInfo",	    SC_ASN1_SEQUENCE,	  SC_ASN1_CONS | SC_ASN1_TAG_SEQUENCE, SC_ASN1_OPTIONAL, NULL, NULL },
  { "recordInfo",	    SC_ASN1_STRUCT,       SC_ASN1_CONS | SC_ASN1_CTX | 1, SC_ASN1_OPTIONAL, NULL, NULL },
  { "supportedAlgorithms", SC_ASN1_STRUCT,  SC_ASN1_CONS | SC_ASN1_CTX | 2, SC_ASN1_OPTIONAL, NULL, NULL },
  { "issuerId",       SC_ASN1_UTF8STRING,   SC_ASN1_CTX | 3, SC_ASN1_OPTIONAL, NULL, NULL },
  { "holderId",       SC_ASN1_UTF8STRING,   SC_ASN1_CTX | 4, SC_ASN1_OPTIONAL, NULL, NULL },
  { "lastUpdate",     SC_ASN1_GENERALIZEDTIME, SC_ASN1_CTX | 5, SC_ASN1_OPTIONAL, NULL, NULL },
  { "preferredLanguage", SC_ASN1_PRINTABLESTRING, SC_ASN1_TAG_PRINTABLESTRING, SC_ASN1_OPTIONAL, NULL, NULL }, 
  { NULL, 0, 0, 0, NULL, NULL }
};

const struct sc_asn1_entry c_asn1_tokeninfo[] = {
  { "TokenInfo", SC_ASN1_STRUCT, SC_ASN1_CONS | SC_ASN1_TAG_SEQUENCE, 0, NULL, NULL },
  { NULL, 0, 0, 0, NULL, NULL }
};

static int parse_ceres_tokeninfo(struct sc_pkcs15_card *p15card, const u8 * buf, size_t buflen)
{
  int r = SC_SUCCESS, bug=1;
  u8 serial[128];
  size_t i;
  size_t serial_len = sizeof(serial);
  u8 mnfid[SC_PKCS15_MAX_LABEL_SIZE];
  size_t mnfid_len = sizeof(mnfid);
  u8 label[SC_PKCS15_MAX_LABEL_SIZE];
  size_t label_len = sizeof(label);
  u8 last_update[32];
  size_t lupdate_len = sizeof(last_update) - 1;
  size_t flags_len = sizeof(p15card->flags);
  struct sc_asn1_entry asn1_toki[13], asn1_toki_ceres[13], asn1_tokeninfo_ceres[3];
  u8 preferred_language[3];
  size_t lang_length = sizeof(preferred_language);
  
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function parse_ceres_tokeninfo\n");
  memset(last_update, 0, sizeof(last_update));
  
  sc_copy_asn1_entry(c_asn1_toki_ceres, asn1_toki_ceres);
  sc_copy_asn1_entry(c_asn1_tokeninfo, asn1_tokeninfo_ceres);  
  sc_format_asn1_entry(asn1_toki_ceres + 0, &p15card->version, NULL, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 1, serial, &serial_len, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 2, mnfid, &mnfid_len, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 3, label, &label_len, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 4, &p15card->flags, &flags_len, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 5, NULL, NULL, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 6, NULL, NULL, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 7, NULL, NULL, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 8, NULL, NULL, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 9, NULL, NULL, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 10, last_update, &lupdate_len, 0);
  sc_format_asn1_entry(asn1_toki_ceres + 11, preferred_language, &lang_length, 0);
  sc_format_asn1_entry(asn1_tokeninfo_ceres, asn1_toki_ceres, NULL, 0);
    
  r = sc_asn1_ceres_decode(p15card->card->ctx, asn1_tokeninfo_ceres, buf, buflen, NULL, NULL);
  if (r) {
    sc_error(p15card->card->ctx,
	     "ASN.1 parsing of EF(TokenInfo) failed: %s\n",
	     sc_strerror(r));
    goto err;
  }

  p15card->version += 1;
  p15card->serial_number = (char *) malloc(serial_len * 2 + 1);
  if (!p15card->serial_number) {
    sc_error(p15card->card->ctx, "Memory allocation failed\n");
    goto err;
  }
  p15card->serial_number[0] = 0;
  for (i = 0; i < serial_len; i++) {
    char byte[3];

    sprintf(byte, "%02X", serial[i]);
    strcat(p15card->serial_number, byte);
  }
  if (p15card->manufacturer_id == NULL) {
    if (!bug) {
      if (asn1_toki[2].flags & SC_ASN1_PRESENT)
	p15card->manufacturer_id = strdup((char *) mnfid);
      else
	p15card->manufacturer_id = strdup("(unknown)");
    } else {
      if (asn1_toki_ceres[2].flags & SC_ASN1_PRESENT)
	p15card->manufacturer_id = strdup((char *) mnfid);
      else
	p15card->manufacturer_id = strdup("(unknown)");
    }
  }
  if (p15card->label == NULL) {
    if (!bug) {
      if (asn1_toki[3].flags & SC_ASN1_PRESENT)
	p15card->label = strdup((char *) label);
      else
	p15card->label = strdup("(unknown)");
    } else {
      if (asn1_toki_ceres[3].flags & SC_ASN1_PRESENT)
	p15card->label = strdup((char *) label);
      else
	p15card->label = strdup("(unknown)");
    }
  }
  if (!bug) {
    if (asn1_toki[10].flags & SC_ASN1_PRESENT)
      p15card->last_update = strdup((char *)last_update);
    if (asn1_toki[11].flags & SC_ASN1_PRESENT) {
      preferred_language[2] = 0;
      p15card->preferred_language = strdup((char *)preferred_language);
    }
  } else {
    if (asn1_toki_ceres[10].flags & SC_ASN1_PRESENT)
      p15card->last_update = strdup((char *)last_update);
    if (asn1_toki_ceres[11].flags & SC_ASN1_PRESENT) {
      preferred_language[2] = 0;
      p15card->preferred_language = strdup((char *)preferred_language);
    }
  }

  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function parse_ceres_tokeninfo\n");
  return r;
 err:
  if (p15card->serial_number == NULL)
    p15card->serial_number = strdup("(unknown)");
  if (p15card->manufacturer_id == NULL)
    p15card->manufacturer_id = strdup("(unknown)");
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function parse_ceres_tokeninfo\n");
  return r;
}


static const struct sc_asn1_entry c_asn1_odf[] = {
	{ "privateKeys",	 SC_ASN1_STRUCT, SC_ASN1_CTX | 0 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "publicKeys",		 SC_ASN1_STRUCT, SC_ASN1_CTX | 1 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "trustedPublicKeys",	 SC_ASN1_STRUCT, SC_ASN1_CTX | 2 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "certificates",	 SC_ASN1_STRUCT, SC_ASN1_CTX | 4 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "trustedCertificates", SC_ASN1_STRUCT, SC_ASN1_CTX | 5 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "usefulCertificates",  SC_ASN1_STRUCT, SC_ASN1_CTX | 6 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "dataObjects",	 SC_ASN1_STRUCT, SC_ASN1_CTX | 7 | SC_ASN1_CONS, 0, NULL, NULL },
	{ "authObjects",	 SC_ASN1_STRUCT, SC_ASN1_CTX | 8 | SC_ASN1_CONS, 0, NULL, NULL },
	{ NULL, 0, 0, 0, NULL, NULL }
};

static const unsigned int odf_indexes[] = {
	SC_PKCS15_PRKDF,
	SC_PKCS15_PUKDF,
	SC_PKCS15_PUKDF_TRUSTED,
	SC_PKCS15_CDF,
	SC_PKCS15_CDF_TRUSTED,
	SC_PKCS15_CDF_USEFUL,
	SC_PKCS15_DODF,
	SC_PKCS15_AODF,
};

static int parse_ceres_odf(struct sc_pkcs15_card *p15card, const u8 * buf, size_t buflen)
{
  const u8 *p = buf;
  u8 pukdf[8]= {0xA1,0x06,0x30,0x04,0x04,0x02,0x60,0x02};
  const u8 *k=pukdf;
  size_t pukdf_len = 8;
  size_t left = buflen;
  int r = SC_SUCCESS, i;
  sc_path_t path;
  sc_pkcs15_df_t *df=NULL;
  struct sc_asn1_entry asn1_obj_or_path[] = {
    { "path", SC_ASN1_PATH, SC_ASN1_CONS | SC_ASN1_SEQUENCE, 0, &path, NULL },
    { NULL, 0, 0, 0, NULL, NULL }
  };
  struct sc_asn1_entry asn1_odf[9];
	
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function parse_ceres_odf\n");

  sc_copy_asn1_entry(c_asn1_odf, asn1_odf);
  for (i = 0; asn1_odf[i].name != NULL; i++)
    sc_format_asn1_entry(asn1_odf + i, asn1_obj_or_path, NULL, 0);
  while (left > 0) {
    r = sc_asn1_ceres_decode_choice(p15card->card->ctx, asn1_odf, p, left, &p, &left);
    if (r == SC_ERROR_ASN1_END_OF_CONTENTS) {
      r = SC_SUCCESS;
      break;
    }
    if (r < 0)
      break;
    r = sc_pkcs15_add_df(p15card, odf_indexes[r], &path, NULL);
    if (r)
      break;
  }

  /* Adds Public Key DF if not exist on ODF */
  for (df=p15card->df_list; df!=NULL && df->type!=SC_PKCS15_PUKDF; df=df->next);
      
  if (df==NULL) {
    while (pukdf_len > 0) {
      r = sc_asn1_ceres_decode_choice(p15card->card->ctx, asn1_odf, k, pukdf_len, &k, &pukdf_len);
      if (r == SC_ERROR_ASN1_END_OF_CONTENTS) {
	r = SC_SUCCESS;
	break;
      }
      if (r < 0)
	break;
      r = sc_pkcs15_add_df(p15card, odf_indexes[r], &path, NULL);
      if (r)
	break;
    }
  }

  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function parse_ceres_odf\n");
  return r;
}


int ceres_pkcs15_encode_unusedspace(sc_context_t *ctx, struct sc_pkcs15_card *p15card, u8 ** buf, size_t *buflen)
{
  return encode_ceres_unusedspace(ctx, p15card, buf, buflen);
}


int encode_ceres_unusedspace(sc_context_t *ctx,
                         struct sc_pkcs15_card *p15card,
                         u8 **buf, size_t *buflen)
{
        static const struct sc_asn1_entry c_asn1_unusedspace[] = {
                { "UnusedSpace", SC_ASN1_STRUCT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        static const struct sc_asn1_entry c_asn1_unusedspace_values[] = {
                { "path", SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
                { "authId", SC_ASN1_PKCS15_ID, SC_ASN1_TAG_OCTET_STRING, SC_ASN1_OPTIONAL, NULL, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry *asn1_unusedspace = NULL;
        struct sc_asn1_entry *asn1_values = NULL;
        int unusedspace_count = 0, r, c = 0;
        sc_pkcs15_unusedspace_t *unusedspace;
        u8 card_path[11] = { 0x5C, 0x36, 0x30, 0x36, 0x31, 0x5C, 0x37, 0x30, 0x30, 0x30, 0x00 };
        int len = 0x00;
	int i = 0x00; 

        unusedspace = p15card->unusedspace_list;
        for ( ; unusedspace != NULL; unusedspace = unusedspace->next)
                unusedspace_count++;

        asn1_unusedspace = (struct sc_asn1_entry *)
                malloc(sizeof(struct sc_asn1_entry) * (unusedspace_count + 1));
        if (asn1_unusedspace == NULL) {
                r = SC_ERROR_OUT_OF_MEMORY;
                goto err;
        }
        asn1_values = (struct sc_asn1_entry *)
                malloc(sizeof(struct sc_asn1_entry) * (unusedspace_count * 3));
        if (asn1_values == NULL) {
                r = SC_ERROR_OUT_OF_MEMORY;
                goto err;
        }

        for (unusedspace = p15card->unusedspace_list; unusedspace != NULL; unusedspace = unusedspace->next) {
 		i = 0x00; 
 		len = 0x00; 
		card_path[8] = 0x30;
		card_path[9] = 0x30;

                sc_copy_asn1_entry(c_asn1_unusedspace, asn1_unusedspace + c);
                sc_format_asn1_entry(asn1_unusedspace + c, asn1_values + 3*c, NULL, 1);
                sc_copy_asn1_entry(c_asn1_unusedspace_values, asn1_values + 3*c);

		len = unusedspace->path.value[unusedspace->path.len-1];

		while (len > 0x0F) {
			i++;
			len -= 0x10;
		}

		if (i > 0x09)
			i += 0x07;
		if (len > 0x09)
			len += 0x07;

		card_path[8] += i;
                card_path[9] += len;
                memcpy (unusedspace->path.value, card_path, sizeof(card_path));
		unusedspace->path.len = 11;

                sc_format_asn1_entry(asn1_values + 3*c, &unusedspace->path, NULL, 1);

                sc_format_asn1_entry(asn1_values + 3*c+1, &unusedspace->auth_id, NULL, unusedspace->auth_id.len);
                c++;
        }
        asn1_unusedspace[c].name = NULL;

        r = sc_asn1_encode(ctx, asn1_unusedspace, buf, buflen);

err:
        if (asn1_values != NULL)
                free(asn1_values);
        if (asn1_unusedspace != NULL)
                free(asn1_unusedspace);

        return r;

}



int parse_ceres_unusedspace( sc_pkcs15_card_t *p15card, const u8 * buf, size_t buflen )
{	
  return ceres_pkcs15_parse_unusedspace( buf, buflen, p15card );
}


int ceres_pkcs15_parse_unusedspace(const u8 * buf, size_t buflen, struct sc_pkcs15_card *card)
{
        const u8 *p = buf;
        char standard_path[] = "3F0060617000"; 
        size_t left = buflen;
        int r;
        sc_path_t path;
        sc_pkcs15_id_t auth_id;
	int count = -1;
        struct sc_asn1_entry asn1_unusedspace[] = {
                { "UnusedSpace", SC_ASN1_STRUCT, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };
        struct sc_asn1_entry asn1_unusedspace_values[] = {
                { "path", SC_ASN1_PATH, SC_ASN1_TAG_SEQUENCE | SC_ASN1_CONS, 0, NULL, NULL },
                { "authId", SC_ASN1_PKCS15_ID, SC_ASN1_TAG_OCTET_STRING, SC_ASN1_OPTIONAL, NULL, NULL },
                { NULL, 0, 0, 0, NULL, NULL }
        };

        if (card->card->ctx->debug) sc_debug(card->card->ctx, "Entering function parse_ceres_unusedspace\n");
  
        /* Clean the list if already present */
        while (card->unusedspace_list)
                sc_pkcs15_remove_unusedspace(card, card->unusedspace_list);

        sc_format_asn1_entry(asn1_unusedspace, asn1_unusedspace_values, NULL, 1);
        sc_format_asn1_entry(asn1_unusedspace_values, &path, NULL, 1);
        sc_format_asn1_entry(asn1_unusedspace_values+1, &auth_id, NULL, 0);

        while (left > 0) {
                memset(&auth_id, 0, sizeof(auth_id));
                r = sc_asn1_decode(card->card->ctx, asn1_unusedspace, p, left, &p, &left);
                if (r == SC_ERROR_ASN1_END_OF_CONTENTS)
                        break;
                if (r < 0)
                        return r;
                 /* If the path length isn't include (-1) then it's against the standard
                 *   but we'll just ignore it instead of returning an error. */

                /* convert the path from ASCII to Hex */

                standard_path[11] = path.value[path.len-2];
		count = path.count;
                sc_format_path (standard_path, &path);
		path.count = count;
                if (path.count > 0) {
  			if (card->card->ctx->debug) sc_debug(card->card->ctx, "Adding new path\n");
                        r = sc_pkcs15_add_unusedspace(card, &path, &auth_id);
                        if (r)
                                return r;
                }
        }

        card->unusedspace_read = 1;

        if (card->card->ctx->debug) sc_debug(card->card->ctx, "Leaving function parse_ceres_tokeninfo\n");
        return 0;
}

int sc_pkcs15_get_card_objects(struct sc_pkcs15_card *p15card, unsigned int type,
			       struct sc_pkcs15_object **ret, size_t ret_size)
{
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_pkcs15_get_card_objects\n");
  return sc_pkcs15_get_card_objects_cond(p15card, type, NULL, NULL, ret, ret_size);
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_pkcs15_get_card_objects\n");
}

int sc_pkcs15_get_card_objects_cond(struct sc_pkcs15_card *p15card, unsigned int type,
                               int (* func)(struct sc_pkcs15_object *, void *),
                               void *func_arg,
                               struct sc_pkcs15_object **ret, size_t ret_size)
{
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_pkcs15_get_card_objects_cond\n");
  return __sc_pkcs15_search_card_objects(p15card, 0, type,
					 func, func_arg, ret, ret_size);
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_pkcs15_get_card_objects_cond\n");
}

int __sc_pkcs15_search_card_objects(sc_pkcs15_card_t *p15card,
			       unsigned int class_mask, unsigned int type,
			       int (*func)(sc_pkcs15_object_t *, void *),
			       void *func_arg,
			       sc_pkcs15_object_t **ret, size_t ret_size)
{
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function __sc_pkcs15_search_card_objects\n");
  sc_pkcs15_object_t *obj;
        sc_pkcs15_df_t  *df;
        unsigned int    df_mask = 0;
        size_t          match_count = 0;
        int             r = 0;

        if (type)
                class_mask |= SC_PKCS15_TYPE_TO_CLASS(type);

        /* Make sure the class mask we have makes sense */
        if (class_mask == 0
         || (class_mask & ~(SC_PKCS15_SEARCH_CLASS_PRKEY |
                            SC_PKCS15_SEARCH_CLASS_PUBKEY |
                            SC_PKCS15_SEARCH_CLASS_CERT |
                            SC_PKCS15_SEARCH_CLASS_DATA |
                            SC_PKCS15_SEARCH_CLASS_AUTH))) {
                return SC_ERROR_INVALID_ARGUMENTS;
        }

        if (class_mask & SC_PKCS15_SEARCH_CLASS_PRKEY)
                df_mask |= (1 << SC_PKCS15_PRKDF);
        if (class_mask & SC_PKCS15_SEARCH_CLASS_PUBKEY)
                df_mask |= (1 << SC_PKCS15_PUKDF)
                         | (1 << SC_PKCS15_PUKDF_TRUSTED);
        if (class_mask & SC_PKCS15_SEARCH_CLASS_CERT)
                df_mask |= (1 << SC_PKCS15_CDF)
                         | (1 << SC_PKCS15_CDF_TRUSTED)
                         | (1 << SC_PKCS15_CDF_USEFUL);
        if (class_mask & SC_PKCS15_SEARCH_CLASS_DATA)
                df_mask |= (1 << SC_PKCS15_DODF);
        if (class_mask & SC_PKCS15_SEARCH_CLASS_AUTH)
                df_mask |= (1 << SC_PKCS15_AODF);

        /* Make sure all the DFs we want to search have been
         * enumerated. */
        for (df = p15card->df_list; df != NULL; df = df->next) {
                if (!(df_mask & (1 << df->type)))
                        continue;
                if (df->enumerated)
                        continue;
                /* Enumerate the DF's, so p15card->obj_list is
                 * populated. */
               SC_TEST_RET(p15card->card->ctx, r, "DF parsing failed");
                df->enumerated = 1;
        }

        /* And now loop over all objects */
        for (obj = p15card->obj_list; obj != NULL; obj = obj->next) {
                /* Check object type */
                if (!(class_mask & SC_PKCS15_TYPE_TO_CLASS(obj->type)))
                        continue;
                if (type != 0
                 && obj->type != type
                 && (obj->type & SC_PKCS15_TYPE_CLASS_MASK) != type)
                        continue;

                /* Potential candidate, apply search function */
                if (func != NULL && func(obj, func_arg) <= 0)
                        continue;
                /* Okay, we have a match. */
                match_count++;
                if (ret_size <= 0)
                        continue;
                ret[match_count-1] = obj;
                if (ret_size <= match_count)
                        break;
        }
	if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function __sc_pkcs15_search_card_objects\n");
        return match_count;
}

int sc_pkcs15_parse_ceres_df(struct sc_pkcs15_card *p15card,
			    const unsigned int df_type,
			    const u8 *buf,
			    const size_t in_bufsize)
{
  sc_context_t            *ctx     = p15card->card->ctx;
  int                      r       = SC_SUCCESS;
  size_t                   bufsize = in_bufsize;
  struct sc_pkcs15_object *obj     = NULL;

  int (* func)(struct sc_pkcs15_card *, struct sc_pkcs15_object *,
	       const u8 **nbuf, size_t *nbufsize) = NULL;
  int (* func2)(struct sc_pkcs15_card *p15card, const u8 * buf, size_t buflen) = NULL;

  if ( ctx->debug ) sc_debug( ctx, "Entering function sc_pkcs15_parse_ceres_df\n" );
 
  switch (df_type) {
  case SC_PKCS15_PRKDF:
    func = sc_pkcs15_ceres_decode_prkdf_entry;
    break;
  case SC_PKCS15_PUKDF:
    func = sc_pkcs15_ceres_decode_pukdf_entry;
    break;
  case SC_PKCS15_CDF:
  case SC_PKCS15_CDF_TRUSTED:
  case SC_PKCS15_CDF_USEFUL:
    func = sc_pkcs15_ceres_decode_cdf_entry;
    break;
  case SC_PKCS15_DODF:
    func = sc_pkcs15_ceres_decode_dodf_entry;
    break;
  case SC_PKCS15_AODF:
    func = sc_pkcs15_ceres_decode_aodf_entry;
    break;
  }
  if (func == NULL) {
    switch (df_type) {
    case SC_PKCS15_ODF:
      func2 = parse_ceres_odf;
      break;
    case SC_PKCS15_TOKENINFO:
      func2 = parse_ceres_tokeninfo;
      break;
    case SC_PKCS15_UNUSED:
      func2 = parse_ceres_unusedspace;
      break;
    }
    if (func2 == NULL) {
      sc_error(ctx, "unknown DF type: %d\n", df_type);
      r = SC_ERROR_INVALID_ARGUMENTS;
      goto ret;
    }
    r = func2(p15card, buf, bufsize);
    if (r!=SC_SUCCESS) {
      sc_perror(ctx, r, "Error decoding DF entry");
    }
    goto ret;
  }

  do {
    const u8 *oldp;
    size_t obj_len;

    obj = (struct sc_pkcs15_object *) calloc(1, sizeof(struct sc_pkcs15_object));
    if (obj == NULL) {
      r = SC_ERROR_OUT_OF_MEMORY;
      goto ret;
    }
    oldp = buf;
		
    r = func(p15card, obj, &buf, &bufsize);
    if (r) {
      free(obj);
      if (r == SC_ERROR_ASN1_END_OF_CONTENTS) {
	r = 0;
	break;
      }
      sc_perror(ctx, r, "Error decoding DF entry");
      goto ret;
    }

    obj_len = buf - oldp;

    obj->der.value = (u8 *) malloc(obj_len);
    if (obj->der.value == NULL) {
      r = SC_ERROR_OUT_OF_MEMORY;
      goto ret;
    }
    memcpy(obj->der.value, oldp, obj_len);
    obj->der.len = obj_len;

    /* These objects are independent one another*/
    obj->df = NULL;
    r = sc_pkcs15_add_object(p15card, obj);
    if (r) {
      if (obj->data)
	free(obj->data);
      free(obj);
      sc_perror(ctx, r, "Error adding object");
      goto ret;
    }
  } while (bufsize && *buf != 0x00);

 ret:
  if (ctx->debug) sc_debug(ctx, "Leaving function sc_pkcs15_parse_ceres_df\n");
  return r;
}

int sc_pkcs15_card_encode_df(sc_context_t *ctx,
                             struct sc_pkcs15_card *p15card,
                             struct sc_pkcs15_df *df,
                             u8 **buf_out, size_t *bufsize_out)
{
  u8 *buf = NULL, *tmp = NULL;
  size_t bufsize = 0, tmpsize;
  const struct sc_pkcs15_object *obj;
  int (* func)(sc_context_t *, const struct sc_pkcs15_object *nobj,
               u8 **nbuf, size_t *nbufsize) = NULL;
  int r = SC_SUCCESS;

  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_pkcs15_card_encode_df\n");

  assert(p15card != NULL && p15card->magic == SC_PKCS15_CARD_MAGIC);
  switch (df->type) {
  case SC_PKCS15_PRKDF:
    func = sc_pkcs15_ceres_encode_prkdf_entry;
    break;
  case SC_PKCS15_PUKDF:
  case SC_PKCS15_PUKDF_TRUSTED:
    func = sc_pkcs15_ceres_encode_pukdf_entry;
    break;
  case SC_PKCS15_CDF:
  case SC_PKCS15_CDF_TRUSTED:
  case SC_PKCS15_CDF_USEFUL:
    func = sc_pkcs15_ceres_encode_cdf_entry;
    break;
  case SC_PKCS15_DODF:
    func = sc_pkcs15_ceres_encode_dodf_entry;
    break;
  case SC_PKCS15_AODF:
    func = sc_pkcs15_encode_aodf_entry;
    break;
  case SC_PKCS15_UNUSED:
    r = ceres_pkcs15_encode_unusedspace(ctx, p15card, buf_out, bufsize_out);
    goto end;
    break;
  }

  if (func == NULL) {
    sc_error(ctx, "unknown DF type: %d\n", df->type);
    *buf_out = NULL;
    *bufsize_out = 0;
    return 0;
  }
  for (obj = p15card->obj_list; obj != NULL; obj = obj->next) {
    if (obj->df != df)
      continue;
    /* if we have a asn.1 codification, just use it! */
    if(obj->der.len > 0 && df->type != SC_PKCS15_DODF) {
      if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Reusing existing DER encoding\n");
      
      tmp = calloc(1, sizeof(u8)*obj->der.len);
      if(!tmp) {
	r = SC_ERROR_OUT_OF_MEMORY;
	goto end;
      }
      memcpy(tmp, obj->der.value, obj->der.len);
      tmpsize = obj->der.len;
    } else {
      r = func(ctx, obj, &tmp, &tmpsize);
      if (r) {
	goto end;
      }
    }
    buf = (u8 *) realloc(buf, bufsize + tmpsize);
    memcpy(buf + bufsize, tmp, tmpsize);
    free(tmp);
    tmp = NULL;
    bufsize += tmpsize;
  }
  *buf_out = buf;
  buf = NULL;
  *bufsize_out = bufsize;
    
end:
  if(tmp) {
    free(tmp);
    tmp = NULL;
  }

  if(buf) {
    free(buf);
    buf = NULL;
  }

  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_pkcs15_card_encode_df\n");

  return r;
}

int sc_get_unusedspace( sc_pkcs15_card_t *card )
{
  int r = SC_SUCCESS;
  u8 buf[10000];
  size_t buflen=sizeof(buf);
  sc_file_t *unusedspace_file=NULL;
  sc_path_t unusedspace_path;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  assert(card!=NULL && card->card!=NULL);
  memset(buf, 0, sizeof(buf));
  
  SC_FUNC_CALLED(card->card->ctx, 1);

  /* we backup use_virtual_fs */
  old_use_virtual_fs = ceres_is_virtual_fs_active(card->card);

  /* we want to use card without virtual fs */
  ceres_set_virtual_fs_state(card->card, 0);

  sc_format_path("3F0050155033", &unusedspace_path);
  unusedspace_file = sc_file_new();
  if(!unusedspace_file) {
    r = SC_ERROR_NOT_ENOUGH_MEMORY;
    goto end;
  }
  r = sc_select_file(card->card, &unusedspace_path, &unusedspace_file);
  if (r!=SC_SUCCESS)
    goto end;
  if(!unusedspace_file) {
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }
  if(unusedspace_file->size > buflen) {
    r = SC_ERROR_BUFFER_TOO_SMALL;
    goto end;
  }
  buflen = unusedspace_file->size;

  r = sc_read_binary(card->card, 0, buf, buflen, 0);
  if(r<=0) {
    r = SC_ERROR_INVALID_DATA;
    goto end;
  }

  r = parse_ceres_unusedspace(card, buf, buflen); 

  if (r!=SC_SUCCESS)
    goto end;

  end:
  /* we restore use_virtual_fs */
  ceres_set_virtual_fs_state(card->card, old_use_virtual_fs);
  if(unusedspace_file)
    sc_file_free(unusedspace_file); 
  SC_FUNC_CALLED(card->card->ctx, r);

  return r;
}

int sc_find_free_unusedspace( sc_pkcs15_card_t *p15card, const size_t size, 
			      sc_pkcs15_unusedspace_t **out_unusedspace )
{
  int found=0, r = SC_SUCCESS;
  sc_path_t us_df;
  sc_pkcs15_unusedspace_t *temp_us=NULL;

  assert(p15card != NULL && out_unusedspace!=NULL);

  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Entering function sc_find_free_unusedspace\n");
  
  if (out_unusedspace && *out_unusedspace) {
    free(out_unusedspace);
    out_unusedspace=NULL;
  }

  sc_format_path("3F0050155033", &us_df);
  /* This select file executes a parse unused space structure */
  r = sc_select_file( p15card->card, &us_df, NULL);
  if (r!=SC_SUCCESS)
    goto sffu_err;
  
  for(temp_us=p15card->unusedspace_list; temp_us!=NULL; temp_us=temp_us->next) {
    if(size <= temp_us->path.count) {
      if (found && temp_us->path.count >= (*out_unusedspace)->path.count) {
	/* file is suitable but previously found was best */
	continue;
      }
      /* we found it */
      *out_unusedspace = temp_us;
      found=1;
    }
  }

 sffu_err:
  if (p15card->card->ctx->debug) sc_debug(p15card->card->ctx, "Leaving function sc_find_free_unusedspace\n");  
  return r;
}

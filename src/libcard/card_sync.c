/*!
 * \file card_sync.c
 * \brief Card synchronization functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda 
 *
 */

#include "card_sync.h"
#include <opensc/opensc.h>
#include <opensc/pkcs15.h>
#include "base_card.h"
#include "pkcs15_standard.h"
#include "pkcs15_default.h"
#include "card_helper.h"
#include "file_compression.h"
#include <string.h> /*!< to call memory functions */



int ceres_sync_card_to_virtual_fs_filter_data_object( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_data_info *data_object = NULL;
  unsigned char *card_data = NULL;
  virtual_file_t  *data_object_virtual_file = NULL;
  virtual_file_t  *data_object_virtual_file_weak_link = NULL; 

  SC_FUNC_CALLED(card->ctx, 1);

  /* we need to correct data_object length in path */
  data_object = (struct sc_pkcs15_data_info *) obj->data;
  if(data_object) {
    if(data_object->path.len > 0) {
      data_object_virtual_file = virtual_file_new();
      if(!data_object_virtual_file) {
	r = SC_ERROR_OUT_OF_MEMORY;
	goto end;
      }

      memcpy(&data_object_virtual_file->path, &data_object->path, sizeof(data_object_virtual_file->path));

      data_object_virtual_file->is_ef = 1;
      data_object_virtual_file->card_to_virtual_fs.sync_state = virtual_file_sync_state_sync_pending;
      data_object_virtual_file->card_to_virtual_fs.sync_callback = ceres_sync_card_to_virtual_fs_data_object_file_callback;
      data_object_virtual_file->virtual_fs_to_card.sync_state = virtual_file_sync_state_unknown;
      data_object_virtual_file->virtual_fs_to_card.sync_callback = NULL;
      
      /* append file to virtual_fs */
      r = virtual_fs_append(virtual_fs, data_object_virtual_file);
      if(r != SC_SUCCESS)
	goto end;

      /* we don't have ownership of virtual_file now,
       * so we don't need to free it */
      data_object_virtual_file_weak_link = data_object_virtual_file;
      data_object_virtual_file = NULL;
  
      
      /* we now synchronize file because this gets a correct size for it */
      r = virtual_file_data_synchronize(data_object_virtual_file_weak_link, card, virtual_file_sync_type_card_to_virtual_fs, DRVDATA(card)->virtual_fs);
      if (r != SC_SUCCESS) {
	sc_error(card->ctx, "Synchronization failed\n");
	goto end;
      }
      
      /* correct length in PKCS#15 */
      data_object->path.count = data_object_virtual_file_weak_link->data_size;
    } else {
      sc_debug(card->ctx, "Path length is 0");
    }
  } else {
    sc_debug(card->ctx, "Pointer to data object info was empty");
  }

 end:
  if(card_data) {
    free(card_data);
    card_data = NULL;
  }

  if(data_object_virtual_file) {
    virtual_file_free(data_object_virtual_file);
    data_object_virtual_file = NULL;
  }

  SC_FUNC_RETURN(card->ctx, 1, r);
}



int ceres_sync_card_to_virtual_fs_filter_cert( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_cert_info *cert = NULL;
  unsigned char *card_data = NULL;
  virtual_file_t  *certificate_virtual_file = NULL;
  virtual_file_t  *certificate_virtual_file_weak_link = NULL; 
  sc_path_t abs_cert_path, cert_card_path;

  SC_FUNC_CALLED(card->ctx, 1);

  /* we need to correct certificate length in path */
  cert = (struct sc_pkcs15_cert_info *) obj->data;
  if(cert) {
    /* set asn1 to map */
    r = map_id_to_der_set_item(DRVDATA(card)->cdf_card_ckaid_to_card_der_map, &cert->id, &obj->der);
    if(r != SC_SUCCESS)
      goto end;
    
    if(cert->path.len > 0) {
      certificate_virtual_file = virtual_file_new();
      if(!certificate_virtual_file) {
	r = SC_ERROR_OUT_OF_MEMORY;
	goto end;
      }

      memset(&abs_cert_path, 0, sizeof(struct sc_path));
      memset(&cert_card_path, 0, sizeof(struct sc_path));
      if(cert->path.len==2) {
	sc_format_path("3F005015", &abs_cert_path);
	r = sc_append_path(&abs_cert_path, &cert->path);
	if(r!=SC_SUCCESS)
	  goto end;
	sc_format_path("3F006061", &cert_card_path);
	r = sc_append_path(&cert_card_path, &cert->path);
	if(r!=SC_SUCCESS)
	  goto end;
      } else {
	r = sc_append_path(&abs_cert_path, &cert->path);
	if(r!=SC_SUCCESS)
	  goto end;
	sc_format_path("3F006061", &cert_card_path);
	cert_card_path.value[4]=cert->path.value[4];
	cert_card_path.value[5]=cert->path.value[5];
	cert_card_path.len=6;
      }

      memcpy(&certificate_virtual_file->path, &abs_cert_path, sizeof(certificate_virtual_file->path));
      r = map_path_to_path_set_item(DRVDATA(card)->virtual_fs_to_card_path_map, &certificate_virtual_file->path, &cert_card_path);
      if(r != SC_SUCCESS)
	goto end;
      certificate_virtual_file->is_ef = 1;
      certificate_virtual_file->card_to_virtual_fs.sync_state = virtual_file_sync_state_sync_pending;
      certificate_virtual_file->card_to_virtual_fs.sync_callback = ceres_sync_card_to_virtual_fs_certificate_file_callback;
      certificate_virtual_file->virtual_fs_to_card.sync_state = virtual_file_sync_state_unknown;
      certificate_virtual_file->virtual_fs_to_card.sync_callback = NULL;
      
      /* append file to virtual_fs */
      r = virtual_fs_append(virtual_fs, certificate_virtual_file);
      if(r != SC_SUCCESS)
	goto end;

      /* we don't have ownership of virtual_file now,
	 so we don't need to free it */
      certificate_virtual_file_weak_link = certificate_virtual_file;
      certificate_virtual_file = NULL;
  
      
      /* we now synchronize file because this gets a correct size for it */
      r = virtual_file_data_synchronize(certificate_virtual_file_weak_link, card, virtual_file_sync_type_card_to_virtual_fs, DRVDATA(card)->virtual_fs);
      if (r != SC_SUCCESS) {
	sc_error(card->ctx, "Synchronization failed\n");
	goto end;
      }
      
      /* correct length in PKCS#15 */
      cert->path.count = certificate_virtual_file_weak_link->data_size;
    } else {
      sc_debug(card->ctx, "Path length is 0");
    }
  } else {
    sc_debug(card->ctx, "Pointer to cert info was empty");
  }

 end:
  if(card_data) {
    free(card_data);
    card_data = NULL;
  }

  if(certificate_virtual_file) {
    virtual_file_free(certificate_virtual_file);
    certificate_virtual_file = NULL;
  }

  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_card_to_virtual_fs_filter_prkey( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_prkey_info *prkey = NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  prkey = (struct sc_pkcs15_prkey_info *) obj->data;
  if(prkey) {
    /* set asn1 to map */
    r = map_id_to_der_set_item(DRVDATA(card)->prkdf_card_ckaid_to_card_der_map, &prkey->id, &obj->der);
    
    if(r != SC_SUCCESS)
      goto end;
    r = map_path_to_key_info_set_item(DRVDATA(card)->card_path_to_card_keyinfo_map,&prkey->path, SC_PKCS15_TYPE_PRKEY_RSA, prkey->key_reference);
    if(r != SC_SUCCESS)
	goto end;
    if(prkey->modulus_length < 512)
      prkey->modulus_length = prkey->modulus_length * 8;
    if(prkey->modulus_length != 2048)
      prkey->modulus_length = 1024;

    if(prkey->path.len > 0) {
      /* append empty file */
      r = virtual_fs_append_new_virtual_file(virtual_fs, &prkey->path, NULL, 1, 1, 1, virtual_file_sync_state_unknown, NULL, virtual_file_sync_state_unknown, NULL);
      if(r != SC_SUCCESS)
	goto end;

      /* correct length in PKCS#15 */
      prkey->path.count = 0;
    } else {
      sc_debug(card->ctx, "Path length is 0");
    }
  } else {
    sc_debug(card->ctx, "Pointer to prkey info was empty");
  }

 end:
  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_card_to_virtual_fs_filter_pukey( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_pubkey_info *pukey = NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  pukey = (struct sc_pkcs15_pubkey_info *) obj->data;
  if(pukey) {
    /* set asn1 to map */
    r = map_id_to_der_set_item(DRVDATA(card)->pukdf_card_ckaid_to_card_der_map, &pukey->id, &obj->der);
    if(r != SC_SUCCESS)
      goto end;
    
    r = map_path_to_key_info_set_item(DRVDATA(card)->card_path_to_card_keyinfo_map,&pukey->path, SC_PKCS15_TYPE_PUBKEY_RSA, pukey->key_reference);
    if(r != SC_SUCCESS)
	goto end;


    if(pukey->path.len > 0) {
      /* append empty file */
      r = virtual_fs_append_new_virtual_file(virtual_fs, 
					     &pukey->path, 
					     obj->der.value, 
					     obj->der.len, 
					     obj->der.len, 
					     1, 
					     virtual_file_sync_state_unknown, 
					     NULL, 
					     virtual_file_sync_state_unknown, 
					     NULL);
      if(r != SC_SUCCESS)
	goto end;

      /* correct length in PKCS#15 */
      pukey->path.count = 0;
    } else {
      sc_debug(card->ctx, "Path length is 0");
    }
  } else {
    sc_debug(card->ctx, "Pointer to pukey info was empty");
  }

 end:
  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_card_to_virtual_fs_any_df( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, int type )
{
  int r = SC_SUCCESS;
  unsigned char *encoded_pkcs15 = NULL;
  size_t encoded_pkcs15_size = 0;
  sc_pkcs15_card_t *temp_pkcs15_card  = NULL;
  sc_pkcs15_object_t *obj = NULL;
  unsigned char *card_data = NULL;
  size_t card_data_length = 0;  
  
  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  /* get file */
  r = ceres_helper_read_file(card, &virtual_file->path, &card_data, &card_data_length);
  if(r < 0)
    goto end;

  /* create a new pkcs15_card structure */
  temp_pkcs15_card = sc_pkcs15_card_new();
  if(!temp_pkcs15_card) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto end;
  }

  /* we set some important fields */
  temp_pkcs15_card->card = card;

  /* Convert card df read to a list of same type of pkcs15 objects. 
     This function uses our internal card decoding parser.
  */
  r = sc_pkcs15_parse_ceres_df( temp_pkcs15_card, 
			       type, 
			       card_data, 
			       card_data_length
                               );
  if(r != SC_SUCCESS) {
    sc_error(card->ctx, "Card parsing failed\n"); 
    goto end;
  }

  /* we need to correct some PKCS#15 data */
  for(obj = temp_pkcs15_card->obj_list; obj != NULL; obj = obj->next) {
    switch(obj->type & SC_PKCS15_TYPE_CLASS_MASK) {
    case SC_PKCS15_TYPE_CERT:
      r = ceres_sync_card_to_virtual_fs_filter_cert(card, virtual_file, virtual_fs, obj);
      break;

    case SC_PKCS15_TYPE_PRKEY:
      r = ceres_sync_card_to_virtual_fs_filter_prkey(card, virtual_file, virtual_fs, obj);
      break;
      
    case SC_PKCS15_TYPE_PUBKEY:
      r = ceres_sync_card_to_virtual_fs_filter_pukey(card, virtual_file, virtual_fs, obj);
      break;
      
    case SC_PKCS15_TYPE_AUTH:
      if(obj->data) {
	sc_pkcs15_pin_info_t * pin=obj->data;
	pin->stored_length= (pin->max_length>pin->stored_length) ? pin->max_length : pin->stored_length;
      }
      break;
    case SC_PKCS15_TYPE_DATA_OBJECT:
      r = ceres_sync_card_to_virtual_fs_filter_data_object(card, virtual_file, virtual_fs, obj);
      break;
    default:
      /* ignore this object */
      break;
    }
  }
  if(r != SC_SUCCESS) {
    sc_error(card->ctx, "Object filtering failed\n");
    goto end;
  }
 

  /* generate pkcs#15 stream for the appropiate object type */
  r = sc_standard_pkcs15_encode_any_df( card->ctx, 
					temp_pkcs15_card, 
					type, /* encode only specific objects type */
					&encoded_pkcs15,
					&encoded_pkcs15_size
                                        );
  if(r != SC_SUCCESS) {
    sc_error(card->ctx, "Standard PKCS#15 encoding failed\n"); 
    goto end;
  }
    
  r = virtual_file_data_update(virtual_file, 0, encoded_pkcs15, encoded_pkcs15_size);
  if(r == SC_SUCCESS) {
    /* add a trailing 0, just in case */
    r = virtual_file_data_update(virtual_file, encoded_pkcs15_size, (const unsigned char *)"\0", 1);
  }

 end:
  if(card_data) {
    free(card_data);
    card_data = NULL;
  }

  if(temp_pkcs15_card) { 
    /* set to NULL without freeing because we reused structure */
    temp_pkcs15_card->card = NULL;
    
    /* now free temp structure */
    sc_pkcs15_card_free(temp_pkcs15_card);
    temp_pkcs15_card = NULL;
  }  

  if(encoded_pkcs15) {
    free(encoded_pkcs15);
    encoded_pkcs15 = NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}


int ceres_sync_card_to_virtual_fs_usdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_UNUSED);
}

int ceres_sync_card_to_virtual_fs_odf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_ODF);
}

int ceres_sync_card_to_virtual_fs_tokeninfo_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_TOKENINFO);
}

int ceres_sync_card_to_virtual_fs_aodf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_AODF);
}

int ceres_sync_card_to_virtual_fs_prkdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with PrKDF param */
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_PRKDF);
}

int ceres_sync_card_to_virtual_fs_pukdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with PuKDF param */
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_PUKDF);
}

int ceres_sync_card_to_virtual_fs_cdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_CDF);
}

int ceres_sync_card_to_virtual_fs_dodf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  return ceres_sync_card_to_virtual_fs_any_df(card, virtual_file, virtual_fs, SC_PKCS15_DODF);
}

int ceres_sync_virtual_fs_to_card_filter_data_object( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_data_info *data_object = NULL;
  sc_path_t *path = NULL;
  unsigned char *compressed_data = NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  data_object = (struct sc_pkcs15_data_info *) obj->data;
  if(data_object) {

    data_object->path.value[2] = 0x50;
    data_object->path.value[3] = 0x15;

    path = map_path_to_path_find(DRVDATA(card)->virtual_fs_to_card_path_map, &data_object->path);
    if(path) {
       /* replace path data */
       memcpy(&data_object->path, path, sizeof(sc_path_t));
    }

  }
 
  if(compressed_data) {
    free(compressed_data);
    compressed_data = NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}


int ceres_sync_virtual_fs_to_card_filter_cert( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_cert_info *cert = NULL;
  sc_pkcs15_der_t *der = NULL;
  sc_path_t *path = NULL;
  sc_pkcs15_id_t *ckaid = NULL;
  struct _virtual_file_t *tmp_vf=NULL;
  unsigned char *compressed_data = NULL;
  size_t compressed_data_length = 0;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  cert = (struct sc_pkcs15_cert_info *) obj->data;
  if(cert) {
    sc_der_clear(&obj->der);

    /* try to find an old der if present */
    der = map_id_to_der_find(DRVDATA(card)->cdf_card_ckaid_to_card_der_map, &cert->id);
    if(der) {
      sc_der_copy(&obj->der, der);
    }

    path = map_path_to_path_find(DRVDATA(card)->virtual_fs_to_card_path_map, &cert->path);
    if(path) {
      /* replace path data */
      memcpy(&cert->path, path, sizeof(sc_path_t));
      
      tmp_vf=virtual_fs_find_by_path(virtual_fs, &cert->path);
      if(!tmp_vf) {
	r = SC_ERROR_INVALID_DATA;
	goto end;
      }
      
      r = file_compress_data(card, tmp_vf->data, tmp_vf->data_size, &compressed_data, &compressed_data_length); 
      if(r!=SC_SUCCESS)
	goto end;
      
      cert->path.count = compressed_data_length;
    }


    /* to add sc_pkcs15_cert to sc_pkcs15_cert_info */
    /* we need CKA_SUBJECT, CKA_ISSUER and CKA_SERIAL_NUMBER found in sc_pkcs15_cert */
    if (cert->value.value) {
      free(cert->value.value);
      cert->value.value = NULL;
    }
    cert->value.value = calloc(1, tmp_vf->data_size);
    memcpy(cert->value.value, tmp_vf->data, tmp_vf->data_size);
    cert->value.len = tmp_vf->data_size;

    /* NOTE: sc_pkcs15_cert has its public key, we can use it to create a sc_pkcs15_pubkey */

    ckaid = map_opensc_id_to_id_find(DRVDATA(card)->virtual_fs_to_card_ckaid_map, &cert->id);
    if(ckaid) {
      /* replace ckaid */
      memcpy(&cert->id, ckaid, sizeof(struct sc_pkcs15_id));
    } else {
      ckaid = map_path_to_id_find(DRVDATA(card)->card_path_to_card_ckaid_map, &cert->path);
      if (ckaid) {
	/* replace ckaid */
	memcpy(&cert->id, ckaid, sizeof(struct sc_pkcs15_id));
      }
    }
  }

 end:
  if(compressed_data) {
    free(compressed_data);
    compressed_data = NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_virtual_fs_to_card_filter_prkey( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_prkey_info *prkey = NULL;
  sc_pkcs15_der_t *der = NULL;
  sc_path_t *path = NULL;
  sc_pkcs15_id_t *ckaid = NULL;
  sc_file_t *keyfile = NULL;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;
  
  prkey = (struct sc_pkcs15_prkey_info *) obj->data;
  if(prkey) {
    sc_der_clear(&obj->der);

    /* try to find an old der if present */
    der = map_id_to_der_find(DRVDATA(card)->prkdf_card_ckaid_to_card_der_map, &prkey->id);
    if(der) {
      sc_der_copy(&obj->der, der);
    }

    path = map_path_to_path_find(DRVDATA(card)->virtual_fs_to_card_path_map, &prkey->path);
    if(path) {
      /* replace path data */
      memcpy(&prkey->path, path, sizeof(sc_path_t));
    }

    ckaid = map_opensc_id_to_id_find(DRVDATA(card)->virtual_fs_to_card_ckaid_map, &prkey->id);
    if(ckaid) {
      /* replace ckaid */
      memcpy(&prkey->id, ckaid, sizeof(struct sc_pkcs15_id));
    }

    /* add manual flags */
    prkey->native = 0x01;
    
    r = map_ckaid_to_keyinfo_find(DRVDATA(card)->card_ckaid_to_card_keyinfo_map, &prkey->id, &prkey->usage, &prkey->access_flags, NULL, NULL);



    /* Select private key file to knows the real size */

    /* we backup use_virtual_fs */
    old_use_virtual_fs = ceres_is_virtual_fs_active(card);

    /* we want to use card without virtual fs */
    ceres_set_virtual_fs_state(card, 0);

    r = sc_select_file(card, &prkey->path, &keyfile);

    /* we restore use_virtual_fs */
    ceres_set_virtual_fs_state(card, old_use_virtual_fs);

    if(r!=SC_SUCCESS)
      goto end;

    prkey->path.count = keyfile->size;

  } else {
    sc_debug(card->ctx, "Pointer to prkey info was empty");
  }


end:
   if(keyfile)
    sc_file_free(keyfile);


  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_virtual_fs_to_card_filter_pukey( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, sc_pkcs15_object_t *obj )
{
  int r = SC_SUCCESS;
  struct sc_pkcs15_pubkey_info *pukey = NULL;
  sc_pkcs15_der_t *der = NULL;
  sc_path_t *path = NULL;
  sc_pkcs15_id_t *ckaid = NULL;
  sc_file_t *keyfile = NULL;
  int old_use_virtual_fs; /*!< backup of use_virtual_fs */

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;
  
  pukey = (struct sc_pkcs15_pubkey_info *) obj->data;
  if(pukey) {
    sc_der_clear(&obj->der);

    /* try to find an old der if present */
    der = map_id_to_der_find(DRVDATA(card)->pukdf_card_ckaid_to_card_der_map, &pukey->id);
    if(der) {
      sc_der_copy(&obj->der, der);
    }

    path = map_path_to_path_find(DRVDATA(card)->virtual_fs_to_card_path_map, &pukey->path);
    if(path) {
      /* replace path data */
      memcpy(&pukey->path, path, sizeof(sc_path_t));
    }

    ckaid = map_opensc_id_to_id_find(DRVDATA(card)->virtual_fs_to_card_ckaid_map, &pukey->id);
    if(ckaid) {
      /* replace ckaid */
      memcpy(&pukey->id, ckaid, sizeof(struct sc_pkcs15_id));
    }
    /* add manual flags */
    pukey->native = 0x01;

    r = map_ckaid_to_keyinfo_find(DRVDATA(card)->card_ckaid_to_card_keyinfo_map, &pukey->id, NULL, NULL, &pukey->usage, &pukey->access_flags);
    if (r != SC_SUCCESS)
    {
      /* If didn't found a key_usage and access flags, we assign it hardcoded */
      pukey->usage  = SC_PKCS15_PRKEY_USAGE_ENCRYPT;
      pukey->usage |= SC_PKCS15_PRKEY_USAGE_WRAP;
      pukey->usage |= SC_PKCS15_PRKEY_USAGE_VERIFY;
  
      pukey->access_flags  = SC_PKCS15_PRKEY_ACCESS_EXTRACTABLE;
      r = SC_SUCCESS;
    }

    if (pukey->path.count < 1)
    {
	    /* If pukey path count haven't a valid value */
	    /* Select public key file to knows the real size */

	    /* we backup use_virtual_fs */
	    old_use_virtual_fs = ceres_is_virtual_fs_active(card);

	    /* we want to use card without virtual fs */
	    ceres_set_virtual_fs_state(card, 0);


	    r = sc_select_file(card, &pukey->path, &keyfile);

	    /* we restore use_virtual_fs */
	    ceres_set_virtual_fs_state(card, old_use_virtual_fs);

	    if(r!=SC_SUCCESS)
	      goto end;

	    pukey->path.count = keyfile->size;
    }

    if (card->type == SC_CARD_TYPE_CERES_ST)
      pukey->key_reference = pukey->path.value[pukey->path.len-1];
  } else {
    sc_debug(card->ctx, "Pointer to pukey info was empty");
  }


end:
  if(keyfile)
    sc_file_free(keyfile);

  pukey = NULL;
  SC_FUNC_RETURN(card->ctx, 1, r);
}


int ceres_update_pkcs15_pubkey_info_with_pkcs15_prkey_info (sc_card_t *card, sc_pkcs15_prkey_info_t *prk_info)
{
	int r = SC_SUCCESS;
	card_pkcs15_df_t p15_df;
	sc_pkcs15_card_t *temp_pkcs15_card = NULL;
	sc_pkcs15_df_t df;
	sc_path_t pukdf_path;
	sc_path_t ini_path;
	u8 *translated_buf = NULL;
	u8 *card_buf = NULL;
	size_t card_bufsize = 0;
	size_t translated_bufsize = 0;
	struct sc_pkcs15_object *obj = NULL;
	struct sc_pkcs15_pubkey_info puk_info;
	unsigned char *buffer = NULL;
	int length = 0;
	char st_puk_path[8] = {0x33, 0x46, 0x31, 0x31, 0x30, 0x31, 0x30, 0x30};
	virtual_file_t *virtual_pukdf = NULL;
	sc_file_t *keyfile = NULL;
	int old_use_virtual_fs; /*!< backup of use_virtual_fs */


	/* Fill sc_pkcs15_pubkey_info_t fields with prkey info */
	memset(puk_info.id.value, 0, sizeof (puk_info.id.value));
	memcpy(puk_info.id.value, prk_info->id.value, prk_info->id.len);
	puk_info.id.len = prk_info->id.len;

	r = map_ckaid_to_keyinfo_find(DRVDATA(card)->card_ckaid_to_card_keyinfo_map, &puk_info.id, NULL, NULL, &puk_info.usage, &puk_info.access_flags);
	if (r != SC_SUCCESS)
	{
		/* If didn't found a key_usage and access flags, we assign it hardcoded */
		puk_info.usage  = SC_PKCS15_PRKEY_USAGE_ENCRYPT;
		puk_info.usage |= SC_PKCS15_PRKEY_USAGE_WRAP;
		puk_info.usage |= SC_PKCS15_PRKEY_USAGE_VERIFY;
  
		puk_info.access_flags  = SC_PKCS15_PRKEY_ACCESS_EXTRACTABLE;
	}
	

	puk_info.native = 0x01;
	puk_info.key_reference = prk_info->key_reference;
	puk_info.modulus_length = prk_info->modulus_length;
	puk_info.modulus_length = puk_info.modulus_length;
	puk_info.subject = NULL;
	puk_info.subject_len = 0;

	if (card->type == SC_CARD_TYPE_CERES_ST) {
		st_puk_path[7] += puk_info.key_reference;
		sc_format_path(st_puk_path, &puk_info.path); 
	}
	else {
		sc_format_path("3F113F78", &puk_info.path);
	}

	/* Select public key file to knows the real size */

	/* we backup use_virtual_fs */
	old_use_virtual_fs = ceres_is_virtual_fs_active(card);

	/* we want to use card without virtual fs */
	ceres_set_virtual_fs_state(card, 0);


	sc_format_path("3F00", &ini_path);
	r = sc_select_file(card, &ini_path, &keyfile);
	if(r!=SC_SUCCESS)
		goto end;

	r = sc_select_file(card, &puk_info.path, &keyfile);

	/* we restore use_virtual_fs */
	ceres_set_virtual_fs_state(card, old_use_virtual_fs);

	if(r!=SC_SUCCESS)
		goto end;

	puk_info.path.count = keyfile->size;


	/* init p15_df structure */
	memset(&p15_df, 0, sizeof(p15_df));
	p15_df.type = SC_PKCS15_PUKDF;
 
	sc_format_path("3F0050156002", &pukdf_path);
	r = virtual_fs_get_data_by_path(DRVDATA(card)->virtual_fs, &pukdf_path, &buffer, &length);
	if (r != SC_SUCCESS) {
		sc_error(card->ctx, "Synchronization failed\n");
		goto end;
	}


	/* pass buffer ownership to data */
	p15_df.data = buffer;
	buffer = NULL;
	p15_df.data_len = length;
	p15_df.file_len = length;
	p15_df.filled_len = length;


	/* We parse PKCS#15 using a standard parser */
	r = ceres_parse_standard_pkcs15(card, &p15_df, &df, &temp_pkcs15_card);
	if(r != SC_SUCCESS) {
		if (card->ctx->debug) sc_debug(card->ctx, "Parsing of standard PKCS#15 failed\n");
		goto end;
	}

	/* now we have the structure in temp_p15card and can add puk_info */
	obj = (struct sc_pkcs15_object *) calloc(1, sizeof(struct sc_pkcs15_object));
	if (obj == NULL) {
		r = SC_ERROR_OUT_OF_MEMORY;
		goto end;
	}
	
	memcpy(obj->label, "Public Key", 10);

	obj->flags = puk_info.access_flags;
	obj->label[10] = '\0';
	obj->type = SC_PKCS15_TYPE_PUBKEY_RSA;
	obj->data = (void *)&puk_info;
	obj->df = &df;

	r = sc_pkcs15_add_object(temp_pkcs15_card, obj);
	if(r != SC_SUCCESS) {
		if (card->ctx->debug) sc_debug(card->ctx, "Parsing of standard PKCS#15 failed\n");
		goto end;
	}


	/* We encode PKCS#15 using CERES PKCS#15 */

	r = sc_pkcs15_encode_df(card->ctx,
                        	temp_pkcs15_card,
                        	&df,
                        	&translated_buf, 
				&translated_bufsize);

	if(r != SC_SUCCESS) {
		sc_error(card->ctx, "SC PKCS#15 encoding failed\n"); 
		goto end;
	}

	virtual_pukdf = virtual_fs_find_by_path( DRVDATA(card)->virtual_fs, &pukdf_path);

	r = virtual_file_data_update( virtual_pukdf, 0, translated_buf, translated_bufsize);

	if(r != SC_SUCCESS) {
		sc_error(card->ctx, "Error updating virtual_file\n"); 
		goto end;
	}

	r = virtual_file_data_synchronize( virtual_pukdf, card, virtual_file_sync_type_virtual_fs_to_card, DRVDATA(card)->virtual_fs);
	if(r != SC_SUCCESS) {
		sc_error(card->ctx, "Error synchronizing pukdf to card\n"); 
		goto end;
	}


 end:
	temp_pkcs15_card = NULL;
	virtual_pukdf = NULL;
	obj = NULL;

	if(translated_buf) {
		memset(translated_buf, 0, translated_bufsize);
		free(translated_buf);
		translated_buf = NULL;
		translated_bufsize = 0;
	}

	if(card_buf) {
		memset(card_buf, 0, card_bufsize);
		free(card_buf);
		card_buf = NULL;
		card_bufsize = 0;
	}

	if(keyfile)
		sc_file_free(keyfile);

	SC_FUNC_RETURN(card->ctx, 1, r);
}



int find_objects_by_df ( sc_card_t *card, const int type, const sc_pkcs15_id_t *id, sc_pkcs15_object_t **obj ) 
{
  int r = SC_SUCCESS;
  sc_pkcs15_card_t *temp_pkcs15_card  = NULL;
  card_pkcs15_df_t p15_df;
  sc_pkcs15_df_t df;
  virtual_file_t *virtual_file = NULL;
  sc_path_t path;
  int df_type = 0;

  /* Get type's virtual_file to find  */
  switch (type) {
    case SC_PKCS15_TYPE_PRKEY:
	sc_format_path("3F0050156001", &path);
	df_type = SC_PKCS15_PRKDF;
	break;
    case SC_PKCS15_TYPE_PUBKEY:
	sc_format_path("3F0050156002", &path);
	df_type = SC_PKCS15_PUKDF;
	break;
    case SC_PKCS15_TYPE_CERT:
	sc_format_path("3F0050156004", &path);
	df_type = SC_PKCS15_CDF;
	break;
    default:
	return SC_ERROR_UNKNOWN_DATA_RECEIVED;
  }

  virtual_file = virtual_fs_find_by_path(DRVDATA(card)->virtual_fs, &path);

  /* init p15_df structure */
  memset(&p15_df, 0, sizeof(p15_df));
  p15_df.type = df_type;

  /* virtualfs keeps buffer ownership */
  p15_df.data = virtual_file->data;
  p15_df.data_len = virtual_file->data_size;
  p15_df.file_len = virtual_file->data_size;
  p15_df.filled_len = virtual_file->data_size;

  /* We parse PKCS#15 using a standard parser */
  r = ceres_parse_standard_pkcs15(card, &p15_df, &df, &temp_pkcs15_card);
  if(r != SC_SUCCESS) {
    if (card->ctx->debug) sc_debug(card->ctx, "Parsing of standard PKCS#15 failed\n");
    goto end;
  }

  /* Looking for an object matching with this id*/
  r = sc_pkcs15_find_object_by_id(temp_pkcs15_card, type, id, obj);

end:

  temp_pkcs15_card = NULL;

  virtual_file = NULL;
 
  return r;
}


int ceres_sync_virtual_fs_to_card_any_df( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs, int type )
{
  int r = SC_SUCCESS;
  sc_pkcs15_card_t *temp_pkcs15_card  = NULL;
  sc_pkcs15_object_t *obj = NULL;
  sc_pkcs15_object_t *obj_aux = NULL;
  sc_pkcs15_df_t df;
  card_pkcs15_df_t p15_df;
  u8 *translated_buf = NULL;
  size_t translated_bufsize = 0;
  u8 *card_buf = NULL;
  size_t card_bufsize = 0;
  struct sc_pkcs15_cert_info* cert_info;
  int found_puk = 0;


  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  /* init p15_df structure */
  memset(&p15_df, 0, sizeof(p15_df));
  p15_df.type = type;

  /* virtualfs keeps buffer ownership */
  p15_df.data = virtual_file->data;
  p15_df.data_len = virtual_file->data_size;
  p15_df.file_len = virtual_file->data_size;
  p15_df.filled_len = virtual_file->data_size;

  /* We parse PKCS#15 using a standard parser */
  r = ceres_parse_standard_pkcs15(card, &p15_df, &df, &temp_pkcs15_card);
  if(r != SC_SUCCESS) {
    if (card->ctx->debug) sc_debug(card->ctx, "Parsing of standard PKCS#15 failed\n");
    goto end;
  }


  /* we need to correct some PKCS#15 data */
  for(obj = temp_pkcs15_card->obj_list; obj != NULL; obj = obj->next) {
    switch(obj->type & SC_PKCS15_TYPE_CLASS_MASK) {
    case SC_PKCS15_TYPE_CERT:
      {
	r = ceres_sync_virtual_fs_to_card_filter_cert(card, virtual_file, virtual_fs, obj);
        if (r != SC_SUCCESS)
                goto end;

	/* We need CKA_ID to find keys */
	cert_info = (struct sc_pkcs15_cert_info *) obj->data;

	r = find_objects_by_df (card, SC_PKCS15_TYPE_PUBKEY, &cert_info->id, &obj_aux);
        if (r == SC_ERROR_OBJECT_NOT_FOUND) 
		found_puk = 0;        	
	else if (r == SC_SUCCESS) 
		found_puk = 1;
	else 
		goto end;
	
	if (!found_puk) {
		r = find_objects_by_df (card, SC_PKCS15_TYPE_PRKEY, &cert_info->id, &obj_aux);
		/* If found a prkey without a pubkey, then create a pkcs15_pubkey copying prkey_info */
		if (r == SC_SUCCESS)
			r = ceres_update_pkcs15_pubkey_info_with_pkcs15_prkey_info (card, (sc_pkcs15_prkey_info_t *) obj_aux->data);
		else if (r == SC_ERROR_OBJECT_NOT_FOUND) 
	  		r = SC_SUCCESS;	
	}
      }
      break;

    case SC_PKCS15_TYPE_PRKEY:
      {
	r = ceres_sync_virtual_fs_to_card_filter_prkey(card, virtual_file, virtual_fs, obj);
      }
      break;

    case SC_PKCS15_TYPE_PUBKEY:
      {
	r = ceres_sync_virtual_fs_to_card_filter_pukey(card, virtual_file, virtual_fs, obj);
      }
      break;

    case SC_PKCS15_TYPE_DATA_OBJECT:
      {
	r = ceres_sync_virtual_fs_to_card_filter_data_object(card, virtual_file, virtual_fs, obj);
      }
      break;
 
      
    default:
      /* ignore this object */
      break;
    }
  }
  if(r != SC_SUCCESS) {
    sc_error(card->ctx, "Object filtering failed\n");
    goto end;
  }

  /* We encode PKCS#15 using CERES PKCS#15 */
  r = sc_pkcs15_card_encode_df(card->ctx,
                               temp_pkcs15_card,
                               &df,
                               &translated_buf,
                               &translated_bufsize);
 
  if(r != SC_SUCCESS) {
    sc_error(card->ctx, "CERES PKCS#15 encoding failed\n"); 
    goto end;
  }

  card_bufsize = translated_bufsize+1;
  card_buf = (u8 *) malloc(card_bufsize);
  if(!card_buf) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto end;
  }
      
  memcpy(card_buf, translated_buf, translated_bufsize);
  card_buf[translated_bufsize] = 0x00;

  r = ceres_helper_update_file( card, &virtual_file->path, card_buf, card_bufsize);
  if(r != SC_SUCCESS) {
    sc_error(card->ctx, "CERES PKCS#15 encoding failed\n"); 
    goto end;
  } 
 end:
  if(translated_buf) {
    memset(translated_buf, 0, translated_bufsize);
    free(translated_buf);
    translated_buf = NULL;
    translated_bufsize = 0;
  }

  if(card_buf) {
    memset(card_buf, 0, card_bufsize);
    free(card_buf);
    card_buf = NULL;
    card_bufsize = 0;
  }

  if(temp_pkcs15_card) { 
    /* set to NULL without freeing because we reused structure */
    temp_pkcs15_card->card = NULL;
    
    /* now free temp structure */
    sc_pkcs15_card_free(temp_pkcs15_card);
    temp_pkcs15_card = NULL;
  }  

  cert_info = NULL;
  obj_aux = NULL;

  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_virtual_fs_to_card_usdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with UNUSED SPACE param */
  return ceres_sync_virtual_fs_to_card_any_df(card, virtual_file, virtual_fs, SC_PKCS15_UNUSED);
}

int ceres_sync_virtual_fs_to_card_dodf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with DODF param */
  return ceres_sync_virtual_fs_to_card_any_df(card, virtual_file, virtual_fs, SC_PKCS15_DODF);
}

int ceres_sync_virtual_fs_to_card_cdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with CDF param */
  return ceres_sync_virtual_fs_to_card_any_df(card, virtual_file, virtual_fs, SC_PKCS15_CDF);
}

int ceres_sync_virtual_fs_to_card_prkdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with PrKDF param */
  return ceres_sync_virtual_fs_to_card_any_df(card, virtual_file, virtual_fs, SC_PKCS15_PRKDF);
}

int ceres_sync_virtual_fs_to_card_pukdf_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  /* use generic synchronization with PrKDF param */
  return ceres_sync_virtual_fs_to_card_any_df(card, virtual_file, virtual_fs, SC_PKCS15_PUKDF);
}


int ceres_sync_card_to_virtual_fs_data_object_file_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  int r = SC_SUCCESS;
  unsigned char *card_data = NULL;
  unsigned char *uncompressed_data = NULL;
  size_t card_data_length = 0;
  size_t uncompressed_data_length = 0;
//  sc_path_t *path=NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;
  
  /* get file */
  r = ceres_helper_read_file(card, &virtual_file->path, &card_data, &card_data_length);
  if(r < 0)
    goto end;
      
  r = file_uncompress_data(card, card_data, card_data_length, &uncompressed_data, &uncompressed_data_length); 
  if(r < 0)
    goto end;

  r = virtual_file_data_update(virtual_file, 0, uncompressed_data, uncompressed_data_length);
  if(r != SC_SUCCESS)
    goto end;
    
 end:
  if(card_data) {
    free(card_data);
    card_data = NULL;
  }

  if(uncompressed_data) {
    free(uncompressed_data);
    uncompressed_data = NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}


int ceres_sync_card_to_virtual_fs_certificate_file_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  int r = SC_SUCCESS;
  unsigned char *card_data = NULL;
  unsigned char *uncompressed_data = NULL;
  size_t card_data_length = 0;
  size_t uncompressed_data_length = 0;
  sc_path_t *path=NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;

  path = map_path_to_path_find(DRVDATA(card)->virtual_fs_to_card_path_map, &virtual_file->path);
  if(!path) {
    r = SC_ERROR_OBJECT_NOT_FOUND;
    goto end;
  }

  /* get file */
  r = ceres_helper_read_file(card, path, &card_data, &card_data_length);
  if(r < 0)
    goto end;
      
  r = file_uncompress_data(card, card_data, card_data_length, &uncompressed_data, &uncompressed_data_length); 
  if(r < 0)
    goto end;

  r = virtual_file_data_update(virtual_file, 0, uncompressed_data, uncompressed_data_length);
  if(r != SC_SUCCESS)
    goto end;
    
 end:
  if(card_data) {
    free(card_data);
    card_data = NULL;
  }

  if(uncompressed_data) {
    free(uncompressed_data);
    uncompressed_data = NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_virtual_fs_to_card_data_object_file_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  int r = SC_SUCCESS;
  unsigned char *compressed_data = NULL;
  size_t compressed_data_length = 0;
  struct _virtual_file_t *data_object_virtual_file=NULL;
  sc_pkcs15_id_t *card_ckaid=NULL;
  sc_path_t *data_object_path=NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;
  
  r = file_compress_data(card, virtual_file->data, virtual_file->data_size, &compressed_data, &compressed_data_length); 
  if(r!=SC_SUCCESS)
    goto do_vfs2c_end;

  /* create certificate file into card */
  r = ceres_helper_create_data_object_file(card, virtual_file, compressed_data_length, &data_object_virtual_file);
  if(r!=SC_SUCCESS)
    goto do_vfs2c_end;

  /* set file data to card */
  r = ceres_helper_update_file(card, &data_object_virtual_file->path, compressed_data, compressed_data_length);
  if(r!=SC_SUCCESS)
    goto do_vfs2c_end;

  /* add path_to_path */
  r = map_path_to_path_set_item(DRVDATA(card)->virtual_fs_to_card_path_map, &virtual_file->path, &data_object_virtual_file->path);
  if(r != SC_SUCCESS)
    goto do_vfs2c_end;

  /* ownership regards to vfs */
  data_object_virtual_file=NULL;
  card_ckaid=NULL;
  data_object_path=NULL;
    
 do_vfs2c_end:
  if(compressed_data) {
    free(compressed_data);
    compressed_data = NULL;
  }
  if(data_object_virtual_file) {
    free(data_object_virtual_file);
    data_object_virtual_file=NULL;
  }
  if(card_ckaid) {
    free(card_ckaid);
    card_ckaid=NULL;
  }
  if(data_object_path) {
    free(data_object_path);
    data_object_path=NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}

int ceres_sync_virtual_fs_to_card_certificate_file_callback( sc_card_t *card, struct _virtual_file_t *virtual_file, virtual_fs_t *virtual_fs )
{
  int r = SC_SUCCESS;
  unsigned char *compressed_data = NULL;
  size_t compressed_data_length = 0;
  struct _virtual_file_t *certificate_virtual_file=NULL;
  sc_pkcs15_id_t *card_ckaid=NULL;
  sc_path_t *cert_path=NULL;

  SC_FUNC_CALLED(card->ctx, 1);

  if(!card || !virtual_file)
    return SC_ERROR_INVALID_ARGUMENTS;
  
  r = file_compress_data(card, virtual_file->data, virtual_file->data_size, &compressed_data, &compressed_data_length); 
  if(r!=SC_SUCCESS)
    goto cert_vfs2c_end;

  /* create certificate file into card */
  r = ceres_helper_create_cert_file(card, virtual_file, compressed_data_length, &certificate_virtual_file);
  if(r!=SC_SUCCESS)
    goto cert_vfs2c_end;

  /* set file data to card */
  r = ceres_helper_update_file(card, &certificate_virtual_file->path, compressed_data, compressed_data_length);
  if(r!=SC_SUCCESS)
    goto cert_vfs2c_end;

  /* add path_to_path */
  r = map_path_to_path_set_item(DRVDATA(card)->virtual_fs_to_card_path_map, &virtual_file->path, &certificate_virtual_file->path);
  if(r != SC_SUCCESS)
    goto cert_vfs2c_end;

  /* get ckaid from certificate (computeing a sha1 form public key modulus) */
  card_ckaid = calloc(1, sizeof(struct sc_pkcs15_id));
  if (!card_ckaid) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto cert_vfs2c_end;
  }
  r = get_ckaid_from_certificate( card, virtual_file->data, virtual_file->data_size, card_ckaid );
  if(r!=SC_SUCCESS)
    goto cert_vfs2c_end;

  cert_path = calloc(1, sizeof(struct sc_path));
  if(!cert_path) {
    r = SC_ERROR_OUT_OF_MEMORY;
    goto cert_vfs2c_end;
  }
  memcpy(cert_path, &certificate_virtual_file->path, sizeof(struct sc_path));
  r = map_path_to_id_set_item(DRVDATA(card)->card_path_to_card_ckaid_map, cert_path, card_ckaid);
  if(r!=SC_SUCCESS)
    goto cert_vfs2c_end;

  /* ownership regards to vfs */
  certificate_virtual_file=NULL;
  card_ckaid=NULL;
  cert_path=NULL;
    
 cert_vfs2c_end:
  if(compressed_data) {
    free(compressed_data);
    compressed_data = NULL;
  }
  if(certificate_virtual_file) {
    free(certificate_virtual_file);
    certificate_virtual_file=NULL;
  }
  if(card_ckaid) {
    free(card_ckaid);
    card_ckaid=NULL;
  }
  if(cert_path) {
    free(cert_path);
    cert_path=NULL;
  }
  SC_FUNC_RETURN(card->ctx, 1, r);
}

/*
 * pkcs15-sec.c: PKCS#15 Ceres cryptography functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "../include/internal.h"
#include <opensc/asn1.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ltdl.h>
#include <opensc/log.h>
#include <opensc/pkcs15.h>
#include "pkcs15_default.h"
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include "padding.h"
 

sc_algorithm_info_t * _ceres_find_rsa_alg(sc_card_t *card,
                                                 unsigned int key_length)
{
        int i;

        for (i = 0; i < card->algorithm_count; i++) {
                sc_algorithm_info_t *info = &card->algorithms[i];

                if (info->algorithm != SC_ALGORITHM_RSA)
                        continue;
                if (info->key_length != key_length)
                        continue;
                return info;
        }
        return NULL;
}


int sc_pkcs15_ceres_decipher(struct sc_pkcs15_card *p15card,
			     const struct sc_pkcs15_object *obj,
			     unsigned long flags,
			     const u8 * in, size_t inlen, u8 *out, size_t outlen)
{
	int r;
	sc_algorithm_info_t *alg_info;
	sc_security_env_t senv;
	sc_context_t *ctx = p15card->card->ctx;
	const struct sc_pkcs15_prkey_info *prkey = (const struct sc_pkcs15_prkey_info *) obj->data;
	unsigned long pad_flags = 0;

	SC_FUNC_CALLED(ctx, 1);
	/* If the key is extractable, the caller should extract the
	 * key and do the crypto himself */
	if (!prkey->native)
		return SC_ERROR_EXTRACTABLE_KEY;

	if (!(prkey->usage & (SC_PKCS15_PRKEY_USAGE_DECRYPT|SC_PKCS15_PRKEY_USAGE_UNWRAP))) {
		sc_error(ctx, "This key cannot be used for decryption\n");
		return SC_ERROR_NOT_ALLOWED;
	}

	alg_info = _ceres_find_rsa_alg(p15card->card, prkey->modulus_length);
	if (alg_info == NULL) {
		sc_error(ctx, "Card does not support RSA with key length %d\n", prkey->modulus_length);
		return SC_ERROR_NOT_SUPPORTED;
	}
	senv.algorithm = SC_ALGORITHM_RSA;
	senv.algorithm_flags = 0;

	if (flags & SC_ALGORITHM_RSA_PAD_PKCS1) {
		if (!(alg_info->flags & SC_ALGORITHM_RSA_PAD_PKCS1))
			pad_flags |= SC_ALGORITHM_RSA_PAD_PKCS1;
		else
			senv.algorithm_flags |= SC_ALGORITHM_RSA_PAD_PKCS1;
	} else if ((flags & SC_ALGORITHM_RSA_PAD_ANSI) ||
		   (flags & SC_ALGORITHM_RSA_PAD_ISO9796)) {
		sc_error(ctx, "Only PKCS #1 padding method supported\n");
		return SC_ERROR_NOT_SUPPORTED;
	} else {
		if (!(alg_info->flags & SC_ALGORITHM_RSA_RAW)) {
			sc_error(ctx, "Card requires RSA padding\n");
			return SC_ERROR_NOT_SUPPORTED;
		}
		senv.algorithm_flags |= SC_ALGORITHM_RSA_RAW;
	}

	senv.operation = SC_SEC_OPERATION_DECIPHER;
	senv.flags = 0;
	/* optional keyReference attribute (the default value is -1) */
	if (prkey->key_reference >= 0) {
		senv.key_ref_len = 1;
		senv.key_ref[0] = prkey->key_reference & 0xFF;
		senv.flags |= SC_SEC_ENV_KEY_REF_PRESENT;
	}
	senv.flags |= SC_SEC_ENV_ALG_PRESENT;

	r = sc_lock(p15card->card);
	SC_TEST_RET(ctx, r, "sc_lock() failed");


	r = sc_set_security_env(p15card->card, &senv, 0);
	if (r < 0) {
		sc_unlock(p15card->card);
		SC_TEST_RET(ctx, r, "sc_set_security_env() failed");
	}
 
       /* Patch para soportar la tarjeta Ceres con distintas longitudes de clave*/
        outlen = prkey->modulus_length;

	r = sc_decipher(p15card->card, in, inlen, out, outlen);
	sc_unlock(p15card->card);
	SC_TEST_RET(ctx, r, "sc_decipher() failed");

	/* Strip any padding */
	if (pad_flags & SC_ALGORITHM_RSA_PAD_PKCS1) {
		r = sc_pkcs1_strip_02_padding(out, (size_t)r, out, (size_t *) &r);
			SC_TEST_RET(ctx, r, "Invalid PKCS#1 padding");
	}

	return r;
}


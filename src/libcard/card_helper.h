/*!
 * \file card_helper.h
 * \brief Card helper routines
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef CARD_HELPER_H
#define CARD_HELPER_H


#include <opensc/opensc.h>
#include "virtual_fs.h"

/*!
  Selects path and reads a file. It returns an allocated
  buffer (which must be freed by the user) with the data.
  
  \param card Struct to access the card
  \param path Path to the file to be read.
  \param buffer Output buffer with data allocated using malloc
  \param length Output length of data

  \returns SC_SUCCESS on success, error code otherwise
*/
int ceres_helper_read_file(sc_card_t *card, const sc_path_t *path, u8 **buffer, size_t *length); 

/*!
  Selects path and updates new data to file. 
  
  \param[in] card Struct to access the card
  \param[in] path Path to the file to be updated.
  \param[in] buffer Buffer with data
  \param[in] length Length of data buffer

  \returns SC_SUCCESS on success, error code otherwise
*/
int ceres_helper_update_file(sc_card_t *card, const sc_path_t *path, u8 *buffer, size_t length); 

/*!
  Creates a data object file to card and also a new data object file
  into virtual fs. This last file is returned as a parameter.
  
  \param[in] card Struct to access the card
  \param[in] virtual_file File containing data object data but with an OpenSC path
  \param[in] fdata_object_len Length of data object file
  \param[out] data_object_virtual_file New data object file created into virtual fs

  \returns SC_SUCCESS on success, error code otherwise
*/
int ceres_helper_create_data_object_file(sc_card_t *card, struct _virtual_file_t *virtual_file, 
				 size_t fdata_object_len, struct _virtual_file_t **data_object_virtual_file);


/*!
  Creates a certificate file to card and also a new certificate file
  into virtual fs. This last file is returned as a parameter.
  
  \param[in] card Struct to access the card
  \param[in] virtual_file File containing certificate data but with an OpenSC path
  \param[in] fcert_len Length of certificate file
  \param[out] certificate_virtual_file New certificate file created into virtual fs

  \returns SC_SUCCESS on success, error code otherwise
*/
int ceres_helper_create_cert_file(sc_card_t *card, struct _virtual_file_t *virtual_file, 
				 size_t fcert_len, struct _virtual_file_t **certificate_virtual_file);

#endif /* CARD_HELPER_H */
	    

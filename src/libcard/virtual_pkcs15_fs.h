/*!
 * \file virtual_pkcs15_fs.h
 * \brief Card virtual PKCS#15 filesystem
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef VIRTUAL_PKCS15_FS_H
#define VIRTUAL_PKCS15_FS_H


/* include for all virtual_* definitions */
#include "virtual_fs.h"


/*!
  Creates all files needed in PKCS#15 operation
  
  \param virtual_fs Virtual filesystem where the files will be created.

  \returns SC_SUCCESS on succes, error code otherwise
*/
int virtual_pkcs15_fs_init( virtual_fs_t *virtual_fs );


#endif /* VIRTUAL_PKCS15_FS_H */


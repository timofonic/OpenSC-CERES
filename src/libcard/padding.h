/*
 * padding.h: miscellaneous padding functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 * 
 */

#ifndef PADDING_H
#define PADDING_H

int sc_pkcs1_add_01_padding(const u8 *in, size_t in_len, u8 *out,
			    size_t *out_len, size_t mod_length);

int sc_pkcs1_strip_01_padding(const u8 *in_dat, size_t in_len, u8 *out,
			      size_t *out_len);

/* remove pkcs1 BT02 padding (adding BT02 padding is currently not
 * needed/implemented) */
int sc_pkcs1_strip_02_padding(const u8 *data, size_t len, u8 *out,
			      size_t *out_len);

int sc_pkcs1_strip_digest_info_prefix(unsigned int *algorithm,
				      const u8 *in_dat, size_t in_len, u8 *out_dat, size_t *out_len);

/* general PKCS#1 encoding function */
int sc_pkcs1_encode(sc_context_t *ctx, unsigned long flags,
		    const u8 *in, size_t in_len, u8 *out, size_t *out_len, size_t mod_len);

/* strip leading zero padding (does only really work when a DigestInfo
 * value has been padded */
int sc_strip_zero_padding(const u8 *in, size_t in_len, u8 *out,
			  size_t *out_len);
  


#endif /* PADDING_H */


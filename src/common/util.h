/*
 * util.h: Auxiliary functions
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef _UTIL_H
#define _UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <opensc/types.h>
#include "../include/ceres_cardctl.h"

/* TLV types  */
#define TLV_NORMAL		0x00
#define TLV_SPECIAL		0x01


  void ulong2lebytes(u8 *buf, unsigned long x);
  void ushort2lebytes(u8 *buf, unsigned short x);
  unsigned long lebytes2ulong(const u8 *buf);
  unsigned short lebytes2ushort(const u8 *buf);
  int push_front_data2buf(u8 **buf, size_t *buflen, const u8 *data, const size_t datalen);
  int push_back_data2buf(u8 **buf, size_t *buflen, const u8 *data, const size_t datalen);
  void free_struct( void *ptr, size_t length );
  int sc_path_set_ceres(sc_path_t *path, int type, unsigned char *id, size_t id_len,
		  int idx, int count);
  /*
    Parse a tlv and gets a buffer from all its components
    computing the tlv value length.

    \param[in] tlv structure containing all tlv data
    \param[out] buf buffer containing all tlv data parsed

    \return number of all tlv bytes if success, or error otherwise
   */
  int tlv2buf_normal (const tlv_t *tlv, u8 **buf);
  int tlv2buf_special(const tlv_t *tlv, u8 **buf);

  /*
    Builds a tlv computing a tlv correct length (ceres mode) and filling its parameters.
    Allocates necessary internal tlv buffers. Need to be freed on external function.

    \param[in] tag byte corresponding to a tag of tlv struct
    \param[in] data data to be copied to tlv
    \param[in] len length of data buffer
    \param[out] tlv structure to be filled

    returns SC_SUCCESS on succes, error otherwise
  */
  int buf2tlv(const u8 tag, const u8 *data, const size_t len, tlv_t *tlv);

  /*
    Frees memory from tlv length and tlv value buffers

    \param[in] tlv structure tlv to be freed
   */
  void free_tlv( tlv_t *tlv );

#ifdef __cplusplus
}
#endif

#endif /* _UTIL_H */

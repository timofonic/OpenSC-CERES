/*
 * structures.c: Support functions for additional structures
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "card_structures.h"
#include "util.h"
#include "../libcard/base_card.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

static int add_object( sc_card_t *card,
		       cert_file_t *obj )
{
  assert (obj!=NULL && card!=NULL);

  cert_file_t *p = DRVDATA(card)->list_cert_files;

  obj->next = obj->prev = NULL;
  if ( DRVDATA(card)->list_cert_files == NULL ) {
    DRVDATA(card)->list_cert_files = obj;
    return 0;
  }
  while (p->next != NULL)
    p = p->next;
  p->next = obj;
  obj->prev = p;

  return 0;
}

/*
  This function allocates memory for cert_file structure, 
  nitialitzing allocated memory to 0.

  @param out_fcert Pointer to a pointer to a cert_file structure.
                   This double pointer let us get the correct memory
		   position to locate cert_file struct.

*/
int init_cert_file_struct ( cert_file_t **out_fcert ) {

  int r = SC_SUCCESS;

  assert(out_fcert!=NULL);

  *out_fcert = (cert_file_t *) malloc(sizeof(struct cert_file));
  if (*out_fcert==NULL)
    return SC_ERROR_OUT_OF_MEMORY;

  memset( *out_fcert, 0, sizeof(struct cert_file));
  (*out_fcert)->next=NULL;
  (*out_fcert)->prev=NULL;
  (*out_fcert)->stored_on_card = 0;

  return r;
}

/*
  This function frees memory for cert_file structure.

  @param in_fcert Pointer to cert_file structure.
 */
void free_cert_file_struct( cert_file_t *in_fcert ) {

  if (in_fcert) {
    cert_file_t *aux;
    
    for (aux=in_fcert; aux!= NULL; aux = aux->next) {
      free(aux->prev);
      aux->prev=NULL;	
    }
    in_fcert = NULL;      
  }
}

static int find_object_by_path( cert_file_t *fcert, sc_path_t *in_path )
{
  cert_file_t *temp=NULL;
  sc_path_t aux_path;
  int r;

  /* just compare last 2 bytes */
  r = sc_path_set_ceres ( &aux_path, 
		    in_path->type, 
		    in_path->value+(in_path->len-2),
		    2,
		    0,
		    0);
  if (r)
    return 0;

  for(temp=fcert; temp!=NULL && !sc_compare_path(&temp->path, &aux_path); 
      temp=temp->next);

  if(temp)
    return 1;
  else 
    return 0;
}

static int get_object_by_path( sc_card_t *card, sc_path_t *in_path, cert_file_t **out_fcert )
{
  cert_file_t *temp=NULL;
  sc_path_t aux_path;
  int r;

  assert(card!=NULL && out_fcert!=NULL);

  if(out_fcert && *out_fcert) {
    free(*out_fcert);
    *out_fcert=NULL;
  }

  /* just compare last 2 bytes */
  r = sc_path_set_ceres ( &aux_path, 
		    in_path->type, 
		    in_path->value+(in_path->len-2),
		    2,
		    0,
		    0);
  if (r)
    return 0;

  for(temp=DRVDATA(card)->list_cert_files; temp!=NULL && 
	!sc_compare_path(&temp->path, &aux_path); temp=temp->next);

  if(temp) {
    *out_fcert = temp;
    return 1;
  }
  else 
    return 0;
}

/*
  This function creates a new cert_file and assigns second argument
  as a its path.

  @param card card structure 
  @param in_path path to the new cert_file object 
 */
int set_cert_file_path( sc_card_t *card, sc_path_t *in_path ) 
{
  cert_file_t *obj;
  int r;

  r = init_cert_file_struct( &obj );
  if (r!=SC_SUCCESS)
    return r;    

  /* just copy the last 2 bytes */
  r = sc_path_set_ceres( &(obj->path),
		   in_path->type, 
		   in_path->value+(in_path->len-2),
		   2,
		   in_path->index,
		   in_path->count
		   );       
  if (r!=SC_SUCCESS)
    return r;

  if (find_object_by_path( DRVDATA(card)->list_cert_files, &obj->path ) )
    return SC_ERROR_FILE_ALREADY_EXISTS;
  else
    return add_object(card, obj);
}


int set_uncompressed_len( sc_card_t *card, sc_path_t *in_path, const size_t in_unclen)
{
  cert_file_t *aux=NULL;

  assert(card!=NULL);
  
  if (get_object_by_path( card, in_path, &aux )) {
    if(aux) {
      aux->unclen = in_unclen;
      return SC_SUCCESS;
    } 
  }
  return SC_ERROR_FILE_NOT_FOUND;
}

int set_cert_stored_on_card( sc_card_t *card, sc_path_t *in_path, const int stored)
{
  cert_file_t *aux=NULL;

  assert(card!=NULL && (stored==0 || stored==1));
  
  if (get_object_by_path( card, in_path, &aux )) {
    if(aux) {
      aux->stored_on_card=stored;
      return SC_SUCCESS;
    } 
  }
  return SC_ERROR_FILE_NOT_FOUND;
}

int get_cert_stored_on_card( sc_card_t *card, sc_path_t *in_path, int *out_stored)
{
  cert_file_t *aux=NULL;

  assert(card!=NULL);
  
  if (get_object_by_path( card, in_path, &aux )) {
    if(aux) {
      *out_stored = aux->stored_on_card;
      return SC_SUCCESS;
    } 
  }
  return SC_ERROR_FILE_NOT_FOUND;
}

int get_uncompressed_len( sc_card_t *card, sc_path_t *in_path, size_t *out_unclen)
{
  cert_file_t *aux=NULL;

  assert(card!=NULL);
  
  if (get_object_by_path( card, in_path, &aux )) {
    if(aux) {
      *out_unclen = aux->unclen;
      return SC_SUCCESS;
    } 
  }
  return SC_ERROR_FILE_NOT_FOUND;
}

int get_compressed_len( sc_card_t *card, sc_path_t *in_path, size_t *out_complen)
{
  cert_file_t *aux=NULL;

  assert(card!=NULL);
  
  if (get_object_by_path( card, in_path, &aux )) {
    if(aux) {
      *out_complen = aux->complen;
      return SC_SUCCESS;
    } 
  }
  return SC_ERROR_FILE_NOT_FOUND;
}

int get_cert_file_len( sc_card_t *card, sc_path_t *in_path, size_t *out_file_len)
{
  cert_file_t *aux=NULL;

  assert(card!=NULL);
  
  if (get_object_by_path( card, in_path, &aux )) {
    if(aux) {
      /* If file_len==0 means that OpenSC
	 wants certificate file uncompressed
	 to read. 
	 Otherwise, means that OpenSC
	 wants to create a file and this value
	 is uncompressed size + 8 header bytes
      */
      if (aux->file_len==0)
	*out_file_len = aux->unclen;
      else
	*out_file_len = aux->file_len;
      return SC_SUCCESS;
    } 
  }
  return SC_ERROR_FILE_NOT_FOUND;
}

/*!
 * \file map_helper.c
 * \brief A map data type
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#include "map_helper.h"
#include <opensc/opensc.h>
#include <opensc/pkcs15.h>
#include <stdlib.h>
#include <string.h>


int aux_compare_path_value(const sc_path_t* path1, const sc_path_t* path2){
	int i;

	if (path1->len != path2->len)
		return 0;
	for ( i=0; i<path1->len;i++){
		if (path1->value[i]!= path2->value[i])
			return 0;
	}
return 1;
}

map_two_t * map_two_new( column_operation_free *index_free,
			 column_operation_is_equal *index_is_equal,
			 column_operation_free *mapped_free,
			 column_operation_is_equal *mapped_is_equal)
{
  column_operations_t column_operations[2];

  column_operations[0].free = index_free;
  column_operations[0].is_equal = index_is_equal;
  column_operations[1].free = mapped_free;
  column_operations[1].is_equal = mapped_is_equal;

  return map_new(2, column_operations);
}

map_three_t * map_three_new( column_operation_free *index_free,
			     column_operation_is_equal *index_is_equal,
			     column_operation_free *mapped_free_one,
			     column_operation_is_equal *mapped_is_equal_one,
			     column_operation_free *mapped_free_two,
			     column_operation_is_equal *mapped_free_is_equal_two)
{

  column_operations_t column_operations[3];
  column_operations[0].free = index_free;
  column_operations[0].is_equal = index_is_equal;
  column_operations[1].free = mapped_free_one;
  column_operations[1].is_equal = mapped_is_equal_one;
  column_operations[2].free = mapped_free_two;
  column_operations[2].is_equal = mapped_free_is_equal_two;
  return map_new(3, column_operations);

}


void * map_two_find_mapped( map_two_t *map, const void *index )
{
  void **map_item = NULL;

  map_item = map_find_by_column_data(map, index, 0);
  if(map_item && map_item[1]) {
    return map_item[1];
  }
  return NULL;
}

int map_two_set_item( map_two_t *map, const void *index, copy_constructor *index_copy_constructor, const void *mapped, copy_constructor *mapped_copy_constructor )
{
  int r = SC_SUCCESS;
  void **map_item = NULL;
  void *map_item_array[2] = {NULL, NULL};
  void *temp_mapped = NULL;
  void *temp = NULL;

  if(mapped_copy_constructor) {
    temp_mapped = mapped_copy_constructor(mapped);
  } else {
    temp_mapped = (void *)mapped;
  }

  map_item = map_find_by_column_data(map, index, 0);
  if(map_item) {
    /* replace mapped */
    /* modifying map_item is modifying object */
    temp = map_item[1];
    map_item[1] = temp_mapped;
    
    /* we leave old data in temp_mapped_path so that it gets freed */
    temp_mapped = temp;
    temp = NULL;
  } else {
    /* we introduce a new item */
    if(index_copy_constructor) {
      temp = index_copy_constructor(index);
    } else {
      temp = (void *)index;
    }

    map_item_array[0] = temp;
    map_item_array[1] = temp_mapped;
    r = map_append_item(map, map_item_array);
    if(r != SC_SUCCESS)
      goto end;

    /* avoid data being freed.. we transfered ownership to map */
    temp = NULL;
    temp_mapped = NULL;
  }

 end:
  if(temp_mapped && map->column_operations && map->column_operations[1].free) {
    map->column_operations[1].free(temp_mapped);
    temp_mapped = NULL;
  }
  
  if(temp && map->column_operations && map->column_operations[0].free) {
    map->column_operations[0].free(temp);
    temp_mapped = NULL;
  }

  return r;
}

int map_three_set_item(map_three_t *map, const void *index, copy_constructor *index_copy_constructor, const void *mapped_one, 
			copy_constructor *mapped_copy_constructor_one, const void *mapped_two, copy_constructor *mapped_copy_constructor_two){
	int r = SC_SUCCESS;
	void **map_item = NULL;
	void *map_item_array[3] = {NULL,NULL,NULL};
	void *temp_index = NULL;
	#if defined(__x86_64__)
	long temp_one = 0;
	long temp_mapped_one = 0;
	long temp_two = 0;
	long temp_mapped_two = 0;
	#else
	unsigned int temp_one = 0;
	unsigned int temp_mapped_one = 0;
	unsigned int temp_two = 0;
	unsigned int temp_mapped_two = 0;
	#endif

	if(mapped_copy_constructor_one) {
	 	#if defined(__x86_64__)
	 	temp_mapped_one = (long)mapped_copy_constructor_one(mapped_one);
		#else
	 	temp_mapped_one = (unsigned int)mapped_copy_constructor_one(mapped_one);
		#endif
	} else {
		#if defined(__x86_64__)
		   temp_mapped_one = *(long*)mapped_one;
		#else
		   temp_mapped_one = *(unsigned int *)mapped_one;
		#endif
 	}
	if(mapped_copy_constructor_two) {
		#if defined(__x86_64__)
		    temp_mapped_two = (long)mapped_copy_constructor_two(mapped_two);
		#else
		    temp_mapped_two = (unsigned int)mapped_copy_constructor_two(mapped_two);
		#endif
	} else {
		#if defined(__x86_64__)
		   temp_mapped_two = *(long *)mapped_two;
		#else
		   temp_mapped_two = *(unsigned int *)mapped_two;
		#endif
 	}

	map_item = map_find_by_column_data(map,index,0);

	if(map_item){
		#if defined(__x86_64__)
			temp_one = (long) map_item[1];
		#else
			temp_one = (unsigned int) map_item[1];
		#endif
		map_item[1] = (void*)temp_mapped_one;
		temp_mapped_one = temp_one;
		temp_one = 0;
		
		#if defined(__x86_64__)
			temp_two = (long)map_item[2];
		#else
			temp_two = (long)map_item[2];
		#endif
		map_item[2] = (void*)temp_mapped_two;
		temp_mapped_two = temp_two;		
		temp_two = 0;
	}
	else{
		if(index_copy_constructor){
			temp_index = index_copy_constructor(index);
		}
		else{
			temp_index = (void *)index;
		}
		map_item_array[0] = temp_index;
		map_item_array[1] = (void*)temp_mapped_one;
		map_item_array[2] = (void*)temp_mapped_two;
		r = map_append_item(map,map_item_array);
	  	if(r != SC_SUCCESS)
      			goto end;

		temp_index = NULL;
		temp_mapped_one = 0;
		temp_mapped_two = 0;
	}
end:
  return r;
}

map_five_t * map_five_new( column_operation_free *index_free,
			   column_operation_is_equal *index_is_equal,
			   column_operation_free *mapped_free_one,
			   column_operation_is_equal *mapped_is_equal_one,
			   column_operation_free *mapped_free_two,
			   column_operation_is_equal *mapped_is_equal_two,
			   column_operation_free *mapped_free_three,
			   column_operation_is_equal *mapped_is_equal_three,
			   column_operation_free *mapped_free_four,
			   column_operation_is_equal *mapped_is_equal_four)
{
  column_operations_t column_operations[5];

  column_operations[0].free = index_free;
  column_operations[0].is_equal = index_is_equal;
  column_operations[1].free = mapped_free_one;
  column_operations[1].is_equal = mapped_is_equal_one;
  column_operations[2].free = mapped_free_two;
  column_operations[2].is_equal = mapped_is_equal_two;
  column_operations[3].free = mapped_free_three;
  column_operations[3].is_equal = mapped_is_equal_three;
  column_operations[4].free = mapped_free_four;
  column_operations[4].is_equal = mapped_is_equal_four;

  return map_new(5, column_operations);
}


/**
 * map_three_find_mapped
 *
 * parameter mapped_one: key_type
 * parameter mapped_two: key_reference
 *
 * returns 1 if correct, else -1
 */
int map_three_find_mapped(map_three_t *map,
				const void *index,
				void *mapped_one,
				void *mapped_two){
	void **map_item = NULL;
	map_item = map_find_by_column_data(map,index,0);
	if(map_item){
		#if defined(__x86_64__)
		*(long*)mapped_one   = (long)map_item[1];
	    	*(long*)mapped_two   = (long)map_item[2];
		#else
		*(unsigned int*)mapped_one   = (unsigned int)map_item[1];
	    	*(unsigned int*)mapped_two   = (unsigned int)map_item[2];
		#endif

	return SC_SUCCESS;
	}
	return SC_ERROR_DATA_OBJECT_NOT_FOUND;
}

int map_five_find_mapped( map_five_t *map, 
				const void *index,
				void *mapped_one, 
				void *mapped_two,
				void *mapped_three, 
				void *mapped_four)
{
  void **map_item = NULL;

  map_item = map_find_by_column_data(map, index, 0);
  if(map_item) {
  	#if defined(__x86_64__)
	    *(long*)mapped_one   = (long)map_item[1];
	    *(long*)mapped_two   = (long)map_item[2];
	    *(long*)mapped_three = (long)map_item[3];
	    *(long*)mapped_four  = (long)map_item[4];
	#else
 	    *(unsigned int*)mapped_one   = (unsigned int)map_item[1];
	    *(unsigned int*)mapped_two   = (unsigned int)map_item[2];
	    *(unsigned int*)mapped_three = (unsigned int)map_item[3];
	    *(unsigned int*)mapped_four  = (unsigned int)map_item[4];

	#endif
    return 0;
  }
  return 1;
}

int map_five_set_item( map_five_t *map, const void *index, copy_constructor *index_copy_constructor, 
			const void *mapped_one, copy_constructor *mapped_copy_constructor_one, 
			const void *mapped_two, copy_constructor *mapped_copy_constructor_two, 
			const void *mapped_three, copy_constructor *mapped_copy_constructor_three, 
			const void *mapped_four, copy_constructor *mapped_copy_constructor_four)
{
  int r = SC_SUCCESS;
  void **map_item = NULL;
  void *map_item_array[5] = {NULL, NULL, NULL, NULL, NULL};
	/* NOTICE: we use int because map_five_item is only used by ckaid_to_keyinfo */
	#if defined(__x86_64__)
		long temp_mapped_one = 0;
  		long temp_one = 0;
  		long temp_mapped_two = 0;
  		long temp_two = 0;
  		long temp_mapped_three = 0;
  		long temp_three = 0;
  		long temp_mapped_four = 0;
  		long temp_four = 0;
	#else
		unsigned int temp_mapped_one = 0;
  		unsigned int temp_one = 0;
  		unsigned int temp_mapped_two = 0;
  		unsigned int temp_two = 0;
  		unsigned int temp_mapped_three = 0;
  		unsigned int temp_three = 0;
  		unsigned int temp_mapped_four = 0;
  		unsigned int temp_four = 0;

	#endif
  void *temp_index = 0;


  if(mapped_copy_constructor_one) {
  	#if defined(__x86_64__)
    		temp_mapped_one = (long)mapped_copy_constructor_one(mapped_one);
	#else
    		temp_mapped_one = (unsigned int)mapped_copy_constructor_one(mapped_one);
	#endif
  } else {
  	#if defined(__x86_64__)
    		temp_mapped_one = *(long*)mapped_one;
	#else
    		temp_mapped_one = *(unsigned int*)mapped_one;
	#endif
  }

  if(mapped_copy_constructor_two) {
  	#if defined(__x86_64__)
   		temp_mapped_two = (long)mapped_copy_constructor_two(mapped_two);
   	#else
   		temp_mapped_two = (unsigned int)mapped_copy_constructor_two(mapped_two);
	#endif
  } else {
  	#if defined(__x86_64__)
    		temp_mapped_two = *(long*)mapped_two;
	#else
    		temp_mapped_two = *(unsigned int*)mapped_two;
	#endif
  }

  if(mapped_copy_constructor_three) {
	#if defined(__x86_64__)
		temp_mapped_three = (long)mapped_copy_constructor_three(mapped_three);
	#else
		temp_mapped_three = (unsigned int)mapped_copy_constructor_three(mapped_three);
	#endif
  } else {
  	#if defined(__x86_64__)
    		temp_mapped_three = *(long*)mapped_three;
	#else
    		temp_mapped_three = *(unsigned int*)mapped_three;
	#endif
  }

  if(mapped_copy_constructor_four) {
  	#if defined(__x86_64__)
    		temp_mapped_four = (long)mapped_copy_constructor_four(mapped_four);
	#else
    		temp_mapped_four = (unsigned int)mapped_copy_constructor_four(mapped_four);
	#endif
  } else {
  	#if defined(__x86_64__)
		temp_mapped_four = *(long*)mapped_four;
	#else
		temp_mapped_four = *(unsigned int*)mapped_four;
	#endif

  }

  map_item = map_find_by_column_data(map, index, 0);
  if(map_item) {
    /* replace mapped */
    /* modifying map_item is modifying object */
    #if defined(__x86_64__)
    temp_one 	= (long)map_item[1];
    map_item[1] =  (void*)temp_mapped_one;
    temp_two 	= (long)map_item[2];
    map_item[2] =  (void*)temp_mapped_two;
    temp_three 	= (long)map_item[3];
    map_item[3] =  (void*)temp_mapped_three;
    temp_four 	= (long)map_item[4];
    map_item[4] =  (void*)temp_mapped_four;
    #else
    temp_one 	= (unsigned int)map_item[1];
    map_item[1] =  (void*)temp_mapped_one;
    temp_two 	= (unsigned int)map_item[2];
    map_item[2] =  (void*)temp_mapped_two;
    temp_three 	= (unsigned int)map_item[3];
    map_item[3] =  (void*)temp_mapped_three;
    temp_four 	= (unsigned int)map_item[4];
    map_item[4] =  (void*)temp_mapped_four;

    #endif
    /* we leave old data in temp_mapped_path so that it gets freed */
    temp_mapped_one   = temp_one;
    temp_mapped_two   = temp_two;
    temp_mapped_three = temp_three;
    temp_mapped_four  = temp_four;
 
  } else {
    
/* we introduce a new item */
    if(index_copy_constructor) {
      temp_index = index_copy_constructor(index);
    } else {
      temp_index = (void *)index;
    }

    map_item_array[0] = temp_index;
    map_item_array[1] = (void*)temp_mapped_one;
    map_item_array[2] = (void*)temp_mapped_two;
    map_item_array[3] = (void*)temp_mapped_three;
    map_item_array[4] = (void*)temp_mapped_four;
    r = map_append_item(map, map_item_array);
    if(r != SC_SUCCESS)
      goto end;

    /* avoid data being freed.. we transfered ownership to map */
    temp_index = NULL;
  }

 end:
 
  return r;
}


map_ckaid_to_keyinfo_t * map_ckaid_to_keyinfo_new()
{
  return map_five_new(free,
		     (int(*)(const void *, const void *))sc_pkcs15_compare_id,
		     NULL,
		     NULL,
		     NULL,
		     NULL,
		     NULL,
		     NULL,
		     NULL,
		     NULL);
}

int map_ckaid_to_keyinfo_set_item( map_ckaid_to_keyinfo_t *map, 
					const sc_pkcs15_id_t *index_id, 
					const unsigned int prk_key_usage,
					const unsigned int prk_access_flags,
					const unsigned int puk_key_usage,
					const unsigned int puk_access_flags )

{
  return map_five_set_item(map, index_id, (copy_constructor *)id_copy_constructor, &prk_key_usage, NULL, &prk_access_flags, NULL, &puk_key_usage, NULL, &puk_access_flags, NULL);
}


int map_path_to_key_info_find( map_path_to_key_info_t *map, const sc_path_t *path, unsigned int **key_type, unsigned int **key_reference)
{

	return map_three_find_mapped(map, path, key_type, key_reference);

}
int map_ckaid_to_keyinfo_find( map_ckaid_to_keyinfo_t *map, 
					const sc_pkcs15_id_t *id,
					unsigned int *prk_key_usage,
					unsigned int *prk_access_flags,
					unsigned int *puk_key_usage,
					unsigned int *puk_access_flags)
{
#if defined(__x86_64__)
  long pr_usage  = 0;
  long pr_access = 0;
  long pu_usage  = 0;
  long pu_access = 0;
#else
  unsigned int pr_usage  = 0;
  unsigned int pr_access = 0;
  unsigned int pu_usage  = 0;
  unsigned int pu_access = 0;
#endif
  int result = 0;
  result =  map_five_find_mapped(map, id, &pr_usage, &pr_access, &pu_usage, &pu_access);

  if (prk_key_usage && prk_access_flags)
  {
    *prk_key_usage    = pr_usage;
    *prk_access_flags = pr_access;
  }

  if (puk_key_usage && puk_access_flags)
  {
    *puk_key_usage    = pu_usage;
    *puk_access_flags = pu_access;
  }

  return result;
}



map_path_to_path_t * map_path_to_path_new()
{
  return map_two_new(free,
		     (int(*)(const void *, const void *))sc_compare_path,
		     free,
		     (int(*)(const void *, const void *))sc_compare_path);
}

int map_path_to_path_set_item( map_path_to_path_t *map, const sc_path_t *index_path, const sc_path_t *mapped_path )
{
  return map_two_set_item(map, index_path, (copy_constructor *)path_copy_constructor, mapped_path, (copy_constructor *)path_copy_constructor);
}

sc_path_t * map_path_to_path_find( map_path_to_path_t *map, const sc_path_t *path )
{
  return map_two_find_mapped(map, path);
}


int map_path_to_path_set_all_keys_paths( map_path_to_path_t *map, int opensc_key_reference, int card_key_reference, int is_st )
{
  int r = SC_SUCCESS;
  sc_path_t opensc_path;
  sc_path_t card_path;
  
  /* clear path */
  memset(&opensc_path, 0, sizeof(opensc_path));
  opensc_path.type = SC_PATH_TYPE_PATH;

  memset(&card_path, 0, sizeof(card_path));
  card_path.type = SC_PATH_TYPE_PATH;

  
  /*
    Store card paths

    card paths:
    * INFINEON:
     - 3F11//3F77 (Prk)
     - 3F11//3F78 (Pbk)
    
    * ST:
     - 3F11//01id (Prk & Pbk)
  */
  if(is_st) {
    /* card path */
    memcpy(&card_path.value, "\x3f\x00\x3f\x11\x01", 5);
    card_path.value[5] = card_key_reference;
    card_path.len = 6;

    /* private key */
    memcpy(&opensc_path.value, "\x3f\x00\x50\x15\x20", 5);
    opensc_path.value[5] = opensc_key_reference;
    opensc_path.len = 6;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;

    /* second private key version */
    memcpy(&opensc_path.value, "\x3f\x00\x20", 3);
    opensc_path.value[3] = opensc_key_reference;
    opensc_path.len = 4;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;

    /* public key */
    memcpy(&opensc_path.value, "\x3f\x00\x50\x15\x21", 5);
    opensc_path.value[5] = opensc_key_reference;
    opensc_path.len = 6;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;

    /* second public key version */
    memcpy(&opensc_path.value, "\x3f\x00\x21", 3);
    opensc_path.value[3] = opensc_key_reference;
    opensc_path.len = 4;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;
  } else {
    /* private key card path */
    memcpy(&card_path.value, "\x3f\x00\x3f\x11\x3f\x77", 6);
    card_path.len = 6;

    /* private key */
    memcpy(&opensc_path.value, "\x3f\x00\x50\x15\x20", 5);
    opensc_path.value[5] = opensc_key_reference;
    opensc_path.len = 6;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;

    /* second private key version */
    memcpy(&opensc_path.value, "\x3f\x00\x20", 3);
    opensc_path.value[3] = opensc_key_reference;
    opensc_path.len = 4;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;

    /* public key card path */
    memcpy(&card_path.value, "\x3f\x00\x3f\x11\x3f\x78", 6);
    card_path.len = 6;

    /* public key */
    memcpy(&opensc_path.value, "\x3f\x00\x50\x15\x21", 5);
    opensc_path.value[5] = opensc_key_reference;
    opensc_path.len = 6;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;

    /* second public key version */
    memcpy(&opensc_path.value, "\x3f\x00\x21", 3);
    opensc_path.value[3] = opensc_key_reference;
    opensc_path.len = 4;
    r = map_path_to_path_set_item(map, &opensc_path, &card_path);
    if(r != SC_SUCCESS)
      goto end;
  }

 end:
  return r;
}

map_id_to_id_t * map_id_to_id_new()
{
  return map_two_new(free,
		     (int(*)(const void *, const void *))sc_pkcs15_compare_id,
		     free,
		     (int(*)(const void *, const void *))sc_pkcs15_compare_id);
}

/*!
  Append new id to map.

  \param map The map object
  \param index_id path to index item
  \param mapped_id mapped path of item
  
  \returns SC_SUCCESS on success, error code otherwise
*/
int map_id_to_id_set_item( map_path_to_path_t *map, const sc_pkcs15_id_t *index_id, const sc_pkcs15_id_t *mapped_id )
{
  return map_two_set_item(map, index_id, (copy_constructor *)id_copy_constructor, mapped_id, (copy_constructor *)id_copy_constructor);
}

map_path_to_id_t * map_path_to_id_new()
{
  return map_two_new(free,
		     (int(*)(const void *, const void *))sc_compare_path,
		     free,
		     (int(*)(const void *, const void *))sc_pkcs15_compare_id);
}

map_path_to_key_info_t * map_path_to_key_info_new(){
  return map_three_new(free,
		       (int(*)(const void *, const void *))aux_compare_path_value,
		       NULL,
		       NULL,
		       NULL,
		       NULL);
}

int map_path_to_key_info_set_item(map_path_to_key_info_t *map, const sc_path_t *path, const unsigned int key_type, const unsigned int key_reference)
{
  return map_three_set_item(map, path, (copy_constructor *)path_copy_constructor, &key_type, NULL, &key_reference, NULL);
}

int map_path_to_id_set_item( map_path_to_id_t *map, const sc_path_t *path, const sc_pkcs15_id_t *id )
{
  return map_two_set_item(map, path, (copy_constructor *)path_copy_constructor, id, (copy_constructor *)id_copy_constructor);
}

sc_pkcs15_id_t * map_path_to_id_find( map_path_to_id_t *map, const sc_path_t *path )
{
  return map_two_find_mapped(map, path);
}

map_id_to_der_t * map_id_to_der_new()
{
  return map_two_new(free,
		     (int(*)(const void *, const void *))sc_pkcs15_compare_id,
		     (void(*)(void *))sc_der_free, /* sc_pkcs15_der_t */
		     NULL /* can't find by der */);
}

int map_id_to_der_set_item( map_id_to_der_t *map, const sc_pkcs15_id_t *id, const sc_pkcs15_der_t *der )
{
  return map_two_set_item(map, id, (copy_constructor *)id_copy_constructor, der, (copy_constructor *)der_copy_constructor);
}

sc_pkcs15_der_t * map_id_to_der_find( map_path_to_path_t *map, const sc_pkcs15_id_t *id )
{
  return map_two_find_mapped(map, id);
}

sc_pkcs15_id_t * map_opensc_id_to_id_find( map_opensc_id_to_id_t *map, const sc_pkcs15_id_t *id )
{
  return map_two_find_mapped(map, id);
}

sc_path_t *path_copy_constructor( const sc_path_t *path )
{
  sc_path_t *result = NULL;
  BINARY_COPY_CONSTRUCTOR(result,path,sizeof(sc_path_t));
  return result;
}

sc_pkcs15_id_t *id_copy_constructor( const sc_pkcs15_id_t *id )
{
  sc_pkcs15_id_t *result = NULL;
  BINARY_COPY_CONSTRUCTOR(result,id,sizeof(sc_pkcs15_id_t));
  return result;
}

sc_pkcs15_der_t *der_copy_constructor( const sc_pkcs15_der_t *der )
{
  sc_pkcs15_der_t *result = NULL;

  if(!der)
    return NULL;

  result = calloc(1, sizeof(sc_pkcs15_der_t));
  if(!result)
    return NULL;

  sc_der_copy(result, der);
  return result;
}

void sc_der_free(sc_pkcs15_der_t *der)
{
  if(der) {
    sc_der_clear(der);
    free(der);
  }
}

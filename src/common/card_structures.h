/*
 * card_structures.h: Support functions for additional card structures
 *
 * Copyright (C) 2006-2010 Fábrica Nacional de Moneda y Timbre - Real Casa de la Moneda
 *
 */

#ifndef _CARD_STRUCTURES_H
#define _CARD_STRUCTURES_H

#ifdef __cplusplus
extern "C" {
#endif

#include <opensc/opensc.h>

/* structure defintions */

struct card_pkcs15_df {
  u8 *            data;        /* Buffer to hold PKCS#15 struct info               */
  size_t          data_len;    /* Data buffer length and also the corresponding    */              
  size_t          file_len;    /* PKCS#15 card file size                           */
  size_t          filled_len;  /* Length of pkcs#15 struct info stored on df       */ 
                               /* This value will be lower or at maximum           */
                               /*  equals than len and will be increased/decreased */ 
                               /*  when added/deleted pkcs#15 objects              */
  sc_path_t       df_path;     /* Path of pkcs15_df                                */
                               /*  a relative or absolute path                     */
  unsigned int    type;        /* PKCS#15 DF type                                  */
};
typedef struct card_pkcs15_df card_pkcs15_df_t;

struct card_list_pkcs15_df {
  card_pkcs15_df_t odf;          /* PKCS#15 Objects Data File */
  card_pkcs15_df_t token_info;   /* PKCS#15 Token Info File */
  card_pkcs15_df_t aodf;         /* PKCS#15 Authentication Objects Data File */
  card_pkcs15_df_t prkdf;        /* PKCS#15 Private Keys Data File */
  card_pkcs15_df_t pukdf;        /* PKCS#15 Public Keys Data File */
  card_pkcs15_df_t cdf;          /* PKCS#15 Certificates Data File */
  card_pkcs15_df_t dodf;         /* PKCS#15 Data Objects Data File */
  card_pkcs15_df_t unused_space; /* PKCS#15 Unused Space Data File */
};
typedef struct card_list_pkcs15_df card_list_pkcs15_df_t;

struct cert_file {
  sc_path_t path;                /* certificate file path (temporaly relative one) */
  size_t unclen;                 /* length of the uncompressed file                */
  size_t complen;                /* length of the compressed file                  */
  size_t file_len;		 /* length of certificate file			   */
  int stored_on_card;            /* Means if Certfile is stored at card            */
  struct cert_file *next, *prev; /* pointers to the next and previous struct elem  */
}; 
typedef struct cert_file cert_file_t;


  /*************************/
  /* Function definitions */
  /*************************/

  int set_cert_file_path( sc_card_t *card, sc_path_t *in_path );
  void free_cert_file_struct( cert_file_t *in_fcert );
  int set_uncompressed_len( sc_card_t *card, sc_path_t *in_path, const size_t in_unclen);
  int set_cert_stored_on_card( sc_card_t *card, sc_path_t *in_path, const int stored);
  int get_cert_stored_on_card( sc_card_t *card, sc_path_t *in_path, int *out_stored);
  int get_uncompressed_len( sc_card_t *card, sc_path_t *in_path, size_t *out_unclen);
  int get_compressed_len( sc_card_t *card, sc_path_t *in_path, size_t *out_complen);
  int get_cert_file_len( sc_card_t *card, sc_path_t *in_path, size_t *out_file_len);


#ifdef __cplusplus
}
#endif

#endif /* _CARD_STRUCTURES_H */
